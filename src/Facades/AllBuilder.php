<?php

namespace Mediapress\AllBuilder\Facades;

use Illuminate\Support\Facades\Facade;
use Mediapress\AllBuilder\Foundation\AllBuilder as AllBuilderFoundation;

class AllBuilder extends Facade
{
    protected static function getFacadeAccessor()
    {
        return AllBuilderFoundation::class;
    }
}