<?php
    /**
     * Created by PhpStorm.
     * User: eray
     * Date: 12.12.2018
     * Time: 14:57
     */

    namespace Mediapress\AllBuilder\Foundation;


    class AllBuilder
    {

        protected $alternating_priority=["scope","object_name","section","module"];


        /**
         * @param $renderable
         * @param null $module
         * @param null $scope
         * @param null $section
         * @param array $alternating_priority
         * @return array
         */
        public function getBuilderClassPath($renderable, $module=null, $scope=null,  $section=null, $alternating_priority=[]){

            return $this->getObjectClassPath($renderable, "renderable", $module, $scope, $section, $alternating_priority);

        }

        /**
         * @param $storer
         * @param null $module
         * @param null $scope
         * @param null $section
         * @param array $alternating_priority
         * @return array
         */
        public function getStorerClassPath($storer, $module=null, $scope=null,  $section=null, $alternating_priority=[]){

            return $this->getObjectClassPath($storer, "storer", $module, $scope, $section, $alternating_priority);

        }




        // region SUB-FUNCTIONS

        /**
         * object_type values renderable, storer
         *
         * @param string $object_name
         * @param string $object_type
         * @param null $module
         * @param null $scope
         * @param null $section
         * @param array $alternating_priority
         * @return array
         */

        private function getObjectClassPath($object_name, $object_type , $module=null, $scope=null,  $section=null, $alternating_priority=[]){
            
            // example = $scope = app $module = Content $section = Homepage $object_name = Sitemap $object_type = Renderables
            
            $alternating_priority = is_array($alternating_priority) ? $alternating_priority : [];

            $object_name    = is_array( $object_name)   ? $object_name  :   [$object_name];
            $module         = is_array( $module)        ? $module       :   [$module];
            $scope          = is_array( $scope)         ? $scope        :   [$scope];
            $section        = is_array( $section)       ? $section      :   [$section];

            $object_name    = count(    $object_name)   ? $object_name  :   [null];
            $module         = count(    $module)        ? $module       :   [null];
            $scope          = count(    $scope)         ? $scope        :   [null];
            $section        = count(    $section)       ? $section      :   [null];

            $alternating_priority = array_intersect($alternating_priority,$this->alternating_priority);
            if(count($alternating_priority)!=4){
                $alternating_priority = array_merge($alternating_priority,array_diff($this->alternating_priority,$alternating_priority));
            }
            $ap=array_reverse($alternating_priority);

            $notfounds=[];

            //loop for every factor in an alternated priority order:
            foreach (${$ap[0]} as ${$ap[0]."_"}) {
                foreach (${$ap[1]} as ${$ap[1]."_"}) {
                    foreach (${$ap[2]} as ${$ap[2]."_"}) {
                        foreach (${$ap[3]} as ${$ap[3]."_"}) {
                            $check = $this->_getObjectClassPath(${"object_name_"}, $object_type, ${"module_"}, ${"scope_"}, ${"section_"});
                            if ($check[0]===true) {
                                return [true, $check[1]];
                            }
                            $notfounds[] = $check[1];
                        }
                    }
                }
            }
            return [false, $notfounds];
        }

        /**
         * @param string $object_name
         * @param string $object_type
         * @param null $module
         * @param null $scope
         * @param null $section
         * @return array
         */
        private function _getObjectClassPath($object_name, $object_type, $module=null, $scope=null,  $section=null ):array{
            $return = [false, ""];

            $namespacepart="";

            switch($object_type){
                case "renderable":
                    $namespacepart="Renderables";
                    break;
                case "storer":
                    $namespacepart="Storers";
                    break;
                default:
                    $return = [false, ""];
                    break;
            }

            $pathpieces = [];

            if($scope){$pathpieces[] = "\\$scope\\Modules";}
            if($module){
                $pathpieces[] = "$module\\AllBuilder\\$namespacepart";
            }

            if($section){$pathpieces[] = "$section";}
            $pathpieces[] = "$object_name";

            $pathbuild=implode("\\",$pathpieces);

            $return[1] = $pathbuild;

            if(class_exists($pathbuild)){
                $return[0] = true;
            }

            return $return;


        }


        // endregion

    }
