<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 22.09.2018
 * Time: 18:30
 */

namespace Mediapress\AllBuilder\Foundation;

use Illuminate\Support\Str;
use Mediapress\Facades\DataSourceEngine;
use Mediapress\Foundation\HtmlElement;
use Cache;

/**
 * Class BuilderRenderable
 * @package Mediapress\AllBuilder\Foundation
 * @author erayera@gmail.com
 * @copyright Mediaclick
 * @version 1.0.0
 */
class BuilderRenderable
{
    //region Constants
    public const BUILDER_RENDERABLE_CLASS_PATH = "Mediapress\\AllBuilder\\Foundation\\BuilderRenderable";
    public const DATASOURCE_CLASS_PATH = "Mediapress\\Foundation\\DataSource";
    public const FUNCTION_TYPE_INLINE_HEAD = "func:";
    public const DATASOURCE_TYPE_INLINE_HEAD = "ds:";
    public const RENDERABLE_TYPES_STR = "renderable_types";
    public const REGEX_LOOKUPS = [
        "print" => '/(<print(?:(:)?([^>]*))?>)((&?[a-zA-Z_]+[a-zA-Z0-9_]*)+(?:->|\.?[a-zA-Z0-9_]+)*)(<\/print>)+/',
        "var" => '/^<var>((&?[a-zA-Z_]+[a-zA-Z0-9_]*)+(->|\.?[a-zA-Z0-9_]+)*)<\/var>$/'
    ];

    const COLLECTABLE_AS_STR = "collectable_as";
    const PARAMS = "params";
    const CONTENTS_STR = "contents";
    const CLASS_STR = "class";
    const STACKS_STR = "stacks";
    const FUNCTION_STR = "function";
    const CALLABLE_STR = "callable";
    const ITEMS = "items";
    public const DESCRIPTION_STR = "description";
    public const CUSTOM_TEMPLATE = "custom_template";
    public const ATTRIBUTES = "attributes";
    public const INPUT_TEXT = "input_text";
    public const DEFAULT_VALUE = "default_value";
    public const OPTIONS = "options";

    public $backup_load_callback="bl_input_val";
    // for text based inputs : bl_input_value
    // for select boxes : bl_selected
    // for check boxes : bl_checked
    // for meta inputs : bl_meta
    // for mfiles : bl_mfile

    //endregion

    // region Properties

    public $ignored_if = false;

    /**
     * A bucket to collect logs when desired.
     * @var array
     */
    public $logs = [];

    /**
     * A bucket to collect warnings when desired.
     * @var array
     */
    public $warnings = [];

    /**
     * Holds errors collected during processes.
     * @var array
     */
    public $errors = [];

    /**
     * Assigned with Class path of this renderable.
     * @var null|string
     */
    public $class = __CLASS__;

    /**
     * Will hold a generated unique key for this class
     * @var null | string
     */
    public $class_id = null;

    /**
     * Holds string values (tags) which this "renderable" can be collected in.
     * Tags which a renderable suits itself will be listed in $collectable_as prop.
     * So a renderable can identify, filter and collect other renderables inside itself or another renderable by a tag.
     * @var array
     */
    public $collectable_as = [];

    /**
     * Predefined parameters. No need to make it callable yet.
     * @var array
     */
    public $params = [
    ];

    /**
     * Holds overwritten/extended contents.
     * @var array
     */
    public $contents = [];

    /**
     * Holds overwritten/extended options.
     * @var array|null
     */
    public $options = [];

    /**
     * Holds overwritten/extended data.
     * @var array|null
     */
    public $data = [];


    /**
     * Holds group keys which an extended object may fall under one or more
     */
    public const  OBJECT_TAGS = [
        "form",
        "object_specific",
        "flow_control",
        "basic_html",
        "mediapress",
        "container",
        "miscellaneous",
        "bootstrap"
    ];



    public const DEFAULT_INFO = [
        "icon_key" => "cubes",
        "object_key" => "BuilderRenderable",
        "object_class" => __CLASS__,
        "object_tags"=>[
            //"form", "object_specific", "flow_control", "basic_html", "mediapress", "container", "	miscellaneous", "bootstrap"
        ],
        "name" => "Builder Renderable Örneği",
        self::DESCRIPTION_STR => "",
        self::ITEMS => [
            self::OPTIONS => [
                "key" => self::OPTIONS,
                "name" => "Ayarlar",
                self::DESCRIPTION_STR => "Nesne için ayarlar",
                "type" => "tab",
                self::CUSTOM_TEMPLATE => "",
                self::ITEMS => [
                    "html" => [
                        "key" => "html",
                        "name" => "HTML",
                        self::DESCRIPTION_STR => "",
                        "type" => "group",
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom
                        self::ITEMS => [
                            "tag" => [
                                "key" => "tag",
                                "name" => "HTML Tag",
                                self::DESCRIPTION_STR => "",
                                "type" => self::INPUT_TEXT,
                                self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                                self::DEFAULT_VALUE => "div"
                            ],
                            self::ATTRIBUTES => [
                                "key" => self::ATTRIBUTES,
                                "name" => "HTML Attributes of the Element",
                                self::DESCRIPTION_STR => "Doğrudan HTML Attr. Ayarları",
                                "type" => "group",
                                self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                                self::ITEMS => [
                                    self::CLASS_STR => [
                                        "key" => self::CLASS_STR,
                                        "name" => "HTML Class",
                                        self::DESCRIPTION_STR => "",
                                        "type" => self::INPUT_TEXT,
                                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom
                                        self::DEFAULT_VALUE => ""
                                    ],
                                    "name" => [
                                        "key" => "name",
                                        "name" => "HTML Name",
                                        self::DESCRIPTION_STR => "",
                                        "type" => self::INPUT_TEXT,
                                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom
                                        self::DEFAULT_VALUE => ""
                                    ]
                                ]
                            ]
                        ]

                    ],
                    "rules"=>[
                        "key" => "rules",
                        "name" => "Kurallar",
                        self::DESCRIPTION_STR => "Geçerlilik kuralları (Validasyon)",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ""
                    ]

                ]
            ],
            "data" => [
                "key" => "data",
                "name" => "Veriler",
                self::DESCRIPTION_STR => "Nesnenin kullanacağı veriler",
                "type" => "tab",
                self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom
                self::ITEMS => []
            ],
            self::PARAMS => [
                "key" => self::PARAMS,
                "name" => "Parametreler",
                self::DESCRIPTION_STR => "Nesnenin kullanacağı parametreler",
                "type" => "tab",
                self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom
                self::ITEMS => []
            ]
        ],
    ];


    public $info = [];

    /**
     * Groups data to be gathered and then pushed to stacks manually.
     * @var array
     */
    public static $stacks = [];

    /**
     * Keeps signal of if contents converted into renderables or not.
     * @var bool
     */
    protected $are_contents_converted = false;

    /**
     * Contents which converted by "parseContentsToRenderables" are held in this variable for caching purpose.
     * @var array
     */
    public $converted_contents = [];

    //endregion

    // region Constructor

    /**
     * BuilderRenderable constructor.
     * @param array $params
     * @param array $contents
     * @param array $options
     */
    public function __construct(array $params = [], array $contents = [], array $options = [], array $data = [])
    {
        $this->class = get_class($this);

        $this->params = array_replace_recursive($this->params, array_replace_recursive($this->defaultParams(), $params));
        $this->params = $this->parseAnnotations($this->params, true);
        $this->options = array_replace_recursive(
            $this->baseOptions(),
            array_replace_recursive(
                $this->options,
                array_replace_recursive(
                    $this->defaultOptions(),
                    $options)
            )
        );
        $this->options = $this->parseAnnotations($this->options, true);


        $this->data = array_replace_recursive($this->data, array_replace_recursive($this->defaultData(), $data));
        $this->data = $this->parseAnnotations($this->data, true);


        $this->params = $this->processData($this->params);
        $this->options = $this->processData($this->options);

        $this->data = $this->processData($this->data);

        $this->contents = array_replace_recursive(
            $this->contents, array_replace_recursive(
                $this->defaultContents(),
                $contents
            )
        );
        $this->contents = $this->parseAnnotations($this->contents);

        if (!count($this->info)) {
            $this->warnings[] = "Bu sınıf için lütfen \"info\" verisi sağlayın: " . get_class($this);
        }

        $this->info = array_replace_recursive(self::DEFAULT_INFO, $this->info);

        if (isset($this->options[self::COLLECTABLE_AS_STR])) {
            $this->collectable_as = array_replace($this->collectable_as, $this->options[self::COLLECTABLE_AS_STR]);
        }

        $this->mergeForcedHTMLClasses();

        $this->setClassIdAuto();
        $this->isIgnored();
    }


    //endregion

    // region Decorators
    /**
     * Default and overwritable/extendable contents.
     *
     * @return array
     */
    public function defaultContents()
    {
        return [];
    }

    /**
     * Default and overwritable/extendable params.
     *
     * @return array
     */
    public function defaultParams()
    {
        return [];
    }


    /**
     * Default and overwritable/extendable options.
     *
     * @return array
     */
    public function defaultOptions(){
        return [];
    }

    /**
     * Default and overwritable/extendable options.
     *
     * @return array
     */
    public function baseOptions()
    {
        return [
            "html" => [
                "tag" => "div",
                "void_element" => false
            ],
            "forced_html_classes"=>[

            ],
            self::RENDERABLE_TYPES_STR => [
                "a" => "Mediapress\\AllBuilder\\Renderables\\Anchor",
                "anchor" => "Mediapress\\AllBuilder\\Renderables\\Anchor",
                "accordion" => "Mediapress\\AllBuilder\\Renderables\\Accordion",
                "accordionpane" => "Mediapress\\AllBuilder\\Renderables\\AccordionPane",
                "alert" => "Mediapress\\AllBuilder\\Renderables\\Alert",
                "blank" => "Mediapress\\AllBuilder\\Renderables\\Blank",
                "blankwithlabel" => "Mediapress\\AllBuilder\\Renderables\\BlankWithLabel",
                "bluetitle" => "Mediapress\\AllBuilder\\Renderables\\BlueTitle",
                "button" => "Mediapress\\AllBuilder\\Renderables\\Button",
                "ckeditor" => "Mediapress\\AllBuilder\\Renderables\\CKEditor",
                "clearfix" => "Mediapress\\AllBuilder\\Renderables\\Clearfix",
                "conditioner" => "Mediapress\\AllBuilder\\Renderables\\Conditioner",
                "contentprotectioncontrol" => "Mediapress\\AllBuilder\\Renderables\\ContentProtectionControl",
                "countryselect" => "Mediapress\\AllBuilder\\Renderables\\CountrySelect",
                "detailmetascontrol" => "Mediapress\\AllBuilder\\Renderables\\DetailMetasControl",
                "detailmetascontrolv2" => "Mediapress\\AllBuilder\\Renderables\\DetailMetasControlV2",
                "detailsaccordion" => "Mediapress\\AllBuilder\\Renderables\\DetailsAccordion",
                "detailslugcontrol" => "Mediapress\\AllBuilder\\Renderables\\DetailSlugControl",
                "detailslugcontrolv2" => "Mediapress\\AllBuilder\\Renderables\\DetailSlugControlV2",
                "detailtabs" => "Mediapress\\AllBuilder\\Renderables\\DetailTabs",
                "em" => "Mediapress\\AllBuilder\\Renderables\\Em",
                "div" => "Mediapress\\AllBuilder\\Renderables\\Div",
                "faicon" => "Mediapress\\AllBuilder\\Renderables\\FaIcon",
                "foreach" => "Mediapress\\AllBuilder\\Renderables\\ForEacher",
                "form" => "Mediapress\\AllBuilder\\Renderables\\Form",
                "formgroup" => "Mediapress\\AllBuilder\\Renderables\\FormGroup",
                "icheck" => "Mediapress\\AllBuilder\\Renderables\\iCheck",
                "img" => "Mediapress\\AllBuilder\\Renderables\\Img",
                "input" => "Mediapress\\AllBuilder\\Renderables\\Input",
                "scriptpusher" => "Mediapress\\AllBuilder\\Renderables\\ScriptPusher",
                "inputwithimgbtn" => "Mediapress\\AllBuilder\\Renderables\\InputWithImgBtn",
                "inputwithlabel" => "Mediapress\\AllBuilder\\Renderables\\InputWithLabel",
                "iradio" => "Mediapress\\AllBuilder\\Renderables\\iRadio",
                "iradiosamigos" => "Mediapress\\AllBuilder\\Renderables\\iRadiosAmigos",
                "label" => "Mediapress\\AllBuilder\\Renderables\\Label",
                "li" => "Mediapress\\AllBuilder\\Renderables\\ListItem",
                "listitem" => "Mediapress\\AllBuilder\\Renderables\\ListItem",
                "mpfile" => "Mediapress\\AllBuilder\\Renderables\\MPFile",
                "ol" => "Mediapress\\AllBuilder\\Renderables\\OrderedList",
                "orderedlist" => "Mediapress\\AllBuilder\\Renderables\\OrderedList",
                "option" => "Mediapress\\AllBuilder\\Renderables\\Option",
                "pagecategoriescontrol" => "Mediapress\\AllBuilder\\Renderables\\PageCategoriesControl",
                "pagecriteriascontrol" => "Mediapress\\AllBuilder\\Renderables\\PageCriteriasControl",
                "pagepropertiescontrol" => "Mediapress\\AllBuilder\\Renderables\\PagePropertiesControl",
                "pagestatuscontrol" => "Mediapress\\AllBuilder\\Renderables\\PageStatusControl",
                "sitemapstatuscontrol" => "Mediapress\\AllBuilder\\Renderables\\SitemapStatusControl",
                "categorystatuscontrol" => "Mediapress\\AllBuilder\\Renderables\\CategoryStatusControl",
                "contentstatuscontrol" => "Mediapress\\AllBuilder\\Renderables\\ContentStatusControl",
                "row" => "Mediapress\\AllBuilder\\Renderables\\Row",
                "radio" => "Mediapress\\AllBuilder\\Renderables\\Radio",
                "radiosamigos" => "Mediapress\\AllBuilder\\Renderables\\RadiosAmigos",
                "select" => "Mediapress\\AllBuilder\\Renderables\\Select",
                "selectwithlabel" => "Mediapress\\AllBuilder\\Renderables\\SelectWithLabel",
                "simpletabs" => "Mediapress\\AllBuilder\\Renderables\\SimpleTabs",
                "span" => "Mediapress\\AllBuilder\\Renderables\\Span",
                "small" => "Mediapress\\AllBuilder\\Renderables\\Small",
                "steptabs" => "Mediapress\\AllBuilder\\Renderables\\StepTabs",
                "tab" => "Mediapress\\AllBuilder\\Renderables\\Tab",
                "textarea" => "Mediapress\\AllBuilder\\Renderables\\Textarea",
                "textareawithlabel" => "Mediapress\\AllBuilder\\Renderables\\TextareaWithLabel",
                "toggleswitch" => "Mediapress\\AllBuilder\\Renderables\\ToggleSwitch",
                "ul" => "Mediapress\\AllBuilder\\Renderables\\UnorderedList",
                "provinceselect" => "Mediapress\\AllBuilder\\Renderables\\ProvinceSelect",
                //"formcheck" => "Mediapress\\AllBuilder\\Renderables\\FormCheck",
            ],
            "object_types" => $this->cachedCreateBuilderRenderableObjects("Content"),
            self::COLLECTABLE_AS_STR => []
        ];
    }

    public function cachedCreateBuilderRenderableObjects($type) {

        return Cache::store('file_not_deleted')->remember('createBuilderRenderableObjects' , 24*60*60, function() use($type) {
            return createBuilderRenderableObjects($type);
        });

    }

    /**
     * Default and overwritable/extendable data.
     * @return array
     */
    public function defaultData()
    {
        return [];
    }

    /**
     * Computes if this class is ignored or not
     */
    protected function isIgnored()
    {
        $ii = $this->options["ignored_if"] ?? null;
        if (!is_null($ii)) {
            if ($ii === true || $ii === false) {
                $this->ignored_if = $ii;
            } elseif (is_array($ii)) {
                $this->ignored_if = $ii = $this->processConditionsArray($ii);
            }
        } else {
            $this->ignored_if = false;
        }
        return $this->ignored_if;
    }

    // endregion

    // region Builders & Converters

    /**
     * Builds a renderable object from data of an array
     * @param array $content
     * @return bool| Mediapress\AllBuilder\Foundation\BuilderRenderable
     */
    protected function buildRenderableFromArray(array $content)
    {

        $this->handleWillBuildRenderableFromArray($content);
        if (is_array($content)) {
            if (!isset($content["type"])) {
                $this->errors[] = "Dizi, geçerli yapıda değil. Type parametresi bulunamadı: " . serialize($content);
                return false;
            }
            $type = isset($content["type"]) ? $content["type"] : null;
            $renderable_types = $this->options[self::RENDERABLE_TYPES_STR];

            if (!array_key_exists($type, $renderable_types)) {
                $this->errors[] = "$type için belirtilmiş bir renderable yolu yok.";
                return false;
            }

            /** @var BuilderRenderable $renderable */
            $renderable = new $renderable_types[$type](
                ($content[self::PARAMS] ?? []),
                ($content[self::CONTENTS_STR] ?? []),
                ($content[self::OPTIONS] ?? []),
                ($content["data"] ?? [])
            );

            if ($renderable->convertContentsToRenderables()) {
                $this->handleBuiltRenderableFromArray($content, $renderable);
                return $renderable;
            } else {
                $this->errors = array_merge($this->errors, $renderable->errors);
                //dump($this->errors);

                return false;
            }
        } else {
            $this->errors[] = "Veri, işlenebilir biçimde değil. Bu: " . get_class($this) . " Tip: " . gettype($content) . (is_object($content) ? ",  Sınıf: " . get_class($content) : "");

            return false;
        }
    }

    /**
     * Converts definition arrays into Renderable classes
     *
     * @return bool
     */
    protected function convertContentsToRenderables()
    {


        $this->converted_contents = [];
        $this->are_contents_converted = false;
        $error = false;
        $this->handleConvertingContentsToRenderables($this->contents);
        foreach ($this->contents as $tag => $content) {
            $this->handleConvertingContentToRenderable($tag,$content);
            if (is_string($content) || is_numeric($content) || is_bool($content)) {
                $renderable = $content . "";
                $this->handleConvertedContentToRenderable($tag,$content, $renderable);
                $this->converted_contents[$tag] = $renderable;
            } else if (is_a($content, "Mediapress\\Foundation\\HTMLElement")) {
                /** @var HtmlElement $renderable */
                $renderable = $content;
                $this->handleConvertedContentToRenderable($tag,$content, $renderable);
                $this->converted_contents[$tag] = $renderable;
            } else if (is_a($content, self::BUILDER_RENDERABLE_CLASS_PATH)) {
                /** @var BuilderRenderable $renderable */
                $renderable = $content;
                if ($renderable->convertContentsToRenderables()) {
                    if (!$renderable->ignored_if) {
                        $this->handleConvertedContentToRenderable($tag,$content, $renderable);
                        $this->converted_contents[$tag] = $renderable;
                    }
                } else {
                    $this->errors = array_merge($this->errors, $renderable->errors);
                    $error = true;
                }
            } else if (is_array($content)) {

                if (!isset($content["type"])) {
                    $this->errors[] = "Dizi, geçerli yapıda değil. Type parametresi bulunamadı: " . serialize($content);
                    $error = true;
                    break;
                }
                $type = isset($content["type"]) ? $content["type"] : null;
                $renderable_types = $this->options[self::RENDERABLE_TYPES_STR];

                if (!array_key_exists($type, $renderable_types)) {
                    $this->errors[] = "$type için belirtilmiş bir renderable yolu yok.";
                    $error = true;
                    break;
                }

                /** @var BuilderRenderable $renderable */
                $renderable = new $renderable_types[$type](
                    ($content[self::PARAMS] ?? []),
                    ($content[self::CONTENTS_STR] ?? []),
                    ($content[self::OPTIONS] ?? []),
                    ($content["data"] ?? [])
                );

                if ($renderable->convertContentsToRenderables()) {
                    if (!$renderable->ignored_if) {
                        $this->handleConvertedContentToRenderable($tag,$content, $renderable);
                        $this->converted_contents[$tag] = $renderable;
                    }
                } else {
                    $this->errors = array_merge($this->errors, $renderable->errors);
                    $error = true;
                }
            } else {
                $this->errors[] = "Veri, işlenebilir bir biçimde değil. Bu: " . get_class($this) . " Tip: " . gettype($content) . (is_object($content) ? ",  Sınıf: " . get_class($content) : "");
                $error = true;
                break;
            }

        }
        if ($error) {
            $this->converted_contents = [];
        } else {
            $this->handleConvertedContentsToRenderables($this->contents, $this->converted_contents);
        }

        return $this->are_contents_converted = !$error;
    }

    /**
     * Function which builds HtmlElement()  using given configuration.
     *
     * @return HtmlElement
     */
    protected function createHTMLElement(array $data)
    {

        $isvoid = $data["html"]["void_element"] ?? false;
        $el = new HtmlElement((isset($data["html"]["tag"]) ? $data["html"]["tag"] : null), $isvoid);
        if (!isset($data["html"])) {
            return $el;
        }
        $attributes = $data["html"][self::ATTRIBUTES] ?? null;
        if ($attributes) {
            $cls = isset($attributes[self::CLASS_STR]) ?
                array_splice($attributes, array_search(self::CLASS_STR, array_keys($attributes)), 1)[self::CLASS_STR] :
                null;
            if ($cls) {
                $el->add_class($cls);
            }
            foreach ($attributes as $attr => $val) {
                $el->add_attr($attr, $val);
            }
        }

        return $el;
    }

    /**
     * Function which builds HtmlElement() from itself using given configuration.
     * Existence purpose is reducing code load in getHtmlElement function and making it overwritable with ease as it is planned to be overwritten mostly.
     *
     * @return HtmlElement
     */
    protected function getSelfHtmlElement()
    {
        $this->handleWillGetSelfHtmlElement();
        $this->mergeStacks();
        $el = $this->createHTMLElement($this->options);
        $this->handleGotSelfHtmlElement($el);
        return $el;
    }

    /**
     * Collects "HTMLElement builds" of contents (which will be parsed if not been yet)
     * @return array
     */
    public function getContentHtmlElements()
    {

        $collected = [];
        if (!$this->are_contents_converted) {
            $this->convertContentsToRenderables();
        }
        $this->handleWillGetContentsHtmlElements($this->converted_contents);

        foreach ($this->converted_contents as $tag => $content) {
            $this->handleWillGetContentsHtmlElement($content);
            if (is_a($content, self::BUILDER_RENDERABLE_CLASS_PATH)) {
                /** @var BuilderRenderable $content */
                $htmlelement = $content->getHtmlElement();
            } else {
                $htmlelement = $content;
            }
            $this->handleGotContentsHtmlElement($content, $htmlelement);
            $collected[$tag] = $htmlelement;
        }

        $this->handleGotContentsHtmlElements($collected);

        return $collected;
    }

    /**
     * Adds HTMLElement contents into self HTMLElement and returns it. Allows managing contents or self with ease by overwriting just before serving.
     * @return HtmlElement
     */
    public function getHtmlElement()
    {
        $this->handleWillGetHtmlElement();
        if ($this->ignored_if) {
            //TODO: Code in needed lines: eleminate ignored items , so they won't count in actions like collect, validate etc.
            return (new HtmlElement());
        }
        /** @var HtmlElement $el */
        $el = $this->getSelfHtmlElement();
        $_contents = $this->getContentHtmlElements();
        $el->add_content($_contents);
        $this->handleGotHtmlElement($el);
        return $el;
    }

    //endregion

    // region Helpers & Walkers

    public function mergeForcedHTMLClasses(){

        $forced_html_classes = $this->options["forced_html_classes"] ?? [];
        if(is_array($forced_html_classes) && count($forced_html_classes)){
            $current_classes = preg_split('/\s/', data_get($this->options, "html.attributes.class", ""));
            $renewed_classes = implode(" ", array_unique(array_merge($current_classes, $forced_html_classes)));
            data_set($this->options, "html.attributes.class", $renewed_classes);
        }
    }

    public function addClasses($classes, $object=null){
        if(is_null($object)){
            $object = $this;
        }
        if(!is_array($classes) && ! is_string($classes) && ! is_numeric($classes)){
            return;
        }
        if(is_numeric($classes)){
            $_classes = [$classes.""];
        }elseif(is_string($classes)){
            $_classes = preg_split('/\s/',$classes);
        }else{
            $_classes = $classes;
        }

        $_classes = array_filter($_classes);
        //dump($classes, $_classes);
        if(count($_classes)){
            $current_classes = preg_split('/\s/', data_get($object, "options.html.attributes.class", ""));
            //dump($current_classes);
            $renewed_classes = implode(" ", array_unique(array_merge($current_classes, $_classes)));
            data_set($object, "options.html.attributes.class", $renewed_classes,true);
            //dump(data_get($object, "options.html.attributes.class","çükübik"));
        }

        return $object;
    }

    public function removeClasses($classes, $object = null){
        if(is_null($object)){
            $object = $this;
        }
        if(!is_array($classes) && ! is_string($classes) && ! is_numeric($classes)){
            return;
        }
        if(is_numeric($classes)){
            $_classes = [$classes.""];
        }elseif(is_string($classes)){
            $_classes = preg_split('/\s/',$classes);
        }else{
            $_classes = $classes;
        }

        $_classes = array_filter($_classes);
        if(count($_classes)){
            $current_classes = preg_split('/\s/', data_get($object, "options.html.attributes.class", ""));
            $renewed_classes = array_unique(array_diff($current_classes, $_classes));
            data_set($object, "options.html.attributes.class", $renewed_classes,true);
        }

        return $object;
    }

    public function forceClass($classes, $object=null){
        return $this->addClasses($classes, $object);
    }

    public function forceClassOnRenderableArray($object_array, $classes){
        return $this->forceClass($classes, $object_array);
    }


    /**
     * Merges Stacks to static
     */
    protected function mergeStacks()
    {

        if (isset($this->data[self::STACKS_STR]) && is_array($this->data[self::STACKS_STR]) && count($this->data[self::STACKS_STR])) {
            foreach ($this->data[self::STACKS_STR] as $stack_key => $stack_data) {
                if (!isset(static::$stacks[$stack_key])) {
                    static::$stacks[$stack_key] = [];
                }
                static::$stacks[$stack_key][$this->class_id] = $stack_data;
            }
        }
    }

    /**
     * @param $stack_name string
     * @param $additional_value string
     */
    public function extendStack(string $stack_name, string $additional_value)
    {
        $current_value = data_get($this->data, "stacks." . $stack_name, null);
        data_set($this->data, "stacks." . $stack_name, $current_value . "\n" . $additional_value, true);
    }


    /**
     * Creates class id for this class
     * @param bool $overwrite
     * @return null|string
     */
    protected function createClassID($overwrite = false)
    {
        if ($this->class_id && !$overwrite) {
            return $this->class_id;
        }
        $class_short_name = explode('\\', get_class($this));
        $class_short_name = array_pop($class_short_name);
        return $this->class_id = strtolower($class_short_name) . "-" . Str::random(10);
    }

    /**
     * @param array $conditions
     * @return bool
     */
    protected function processConditionsArray(array $conditions)
    {

        if (count($conditions) < 3) {
            return false;
        }
        $item1 = is_array($conditions[0]) ? $this->processConditionsArray($conditions[0]) : $conditions[0];
        $item2 = is_array($conditions[1]) ? $this->processConditionsArray($conditions[1]) : $conditions[1];
        $operand = $conditions[2];

        switch ($operand) {
            case "&&":
                $return = ($item1 && $item2);
                break;
            case "||":
                $return = ($item1 || $item2);
                break;
            case "==":
                $return = ($item1 == $item2);
                break;
            case "===":
                $return = ($item1 === $item2);
                break;
            case "!=":
                $return = ($item1 != $item2);
                break;
            case "!==":
                $return = ($item1 !== $item2);
                break;
            case "&":
                $return = ($item1 & $item2);
                break;
            case "|":
                $return = ($item1 | $item2);
                break;
            case ">":
                $return = ($item1 > $item2);
                break;
            case ">=":
                $return = ($item1 >= $item2);
                break;
            case "<":
                $return = ($item1 < $item2);
                break;
            case "<=":
                $return = ($item1 <= $item2);
                break;
            case "instanceof":
                $return = ($item1 instanceof $item2);
                break;
            default:
                $return = false;
        }

        return $return;

    }

    /**
     * Trigger recursive parsing of data
     * @param $data
     * @return mixed
     */
    protected function processData($data)
    {
        return $this->dataProcessor($data);

    }

    /**
     * Parse "parseble" arrays.
     * @param $data
     * @return mixed
     */
    protected function dataProcessor($data) // recursive
    {

        extract($this->params);

        $result = $data;

        if (is_array($data)) {

            // process all nodes at first
            foreach ($data as &$d) {
                $d = $this->dataProcessor($d);
            }
            // then process self

            //types recognized:
            // function | function_arr | own_function | own_function_arr | ds | datasource | function_on_item | func:<funcname> | ds:<dsmodule>-><dsname>

            if (isset($data["type"])) {
                if ($data["type"] == self::FUNCTION_STR) {
                    $result = call_user_func($data[self::CALLABLE_STR], ...(array_values($data[self::PARAMS] ?? [])));
                } else if ($data["type"] == "function_arr") {
                    $result = call_user_func_array($data[self::CALLABLE_STR], ($data[self::PARAMS] ?? []));
                } else if ($data["type"] == "own_function") {
                    $result = call_user_func([$this, $data[self::CALLABLE_STR]], ...(array_values($data[self::PARAMS] ?? [])));
                } else if ($data["type"] == "own_function_arr") {
                    $result = call_user_func_array([$this, $data[self::CALLABLE_STR]], ($data[self::PARAMS] ?? []));
                } else if (in_array($data["type"], ["function_on_item", "foi"]) && isset($data["item"]) && isset($data[self::FUNCTION_STR])) {
                    $func = $data[self::FUNCTION_STR];
                    $args = $data[self::PARAMS] ?? [];
                    $result = $data["item"]->$func(...array_values($args));
                } else if (\Str::startsWith($data["type"], self::FUNCTION_TYPE_INLINE_HEAD)) {
                    $prefix = self::FUNCTION_TYPE_INLINE_HEAD;
                    $str = $data["type"];
                    $func = preg_replace('/^' . preg_quote($prefix, '/') . '/', '', $str);
                    if (function_exists($func)) {
                        $args = ($data[self::PARAMS] ? array_values($data[self::PARAMS]) : []);
                        $result = $func(...$args);
                    } else if (method_exists($this, $func)) {
                        $result = call_user_func([$this, $func], ...array_values(($data[self::PARAMS] ?? [])));
                    }
                } else if (\Str::startsWith($data["type"], self::DATASOURCE_TYPE_INLINE_HEAD) && strpos($data["type"], "->") !== false) {
                    $prefix = self::DATASOURCE_TYPE_INLINE_HEAD;
                    $str = $data["type"];
                    $data[self::PARAMS] = isset($data[self::PARAMS]) ? $data[self::PARAMS] : [];
                    $dspath = preg_replace('/^' . preg_quote($prefix, '/') . '/', '', $str);
                    $dspieces = preg_split('/->/', $dspath);
                    $result = DataSourceEngine::getDataSource($dspieces[0], $dspieces[1], ($data[self::PARAMS] ?? []))->getData();
                } else {
                    $result = $data;
                }
            } else {
                $result = $data;
            }
        } else if (is_a($data, self::DATASOURCE_CLASS_PATH)) {
            $result = $data->getData();
        } else if (is_closure($data)) {
            $result = $data();
        }
        return $result;
    }

    /**
     * Triggers recursive parsing of annotations by "annotationParser"
     *
     * @param array $data
     * @return array
     */
    protected function parseAnnotations(Array $data, $put_defaults = false): array
    {
        $params_ = [$put_defaults];
        return $this->array_map_recursive([$this, 'annotationParser'], $data, $params_);
    }

    /**
     * Recursive version of array_map.
     *
     * @param $callback
     * @param $array
     * @return array
     */
    protected function array_map_recursive($callback, $array, $params = null)
    {
        $params = is_array($params) ? $params : [];
        $func = function ($item) use (&$func, &$callback, &$params) {
            $params_ = $params;
            array_unshift($params_, $item);
            return is_array($item) ?
                array_map($func, $item) :
                call_user_func_array($callback, $params_);
        };

        return array_map($func, $array);
    }

    /**
     * Recursive version of array_map.
     *
     * @param $callback
     * @param $array
     * @return array
     */
    protected function array_map_arrays_recursive($callback, $array)
    {
        $func = function ($item) use (&$func, &$callback) {
            return is_array($item) ? call_user_func($callback, $item) : $item;
        };

        return array_map($func, $array);
    }

    /**
     * @param $callback
     * @param $raw_renderable_array
     * @return mixed
     */
    protected function map_raw_contents($callback, $raw_renderable_array)
    {

        $raw_renderable_array = call_user_func($callback, $raw_renderable_array);
        $contents_ = $raw_renderable_array[self::CONTENTS_STR] ?? [];

        if (count($contents_)) {
            foreach ($contents_ as &$content) {
                $content = is_array($content) ? $this->map_raw_contents($callback, $content) : $content;
            }
        }
        $raw_renderable_array[self::CONTENTS_STR] = $contents_;
        return $raw_renderable_array;
    }

    /**
     * Replaces annotations to their opposing values. put_defaults is false at default because only non-contents array items should be parsed with defaults.
     *
     * @param $item
     * @param boolean $put_defaults
     * @return array|mixed|null
     */
    protected function annotationParser($item, $put_defaults = false)
    {


        if (!is_string($item)) {
            return $item;
        }


        extract($this->params);
        $original_item = $item;

        $matches = [];
        preg_match_all(self::REGEX_LOOKUPS["print"], $item, $matches);

        if (isset($matches[0][0])) {
            $matches_count = count($matches[0]);

            for ($i = 0; $i < $matches_count; $i++) {
                $pure_chain = $matches[4][$i];
                $default_value = $matches[3][$i];
                $has_default = $matches[2][$i] == ":";
                $chainrings = preg_split("/->/", $pure_chain);
                $broken = false;
                $virtual = null;

                foreach ($chainrings as $partind => $part) {
                    $byref = false;
                    if (is_null($virtual)) {
                        if ($part[0] == "&") {
                            $part = ltrim($part, "&");
                            $byref = true;
                        }
                        if (isset(${$part})) {
                            $byref ? $virtual =& ${$part} : $virtual = ${$part};
                        } else {
                            $broken = true;
                            break;
                        }
                    } else if (is_array($virtual) || is_a($virtual, 'Illuminate\Support\Collection')) {
                        if (isset($virtual["$part"])) {
                            $virtual = $virtual["$part"];
                        } else {
                            $broken = true;
                            break;
                        }
                    } else if (is_a($virtual, "Illuminate\\Database\\Eloquent\\Model")) {
                        if (isset($virtual->$part) || in_array($part, array_keys($virtual->getAttributes()))) {
                            $virtual = $virtual->$part;
                        } else {
                            if ($partind == count($chainrings) - 1) {
                                $virtual = $virtual->$part;
                            } else {
                                $broken = true;
                                break;
                            }
                        }
                    } else if (is_object($virtual)) {
                        if (isset($virtual->$part)) {
                            $virtual = $virtual->$part;
                        } else {
                            $broken = true;
                            break;
                        }
                    } else {
                        $broken = true;
                        break;
                    }
                }

                if (!$broken) {
                    $item = str_replace($matches[0][$i], $virtual, $item);
                } else {
                    if ($put_defaults && $has_default) {
                        $item = str_replace($matches[0][$i], $default_value, $item);
                    }
                }
            }

            if ($item !== $original_item) {
                $item = $this->annotationParser($item, $put_defaults);
            } else {
                return $this->varParser($item, $put_defaults);
            }
        }

        return $this->varParser($item,$put_defaults);

    }

    /**
     * Replaces a variable annotation with variable itself.
     * @param $item
     * @return array|mixed|null
     */
    protected function varParser($item,$put_defaults)
    {

        if (!is_string($item)) {
            return $item;
        }

        extract($this->params);

        $matches = [];
        preg_match(self::REGEX_LOOKUPS["var"], $item, $matches);

        if (count($matches)) {

            $pure_chain = $matches[1];
            $chainrings = preg_split("/->/", $pure_chain);
            $broken = false;
            $virtual = null;

            foreach ($chainrings as $part) {
                $byref = false;
                if (is_null($virtual)) {
                    if ($part[0] == "&") {
                        $part = ltrim($part, "&");
                        $byref = true;
                    }
                    if (isset(${$part})) {
                        $byref ? $virtual = &${$part} : $virtual = ${$part};
                    } else {
                        $broken = true;
                        break;
                    }
                } else if (is_array($virtual) || is_a($virtual, 'Illuminate\Support\Collection')) {
                    if (isset($virtual["$part"])) {
                        $virtual = $virtual["$part"];
                    } else {
                        $broken = true;
                        break;
                    }
                } else if (is_a($virtual, "Illuminate\\Database\\Eloquent\\Model")) {
                    $has_it =!is_null($virtual->$part) ? $virtual->$part : null;
                    if (isset($virtual->$part) || in_array($part, array_keys($virtual->getAttributes())) || !is_null($has_it)) {
                        $virtual = $virtual->$part;
                    }else {
                        $broken = true;
                        break;
                    }
                } else if (is_object($virtual)) {
                    if (isset($virtual->$part)) {
                        $virtual = $virtual->$part;
                    } else {
                        $broken = true;
                        break;
                    }
                } else {
                    $broken = true;
                    break;
                }
            }

            if (!$broken) {
                $item = $virtual;
            }else{
                if($put_defaults){
                    return null;
                }
            }

        }

        return $item;
    }

    /**
     * Returns key (which is used as a short name) of an object which is recognized by BuilderRenderable
     * @param $object_class
     * @return string|null
     */
    public function getObjectKey($object_class)
    {
        return array_search($object_class, $this->options["object_types"]) ?? null;
    }


    public function getObjectTags($translate = false){
        $object_tags = self::OBJECT_TAGS;
        $result = [];
        foreach($object_tags as $tag){
            $result[$tag]= $translate ? trans("MPCorePanel::renderables.object_tags.".$tag) : str_replace("_", " ", \Str::title($tag))." Elements";
        }
        return $result;
    }
    // endregion

    //region Getters & Setters

    /**
     * Create and set class id and html id attribute.
     */
    public function setClassIdAuto()
    {
        $hai = 'html.attributes.id';

        $current_class_id = data_get($this->options, $hai, null);
        $this->handleWillSetClassIDAuto($current_class_id);
        data_set($this->options, $hai, $this->createClassID(), false);
        $new_class_id = data_get($this->options, $hai, null);
        $this->handleSetClassIDAuto($current_class_id, $new_class_id);
    }


    /**
     * Get contents array
     * @return array
     */
    public function getContents()
    {
        return $this->contents;
    }

    /**
     * Adds a new content or replaces an existing content with a new one.
     * @param $content
     * @param null $tag
     */
    public function setContent($content, $key = null)
    {
        if (!$key) {
            $key = count($this[self::CONTENTS_STR]);
        }
        $this[self::CONTENTS_STR][$key] = $content;
    }

    /**
     * Returns a content whose key is given.
     * @param string $tag
     * @return array|HtmlElement|null
     */
    public function getContent($key, $default_value = null)
    {
        return data_get($this->contents, $key, $default_value);
    }

    /**
     * Adds a new renderable_type or replaces an existing one with new one.
     * @param string $type_name
     * @param string $class_name
     * @return string
     */
    public function setRenderableType($type_name, $class_name)
    {
        return $this->renderable_types[$type_name] = $class_name;
    }

    /**
     * Returns "renderable class name" of given the short name.
     * @param string $type_name
     * @return string|null
     */
    public function getRenderableType($type_name)
    {
        return array_key_exists($type_name, $this->renderable_types) ? $this->renderable_types[$type_name] : null;
    }

    /**
     * Getter method of params property.
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param $key
     * @return mixed|null
     */
    public function getParam($key, $default_value = null)
    {
        return data_get($this->params, $key, $default_value);
    }

    /**
     * Set a param by key
     * @param $key
     * @param $value
     * @param bool $override
     */
    public function setParam($key, $value, $override = false)
    {
        data_set($this->params, $key, $value, $override);
    }

    /**
     * Getter method of options property.
     * @return array|null
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Get an option by key.
     * @param $key
     * @param $default_value
     * @return mixed
     */
    public function getOption($key, $default_value = null)
    {
        return data_get($this->options, $key, $default_value);
    }

    /**
     * Set an option by key
     * @param $key
     * @param $value
     * @param bool $override
     */
    public function setOption($key, $value, $override = false)
    {
        data_set($this->options, $key, $value, $override);
    }

    /**
     * Get data array.
     * @return array|mixed|null
     */
    public function getDatas()
    {
        return $this->data;
    }

    /**
     * Getter method of data property.
     * @param $key
     * @return mixed|null
     */
    public function getData($key, $default_value = null)
    {
        return data_get($this->data, $key, $default_value);
    }

    /**
     * Setter method of data property;
     * @param $key
     * @param $data
     * @return mixed
     */
    public function setData($key, $value, $override = false)
    {
        data_set($this->data, $key, $value, $override);
    }

    /**
     * Returns stacks
     * @return array
     */
    public function getStacks()
    {
        return static::$stacks;
    }

    public function getInfo()
    {
        return $this->info;
    }

    //endregion

    //region Tools for User
    /**
     * Collects "renderables" which belong to the given tag(key).
     * @param string $tag_which_item_is_collectable_for
     * @param bool $allow_tag_in_tag
     * @return array
     */
    public function collectItemsByTag($tag_which_item_is_collectable_for, $allow_tag_in_tag = false)
    {
        $collected = [];
        if (!$this->are_contents_converted) {
            $this->convertContentsToRenderables();
        }
        foreach ($this->converted_contents as $content) {
            if (!is_a($content, self::BUILDER_RENDERABLE_CLASS_PATH)) {
                continue;
            }
            if (in_array($tag_which_item_is_collectable_for, $content->collectable_as)) {
                $collected[] = $content;
                if ($allow_tag_in_tag) {
                    $collected = array_merge($collected, $content->collectItemsByTag($tag_which_item_is_collectable_for, $allow_tag_in_tag));
                }
            } else {
                $collected = array_merge($collected, $content->collectItemsByTag($tag_which_item_is_collectable_for, $allow_tag_in_tag));
            }
        }
        return $collected;
    }

    /**
     * Collects "renderables" which belong to the given short name.
     * @param string $type
     * @param bool $allow_type_in_type
     * @return array
     */
    public function collectItemsByType($type, $allow_type_in_type = false)
    {
        $collected = [];
        if (!$this->are_contents_converted) {
            $this->convertContentsToRenderables();
        }

        foreach ($this->converted_contents as $content) {

            $types = array_replace($content->options[self::RENDERABLE_TYPES_STR], $this->options[self::RENDERABLE_TYPES_STR]);
            $content_type = in_array($type, $types) ? array_search(get_class($content)) : null;

            if ($content_type) {
                $collected[] = $content;
                if ($allow_type_in_type) {
                    $collected = array_merge($collected, $content->collectItemsByType($type, $allow_type_in_type));
                }
            } else {
                $collected = array_merge($collected, $content->collectItemsByType($type, $allow_type_in_type));
            }
        }
        return $collected;
    }

    /**
     * Prints this by dumping its HTMLElement.
     */
    public function render($dump_errors_after = true)
    {
        $this->handleWillRenderRenderable($dump_errors_after);
        $dumped_str = $this->getHtmlElement()->give_html(0);
        $this->handleRenderedRenderable($dumped_str);
        echo $dumped_str;
        if (count($this->errors) && $dump_errors_after) {
            dump($this->errors);
        }
    }
    //endregion


    // region Default Event Handlers

    //------------ Convert Contents To Renderables

    //for all contents
    protected function handleConvertingContentsToRenderables(&$contents)
    {
        $contents;
    }

    //for each single content
    protected function handleConvertingContentToRenderable($tag, &$content)
    {
        $tag;
        $content;
    }

    //for all contents
    protected function handleConvertedContentsToRenderables(&$contents, &$renderables)
    {
        $contents;
        $renderables;
    }

    //for each single content
    protected function handleConvertedContentToRenderable($tag, &$content, &$renderable)
    {
        $tag;
        $content;
        $renderable;
    }

    //------------ Build Renderable From Array

    protected function handleWillBuildRenderableFromArray(array &$arr)
    {
        $arr;
    }

    protected function handleBuiltRenderableFromArray(array &$array, BuilderRenderable &$built)
    {
        $array;
        $built;
    }

    //------------ Get Contents Html Elements

    protected function handleWillGetContentsHtmlElements(&$contents) // not placed yet
    {
        $contents;
    }

    protected function handleWillGetContentsHtmlElement(&$content) // not placed yet
    {
        $content;
    }

    protected function handleGotContentsHtmlElements(&$htmlelements) // not placed yet
    {
        $htmlelements;
    }

    protected function handleGotContentsHtmlElement(&$content, &$htmlelement) // not placed yet
    {
        $content;
        $htmlelement;
    }

    //------------ Render & HTMElement (self)
    protected function handleWillRenderRenderable(&$dump_errors_after)
    {
        $dump_errors_after;
    }

    protected function handleRenderedRenderable(&$dumped_str)
    {
        $dumped_str;
    }

    protected function handleWillGetHtmlElement()
    {
    }

    protected function handleGotHtmlElement(HtmlElement &$htmlelement)
    {
        $htmlelement;
    }

    protected function handleWillGetSelfHtmlElement()
    {
    }

    protected function handleGotSelfHtmlElement(HtmlElement &$htmlelement)
    {
        $htmlelement;
    }


    //------------ Misc (self)
    protected function handleWillSetClassIDAuto($current_class_id)
    {
        $current_class_id;
    }

    protected function handleSetClassIDAuto($old_class_id, $new_class_id)
    {
        $old_class_id;
        $new_class_id;
    }

    // endregion


}
