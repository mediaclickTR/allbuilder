<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 15.10.2018
 * Time: 11:06
 */

namespace Mediapress\AllBuilder\Foundation;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Mediapress\FileManager\Models\MFileGeneral;
use Mediapress\Modules\Content\Models\PageDetailExtra;

class FormStorer
{

    const OBJECT_STR = "object";
    public const ACTION = "action";
    public const USE_PARAMS = "use_params";
    public const PARAMS = "params";
    public const INPUT = "input";
    public const EXTRAS = "->extras->";
    public const RELATION_FIELD = "relation_field";
    public const OBJECT_TYPES = "object_types";


    //region Properties
    /**
     * Collect errors. Retrieved by getter.
     * @var array
     */
    protected $errors = [];


    /**
     * Main object data
     * @var null
     */
    protected $main_object_data = ["class"=>null, "identifier"=>null];

    /**
     * Hold renderable object
     * @var null
     */
    protected $renderable=null;
    /**
     * Collect warnings. Retrieved by getter.
     * @var array
     */
    protected $warnings = [];


    /**
     * Collect logs. Retrieved by getter.
     * @var array
     */
    protected $logs = [];


    /**
     *  Overall status. Retrieved by getter.
     * @var bool
     */
    protected $result = true;

    /**
     * Built models stored/cached here to process later on.
     * @var array
     */
    protected $models_bucket = [];


    protected $prior_models_bucket = [];

    /**
     *
     * @var array
     */
    protected $free_input = [];
    protected $inaccurate_input = [];

    protected $form = null, $object_types = [], $form_data = [], $validation_rules = [];

    //endregion Properties

    /**
     * Simply validate form input with rules defined in BuilderRenderable.
     *
     * @param array $validation_rules
     * @param Request $request
     */
    protected function validate($validation_rules, Request $request)
    {
        $request->validate($validation_rules);
    }

    //region App specific code
    protected function getExtraModelData($model_class)
    {

        $model_path_split = preg_split("/\\\\/", $model_class);
        $model_clean_name = array_pop($model_path_split);
        $relation_field = \Str::snake($model_clean_name) . "_id";
        $extra_model_path = $model_class . "Extra";

        return ["path" => $extra_model_path, self::RELATION_FIELD => $relation_field];

    }

    protected function getExtrasForDeletion($model_class, $model_id, $key)
    {

        $extra_model_data = $this->getExtraModelData($model_class);
        $extra_model_path = $extra_model_data["path"];
        $relation_field = $extra_model_data[self::RELATION_FIELD];
        //$this->logs[]=$extra_model_path;
        if ($extra_model_path && class_exists($extra_model_path)) {
            $query = $extra_model_path::where($relation_field, $model_id)->where("key", $key);
            $this->logs[] = "[$model_id,$key]" . $query->toSql();
            $get = $query->get();
            return $get ?? false;
        }

        return false;
    }

    protected function getExtraModelFor($model_class, $model_id, $key)
    {

        $extra_model_data = $this->getExtraModelData($model_class);
        $extra_model_path = $extra_model_data["path"];
        $relation_field = $extra_model_data[self::RELATION_FIELD];
        if (class_exists($extra_model_path)) {
            return new $extra_model_path([$relation_field => $model_id, 'key' => $key]);
        }

        return false;
    }

    //endregion

    //region Getters & Setters

    public function getErrors()
    {
        return $this->errors;
    }

    public function getWarnings()
    {
        return $this->warnings;
    }

    public function getLogs()
    {
        return $this->logs;
    }

    public function getFreeInput()
    {
        return $this->free_input;
    }

    public function getInaccurateInput()
    {
        return $this->inaccurate_input;
    }

    public function getMainObjectData(){
        return $this->main_object_data;
    }

    public function getForm(){
        return $this->form;
    }

    public function getFormData(){
        return $this->form_data;
    }

    public function getValidationRules(){
        return $this->validation_rules;
    }

    public function setMainObjectData($object_or_array, $identifier_key="id"){
        if(is_object($object_or_array)){
            if(isset($object_or_array->$identifier_key)){
                $this->main_object_data =  [
                    "class"=>get_class($object_or_array),
                    "identifier"=>$object_or_array->$identifier_key
                ];

            }
        }elseif(is_array($object_or_array) && isset($object_or_array["class"]) && isset($object_or_array["identifier"])){
            $this->main_object_data=Arr::only($object_or_array,["class","identifier"]);
        }
    }

    public function getMainObjectFromBucket(){
        if($this->main_object_data["class"] && $this->main_object_data["identifier"]){
            $object_key = $this->renderable->getObjectKey($this->main_object_data["class"]);

            if($object_key ){
                $bucket_key = $object_key."->".$this->main_object_data["identifier"];

                $return = $this->models_bucket[$bucket_key] ?? null;
                return $return["object"] ?? null;
            }
        }
        return null;
    }

    //endregion

    //region PHP Magic functions
    public function __construct(BuilderRenderable $builder_renderable, Request $request)
    {

        $this->renderable = $builder_renderable;
        $forms = $builder_renderable->collectItemsByTag('form', true);
        if (!count($forms)) {
            $this->errors[] = "Form bulunamadı.";
            $this->result = false;
            return false;
        }
        $form = $forms[0];
        $ignored_fields = $request->has("formstorer_ignore_fields") &&  count($request->formstorer_ignore_fields) ?  $request->formstorer_ignore_fields : [];

        //dump($ignored_fields);


        $form_data = $request->except(['_token']);
        $validation_rules = $form->collectValidationRules();

        //dump($ignored_fields,$form_data,$validation_rules,"----------------------");

        // We need an array to unset keys from $form_data and $validation_rules arrays (by array_diff_keys)
        // Create an array which keys are $ignored_fields and values null
        if(count($ignored_fields)){
            $ignored_fields = array_map([$this, 'nameRegulator'],$ignored_fields);
            //$ignored_fields = array_combine($ignored_fields, array_pad(array(),count($ignored_fields),null));
            $this->logs[] = "Ignored fields are: ".implode(", ", $ignored_fields);
            $form_data = Arr::except($form_data, $ignored_fields);
            $validation_rules = Arr::except($validation_rules, $ignored_fields);
            //dump($form_data,$validation_rules);
        }

        //return dd(count($ignored_fields));

        $this->validate($validation_rules, $request);

        $renderable_options = $builder_renderable->getOptions();
        $object_types = isset($renderable_options[self::OBJECT_TYPES]) ? $renderable_options[self::OBJECT_TYPES] : [];

        $object_types['m_file_general'] = MFileGeneral::class;
        ////////
        $this->validation_rules = $validation_rules;
        $this->object_types = $object_types;
        $this->form_data = $form_data;
        $this->form = $form;
        ////////
    }

    function nameRegulator($name){
        $name .="";
        $found = [];
        preg_match_all('/(\[(.[^\[\]]*)\])/',$name,$found);
        $has_named_index = count($found[2]);
        if($has_named_index){
            $name = preg_replace('/(\[(.[^\[\]]*)\])/', '.$2', $name);
        }
        return preg_replace('/(\[\])/', '', $name);

    }

    //endregion

    /**
     * Builds models using names of inputs filled in the form (retrieved via request) and assigns values to attributes of this model.
     * It's frequent assigning multiple attributes on a model, so instead of building same model times and times, built models stored in models_bucket array.
     * Thus when a model is to be build, an already built existence is checked from bucket and used from there if exists.
     * Otherwise model is built and stored to the bucket for future uses.
     *
     * @return bool
     */
    public function store()
    {

        // Object types data is pulled from renderable object. Assigned in constructor.
        $object_types = $this->object_types;
        // Form data is pulled from Request object. Holds user input. Assigned in constructor.
        $form_data = $this->form_data;

        // Built models are going into
        $this->models_bucket = [];
        // Models in bucket with keys in array below will be processed once
        $to_be_processed_first = [];
        $mod_key_aliasses = [];

        $this->logs[self::OBJECT_TYPES] = $object_types;

        $total_counter = 0;
        $extras_data = [];
        foreach ($form_data as $key => $value) {

            $total_counter++;
            /////
            $this->logs[] = "$key için değer " . (is_array($value) ? implode(', ', $value) : $value);
            /////
            $pieces = preg_split("/->/", $key);
            /////
            $this->logs[] = "$key parçalanıyor:";
            $this->logs[] = $pieces;
            /////

            $model = null;

            if (in_array($pieces[0], array_keys($object_types))) {
                if( $pieces[0] == 'page_detail' ){
                    if( !in_array($pieces[1],$extras_data) ){
                        array_push($extras_data, $pieces[1]);
                    }
                }
                $model_class = $object_types[$pieces[0]];
                if (!isset($pieces[1])) {
                    $this->warnings[] = "Bir " . $object_types[$pieces[0]] . " nesnesiyle ilişkilenen $key alanı için atama yapılamıyor, çünkü nesne id'si geçerli değil. (Nesne id'si: " . " ! isset" . ")";
                    $this->inaccurate_input[$key] = $value;
                    continue;
                }

                $newkey_matches = [];
                preg_match('/(?:new:([\w\-\.]+)){1}(?:\(((?:[\w\-\.]+:(?:[^:,)(])*)(?:(?:,)(?:[\w\-\.]+:[^:,)(]*))*)*\))/', $pieces[1], $newkey_matches);
                $newkey_matches_count = count($newkey_matches);

                $fonkey_matches = [];
                preg_match('/(?:fon:([\w\-\.\:]+)){1}(?:\(((?:[\w\-\.]+:(?:[^:,)(])*)(?:(?:,)(?:[\w\-\.\:]+:[^:,)(]*))*)*\))/', $pieces[1], $fonkey_matches);

                $fonkey_matches_count = count($fonkey_matches);

                if ($newkey_matches_count) {
                    $pieces[1] = $newkey_matches[1];
                    $immediate_assignments = [];
                    if ($newkey_matches_count > 2) {
                        $immediate_assignments = $this->createAssocArrayFromString($newkey_matches[2]);
                    }

                    $model = isset($this->models_bucket[$pieces[0] . "->" . $pieces[1]][self::OBJECT_STR]) ?
                        $this->models_bucket[$pieces[0] . "->" . $pieces[1]][self::OBJECT_STR] :
                        new $model_class();

                    foreach ($immediate_assignments as $fld => $val) {
                        $model->$fld = $val;
                    }
                    $to_be_processed_first[] = $pieces[0] . "->" . $pieces[1];
                    $mod_key_aliasses[] = $pieces[0] . "->" . $pieces[1];
                } elseif ($fonkey_matches_count) {
                    $pieces[1] = $fonkey_matches[1];
                    $fon_params = [];
                    if ($fonkey_matches_count > 2) {
                        $fon_params = $this->createAssocArrayFromString($fonkey_matches[2]);
                    }

                    $model = isset($this->models_bucket[$pieces[0] . "->" . $pieces[1]][self::OBJECT_STR]) ?
                        $this->models_bucket[$pieces[0] . "->" . $pieces[1]][self::OBJECT_STR] :
                        $model_class::firstOrNew($fon_params);

                    $to_be_processed_first[] = $pieces[0] . "->" . $pieces[1];
                    $mod_key_aliasses[] = $pieces[0] . "->" . $pieces[1];
                } else {
                    if (!is_numeric($pieces[1]) && !in_array($pieces[0] . "->" . $pieces[1], $mod_key_aliasses)) {
                        $this->warnings[] = "$key alanı için nesne çağırılamıyor, çünkü nesne id'si geçerli değil. (Nesne id'si: " . (isset($pieces[1]) ? $pieces[1].")" : " ! isset" . ")");
                        continue;
                    }
                    $model = isset($this->models_bucket[$pieces[0] . "->" . $pieces[1]][self::OBJECT_STR]) ?
                        $this->models_bucket[$pieces[0] . "->" . $pieces[1]][self::OBJECT_STR] :
                        $model_class::where('id', $pieces[1])->withTrashed()->first();

                }

                /////
                $this->logs[] = $pieces[0] . "->" . $pieces[1];
                $this->logs[] = $model;
                /////
                $this->models_bucket[$pieces[0] . "->" . $pieces[1]] = [self::OBJECT_STR => $model, self::ACTION => "save", self::USE_PARAMS => false, self::PARAMS => null, self::INPUT => ""];

                if (Str::startsWith($pieces[2], "sync:")) {

                    $piece2 = $pieces[2];
                    $tosync= str_replace("sync:","",$pieces[2]);
                    $val = is_array($value) ? $value : [$value];
                    $model =  $this->models_bucket[$pieces[0] . "->" . $pieces[1]][self::OBJECT_STR]->$tosync();


                    $this->models_bucket[$pieces[0] . "->" . $pieces[1]. "->".$pieces[2]."->".$total_counter]=[
                        self::OBJECT_STR => $model,
                        self::ACTION => "sync",
                        self::USE_PARAMS => true,
                        self::PARAMS => $val,
                        self::INPUT => ""
                    ];

                } else {

                    if ($pieces[2] == "extras") {
                        if (isset($pieces[3]) && $pieces[3]) {

                            $model_or_coll_to_be_deleted = $this->getExtrasForDeletion(get_class($model), $model->id, $pieces[3]);
                            $this->logs[] = "Silmek için extra(lar) soruldu: " . '$this->getExtrasForDeletion(' . get_class($model) . ', ' . $model->id . ', ' . $pieces[3] . ');';
                            if (is_object($model_or_coll_to_be_deleted) && !isset($this->models_bucket[$pieces[0] . "->" . $pieces[1] . self::EXTRAS . $pieces[3] . "->forDeletion"])) {
                                if (is_countable($model_or_coll_to_be_deleted)) {
                                    $cnt = 0;
                                    $this->logs[] = "Countable olan sonuç demetinde " . count($model_or_coll_to_be_deleted) . " model var.";
                                    foreach ($model_or_coll_to_be_deleted as $delitem) {
                                        $this->models_bucket[$pieces[0] . "->" . $pieces[1] . self::EXTRAS . $pieces[3] . "->forDeletion->" . $cnt] = [
                                            self::OBJECT_STR => $delitem,
                                            self::ACTION => "delete",
                                            self::USE_PARAMS => false,
                                            self::PARAMS => null
                                        ];
                                        $cnt++;
                                    }
                                } else {
                                    $this->models_bucket[$pieces[0] . "->" . $pieces[1] . self::EXTRAS . $pieces[3] . "->forDeletion"] = [
                                        self::OBJECT_STR => $model_or_coll_to_be_deleted,
                                        self::ACTION => "delete",
                                        self::USE_PARAMS => false,
                                        self::PARAMS => null
                                    ];
                                }
                            } else {
                                $this->logs[] = "Silinecek model ya da koleksiyon dönmedi: gettype: " . gettype($model_or_coll_to_be_deleted);
                            }

                            $extra_models = [];
                            if (is_array($value)) {
                                $cnt = 0;
                                foreach ($value as $v) {
                                    $extra_model = $this->getExtraModelFor(get_class($model), $model->id, $pieces[3]);
                                    if ($extra_model) {
                                        $extra_model->value = $v."";
                                        $this->models_bucket[$pieces[0] . "->" . $pieces[1] . self::EXTRAS . $pieces[3] . "-" . $cnt] = [self::OBJECT_STR => $extra_model, self::ACTION => "save", self::USE_PARAMS => false, self::PARAMS => null, self::INPUT => $value];
                                        $cnt++;
                                    }
                                }
                            } else {
                                $extra_model = isset($this->models_bucket[$pieces[0] . "->" . $pieces[1] . self::EXTRAS . $pieces[3]][self::OBJECT_STR]) ?
                                    $this->models_bucket[$pieces[0] . "->" . $pieces[1] . self::EXTRAS . $pieces[3]][self::OBJECT_STR] :
                                    ($model->exists ? $this->getExtraModelFor(get_class($model), $model->id, $pieces[3]) : $this->getExtraModelFor(get_class($model), $pieces[1], $pieces[3]));

                                $extra_model->value = $value."";
                                $this->models_bucket[$pieces[0] . "->" . $pieces[1] . self::EXTRAS . $pieces[3]] = [self::OBJECT_STR => $extra_model, self::ACTION => "save", self::USE_PARAMS => false, self::PARAMS => null, self::INPUT => $value];
                            }

                        } else {
                            $this->warnings[] = "$key alanı için extra-key belirtilmemiş. Es geçildi.";
                        }
                    } else {
                        // model = <sitemap_detail->5> (first 2 pieces' replacement model)
                        if(count($pieces)>3){
                            // sample:      sitemap_detail    ->    5    ->    url    ->    metas    ->    title
                            //                pieces[0]        pieces[1]    pieces[2]     pieces[3]     pieces[4]
                            //                 Count 1          Count 2      Count 3       Count 4       Count 5
                            $liveparam = $model;
                            // from piece "url" to piece "metas"
                            for($pi=2;$pi<(count($pieces)-1);$pi++){
                                $extended_param = $pieces[$pi];
                                $liveparam = $liveparam->$extended_param;
                            }
                            // liveparam is MetasKeyValueCollection model
                            // last piece "title"
                            $param = $pieces[count($pieces)-1];
                            $liveparam->$param = $value;

                            // when $model is saved, above assignment will work
                            // as long as all pieces between 2 and n-1 are objects
                            // because they are assigned by reference by default at this line: $liveparam = $liveparam->$extended_param; //NOSONAR
                            // piece[n] assumed to be a property of the object which liveparam finally morphed to

                        }else{
                            // sample:      sitemap_detail    ->    5    ->    title
                            //                pieces[0]        pieces[1]    pieces[2]
                            //                 Count 1          Count 2      Count 3
                            if($pieces[0] == 'm_file_general') {
                                $value = json_decode($value,1);
                            }
                            $param = $pieces[2];
                            $model->$param = $value;
                        }
                        $this->models_bucket[$pieces[0] . "->" . $pieces[1]] = [self::OBJECT_STR => $model, self::ACTION => "save", self::USE_PARAMS => false, self::PARAMS => null, self::INPUT => $value];
                    }

                }


            } else {
                $valstr = is_array($value) ? implode(", ", $value) : $value;
                $this->warnings[] = $pieces[0] . " tanınan bir nesne tipine karşılık gelen bir anahtar olmadığı için free_input dizisine aktarıldı ( \$this->free_input[$key]=$valstr )";
                $this->free_input[$key] = $value;
            }


            /*if ($pieces[0] === "sm" || $pieces[0] === "smd") {
                if (!isset($pieces[1]) || !is_numeric($pieces[1])) {
                    $this->errors[] = "$key alanı için atama yapılamıyor, çünkü nesne id'si geçerli değil. (Nesne id'si: " . isset($pieces[1]) ? $pieces[1] : " ! isset" . ")";
                    $this->result = false;
                }

                switch ($pieces[0]) {
                    case "sm":
                        $model = isset($models_bucket[$pieces[0] . "->" . $pieces[1]]) ?
                            $models_bucket[$pieces[0] . "->" . $pieces[1]] :
                            Sitemap::where('id', $pieces[0])->with('details', 'extras', 'details.extras')->first();
                        $extra_model_class = "Mediapress\\Modules\\Content\\Models\\SitemapExtra";
                        break;
                    case "smd":
                        $model = isset($models_bucket[$pieces[0] . "->" . $pieces[1]]) ?
                            $models_bucket[$pieces[0] . "->" . $pieces[1]] :
                            $model = SitemapDetail::where('id', $pieces[0])->with('extras')->first();
                        $extra_model_class = "Mediapress\\Modules\\Content\\Models\\SitemapDetailExtra";
                        break;
                }

                if (!isset($models_bucket[$pieces[0] . "->" . $pieces[1]])) {
                    $models_bucket[$pieces[0] . "->" . $pieces[1]] = $model;
                }

            }*/


        }
        foreach( $extras_data as $extra ){
            if(!array_key_exists('page_detail->'.$extra.'->extras->tags', $form_data)){
                PageDetailExtra::where('page_detail_id', $extra)->where('key', 'tags')->delete();
            }
        }
        if (!count($this->models_bucket)) {
            $this->warnings[] = "Kaydedilecek model birikmedi.";
            return true;
        } else {
            if (!count($this->errors)) {
                DB::beginTransaction();
                try {
                    if ($this->processFreeInputBeforeModelsSave() === false) {
                        DB::rollBack();
                        $this->result = false;
                        return false;
                    }

                    if ($this->processPriorModels($to_be_processed_first) === false) {
                        DB::rollBack();
                        $this->result = false;
                        return false;
                    }

                    foreach ($this->models_bucket as $model_key => $model_action_data) {
                        $model_from_bucket = $model_action_data[self::OBJECT_STR];

                        $is_ok = $this->handleWillProcessArray($model_from_bucket, ["func"=>"store"]);
                        if($is_ok === false){
                            DB::rollBack();
                            $this->result = false;
                            return false;
                        }
                        $action = $model_action_data[self::ACTION];
                        $params = $model_action_data[self::PARAMS];
                        $use_params = $model_action_data[self::USE_PARAMS] ?? true;

                        if($action){
                            if ($use_params) {
                                $do = $model_from_bucket->$action($params);
                            } else {
                                $do = $model_from_bucket->$action();
                            }
                        }else{
                            $do = true;
                        }
                        if (!$do) {
                            $this->errors[] = "$model_key alanı için (gerekli olduğunda atama yapıldıktan sonra) " . get_class($model_from_bucket) . " modeli işlenemedi($action). (Nesne var: " . $model_from_bucket->exists ? "Evet" : "Hayır" . ")";
                            $this->result = false;
                            DB::rollBack();
                            return false;
                        }
                    }
                    if ($this->processFreeInputAfterModelsSave() === false) {
                        DB::rollBack();
                        $this->result = false;
                        return false;
                    }

                    DB::commit();
                    return true;
                } catch (\Exception $e) {
                    $this->errors[] = $e->getFile() . ":" . $e->getLine() . " - " . $e->getMessage();
                    $this->result = false;
                    DB::rollBack();
                    return false;
                }
            } else {
                $this->result = false;
                return false;
            }

        }


    }

    /**
     * @param string $str
     * @return array
     */
    public function createAssocArrayFromString($str)
    {
        $pairs = array_filter(preg_split('/,/', $str));
        $new_data = [];
        array_walk($pairs, function ($value) use (&$new_data) {
            $split = preg_split('/:/', $value);
            $new_data[$split[0]] = $split[1] . (isset($split[2]) ? ':'.$split[2] : '');
        });
        return $new_data;
    }

    protected function processPriorModels($prior_models_bucket_keys)
    {

        $models_bucket = $this->models_bucket;

        foreach ($models_bucket as $model_key => $model_action_data) {
            if (in_array($model_key, $prior_models_bucket_keys)) {
                if(count($this->errors)){
                    $this->result = false;
                    return false;
                }
                $model_from_bucket = $model_action_data[self::OBJECT_STR];
                $is_ok = $this->handleWillProcessArray($model_from_bucket, ["func"=>"processPriorModels"]);
                if($is_ok === false){
                    // handleWillProcessArray should have set $this->result=false and inserted into $this->errors
                    // before returning false
                    return false;
                }
                $action = $model_action_data[self::ACTION];
                $params = $model_action_data[self::PARAMS] ?? null;
                $use_params = $model_action_data[self::USE_PARAMS] ?? true;
                if($action){
                    if ($use_params) {
                        $do = $model_from_bucket->$action($params);
                    } else {
                        $do = $model_from_bucket->$action();
                    }
                }else{
                    $do=true;
                }

                if (!$do) {
                    $this->errors[] = "$model_key alanı için (gerekli olduğunda atama yapıldıktan sonra) " . get_class($model_from_bucket) . " modeli işlenemedi($action). (Nesne var: " . $model_from_bucket->exists ? "Evet" : "Hayır" . ")";
                    $this->result = false;
                    return false;
                } else {
                    $id = $model_from_bucket->id;
                    $key_split = preg_split('/->/', $model_key);
                    $idish = $key_split[1];
                    $new_key = $key_split[0] . "->" . $id;
                    $model_relation_key = $model_from_bucket->getForeignKey();
                    $extras_start_with_str = $model_key . self::EXTRAS;

                    unset($this->models_bucket[$model_key]);
                    $model_action_data[self::OBJECT_STR] = $model_from_bucket;
                    $this->models_bucket[$new_key] = $model_action_data;

                    $to_be_unset = [];
                    $to_be_inserted = [];
                    foreach ($this->models_bucket as $modkey => $modactiondata) {
                        $modval = $modactiondata[self::OBJECT_STR];
                        if (\Str::startsWith($modkey, $extras_start_with_str)) {
                            $newmodkey = str_replace("->$idish->", "->$id->", $modkey);
                            $modval->$model_relation_key = $id;
                            $to_be_unset[] = $modkey;
                            $modactiondata[self::OBJECT_STR] = $modval;
                            $to_be_inserted[$newmodkey] = $modactiondata;
                        }
                    }

                    foreach ($to_be_unset as $tbu) {
                        unset($this->models_bucket[$tbu]);
                    }

                    $this->models_bucket = array_merge($this->models_bucket, $to_be_inserted);


                } //if main model saved
            } // if in_array - if model is through to_be_saved_first
        } // foreach

        return true;
    }

    //region Gates
    public function processFreeInputBeforeModelsSave()
    {
        //process $this->free_input
        return true;
    }

    public function processFreeInputAfterModelsSave()
    {
        //process $this->free_input
        return true;
    }
    //endregion

    // region Handlers

    /**
     * May modify referenced array
     * Returning false blocks further flow
     * IMPORTANT: $this->result should be set to false and message should be inserted into $this->errors before returning false
     * @param $array_to_proc
     * @return bool
     */
    protected function  handleWillProcessArray(&$array_to_proc, $additional_data=[]){
        return true;
    }

    /**
     * May modify referenced array
     * Returning false blocks further flow
     * IMPORTANT: $this->result should be set to false and message should be inserted into $this->errors before returning false
     * @param $array_to_proc
     * @return bool
     */
    protected function  handleProcessedArray(&$array_to_proc, $additional_data=[]){
        return true;
    }

    //endregion

}
