<?php

namespace Mediapress\AllBuilder\Providers;

use Mediapress\Modules\Module as ServiceProvider;

class AllBuilderServiceProvider extends ServiceProvider
{


    public const RESOURCES = 'Resources';
    public const VIEWS = 'views';

    public function boot()
    {
        $this->publishes([__DIR__ . '/..' . DIRECTORY_SEPARATOR . 'Config' => config_path()], "AllbuilderPublishes");
    }

    public function register()
    {


    }
}
