<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;

class FaIcon extends BuilderRenderable
{
    
    const DESCRIPTION = "description";
    const ITEMS = "items";
    const ICONNAME = "iconname";
    const INPUT_TEXT = "input_text";
    const DEFAULT_VALUE = "default_value";
    const SIZE_X = "size-x";
    public $info = [
        "icon_key" => "font-awesome-flag",
        "object_key" => "FaIcon",
        "object_class" => __CLASS__,
        "object_tags" => [
            "miscellanous"
        ],
        "name" => "Font Awesome Simge",
        self::DESCRIPTION => "Font Awesome türü simge",
        self::ITEMS => [
            "options" => [
                self::ITEMS => [
                    self::ICONNAME => [
                        "key" => self::ICONNAME,
                        "name" => "Simge adı",
                        self::DESCRIPTION => "Simgenin \"fa\" kodu (fa- hariç)",
                        "type" => self::INPUT_TEXT,
                        "custom_template" => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "check"
                    ],
                    self::SIZE_X => [
                        "key" => self::SIZE_X,
                        "name" => "Simge boyutu",
                        self::DESCRIPTION => "Simgenin boyutu (1-4)",
                        "type" => self::INPUT_TEXT,
                        "custom_template" => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "1"
                    ],
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => self::INPUT_TEXT,
                                self::DEFAULT_VALUE => "i"
                            ],
                            "attributes" => [
                                self::ITEMS => [
                                ]
                            ]
                        ]
                    ],
                ]
            ],
        ],
    
    
    ];
    public $options = [
        "html" => [
            "tag" => "i",
            "void_element" => false,
        ]
    ];
    
    public $collectable_as = ["fa", "icon", "faicon"];
    
    public function __construct(array $params = [], array $contents = [], array $options = [], array $data = [])
    {
        parent::__construct($params, $contents, $options, $data);
        
        $icon = isset($this->options[self::ICONNAME]) ? "fa-" . $this->options[self::ICONNAME] : "fa-exclamation-triangle";
        $sizex = $this->options[self::SIZE_X] ?? 1;
        
        $sizex = $sizex > 1 ? "fa-" . $sizex . "x" : null;
        
        $classes = data_get($this->options, "html.attributes.class", "");
        
        $classes = preg_split('/\s/', $classes);
        
        $classes[] = $icon;
        $classes[] = $sizex;
        $classes[] = "fa";
        
        $classes = implode(" ", array_unique(array_filter($classes)));
        
        data_set($this->options, "html.attributes.class", $classes, true);
        
    }
    
}