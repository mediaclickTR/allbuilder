<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;

class MPFile extends BuilderRenderable
{

    public const DESCRIPTION = "description";
    public const ITEMS = "items";
    public const DEFAULT_VALUE = "default_value";
    public const INPUT = "input";
    public const ATTRIBUTES = "attributes";
    public const DATA_TAGS = 'data-tags';
    public $info = [
        "icon_key" => "file-upload",
        "object_key" => "MPFile",
        "object_class" => __CLASS__,
        "object_tags" => [
            "mediapress", "form", "object_specific"
        ],
        "name" => "Dosya Yükleyici",
        self::DESCRIPTION => "Mediapress Dosya Yükleyici / Seçici",
        self::ITEMS => [
            "options" => [
                self::ITEMS => [
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "input_text",
                                self::DEFAULT_VALUE => self::INPUT
                            ],
                            self::ATTRIBUTES => [
                                self::ITEMS => [
                                    "type" => [
                                        "key" => "type",
                                        "name" => "HTML Type",
                                        self::DESCRIPTION => "",
                                        "type" => "readonly_text",
                                        "custom_template" => "", // html - used if input_type is custom,
                                        self::DEFAULT_VALUE => "hidden"
                                    ],
                                    "class" => [
                                        "type" => "input_text",
                                        self::DEFAULT_VALUE => "mfile mpimage"
                                    ]
                                ]
                            ]
                        ]
                    ],
                    "tags"=>[
                        "key" => "tags",
                        "name" => "Yüklenecek Dosyalar",
                        self::DESCRIPTION => "",
                        "type" => "mpfile_tag",
                        "custom_template" => "", // html - used if input_type is custom,
                        self::ITEMS => []
                    ]
                ]
            ],
            //""
            "params"=>[
                "items"=>[
                    "files"=>[
                        "key" => "files",
                        "name" => "Dosyalar",
                        self::DESCRIPTION => "Dosyalar",
                        "type" => "input_text",
                        "custom_template" => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "&lt;var&gt;<object>->mfiles&lt;/var&gt;"
                    ]
                ]
            ]
        ],
    ];

    public $options = [
        "html" => [
            "tag" => self::INPUT,
            self::ATTRIBUTES => [
                "type" => "hidden",
                "class" => "mfile mpimage",
            ]
        ],
    ];

    public $collectable_as = [self::INPUT,"formfield"];

    public function __construct(array $params = [], array $contents = [], array $options = [], array $data = [])
    {
        parent::__construct($params, $contents, $options, $data);

        if (isset($this->options['tags'])) {
            $array = [];

            if (is_array($this->options['tags'])) {
                foreach ($this->options['tags'] as $key => $tag) {
                    $array[$key] = json_decode(str_replace('%quot%', '"', $tag), 1);
                }

                foreach ($array as $key => $item) {
                    $array[$key]['title'] = getPanelLangPartKey($key, $item['title']);
                }

                $tags = str_replace('"', '&quot;', json_encode($array));

                $this->options['html'][self::ATTRIBUTES][self::DATA_TAGS] = $tags;
                $this->options['title'][self::ATTRIBUTES][self::DATA_TAGS] = $tags;
            } else {
                $this->options['html'][self::ATTRIBUTES][self::DATA_TAGS] = '{}';
            }

        } else {
            $this->options['html'][self::ATTRIBUTES][self::DATA_TAGS] = '{}';
        }

        extract($this->params);
        if (isset($files) && is_countable($files)) {
            $files = collect($files->toArray())->groupBy('pivot.file_key');
            $list = [];
            foreach ($files as $key => $keyFiles) {
                $list[$key] = [];
                foreach ($keyFiles as $keyFile) {
                    $list[$key][] = $keyFile['id'];
                }
            }
            $this->options['html'][self::ATTRIBUTES]['value'] = json_encode($list);
        } else {
            $this->options['html'][self::ATTRIBUTES]['value'] = '{}';
        }

    }
}
