<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\HtmlElement;

class SimpleTabs extends BuilderRenderable
{
    public const ITEMS = "items";
    public $collectable_as = ["tabs", "simpletabs"];
    
    public $info = [
        "icon_key" => "window-restore",
        "object_key" => "SimpleTabs",
        "object_class" => __CLASS__,
        "object_tags" => [
            "container"
        ],
        "name" => "Sekme Kapsayıcısı",
        "description" => "İçine konulan sekme(tab) nesneleri için navigasyonunu oluşturan kapsayıcı nesne",
        self::ITEMS => [
            "options" => [
                self::ITEMS => [
                    "html" => [
                        self::ITEMS => [
                            "attributes" => [
                                self::ITEMS => [
                                    "class" => [
                                        "type" => "input_text",
                                        "default_value" => "tab-list"
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
        ],
    ];
    
    public $options = [
        "html" => [
            "tag" => "div",
            "attributes" => [
                "class" => "tab-list"
            ]
        ]
    ];
    
    public function getHtmlElement()
    {
        
        if ($this->ignored_if) {
            return (new HtmlElement());
        }
        $el = $this->getSelfHtmlElement();
        $contents = $this->getContentHtmlElements();
        $nav = (new HTMLElement("ul", false))->add_class("nav nav-tabs");
        $counter = 0;
        foreach ($contents as $content) {
            if (is_a($content, "Mediapress\\Foundation\\HTMLElement") && $content->has_class("tab-pane") && $content->get_tag()) {
                $counter++;
                if ($counter == 1) {
                    $content->add_class("active show");
                }
                $content->add_class("fade");
                $button = (new HtmlElement('a', false))
                    ->add_attr("href", '#' . $content->get_id())
                    ->add_class($counter == 1 ? "active show" : "")
                    ->data('toggle', 'tab')
                    ->add_content($content->data('tab-title'));
                $nav->add_content((new HTMLElement("li", false))->add_content($button));
            }
        }
        
        $tabcontent = (new HTMLElement("div", false))->add_class("tab-content")->add_attr("style", "padding-top:40px;")
            ->add_class($counter == 1 ? "active" : "");
        
        $el->add_content($nav)
            ->add_content(
                $tabcontent->add_content($contents)
                    ->add_content("<div class='clearfix'></div>")
            );
        return $el;
    }
}