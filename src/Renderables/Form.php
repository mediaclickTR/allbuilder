<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\HtmlElement;

class Form extends BuilderRenderable
{
    const ITEMS = "items";
    const ATTRIBUTES = "attributes";
    public $info = [
        "icon_key" => "file-invoice",
        "object_key" => "Form",
        "object_class" => __CLASS__,
        "object_tags" => [
            "form", "basic_html"
        ],
        "name" => "HTML Form",
        "description" => "Standart Form etiketi",
        self::ITEMS => [
            "options" => [
                self::ITEMS => [
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "readonly_text",
                                "default_value" => "form"
                            ],
                            self::ATTRIBUTES => [
                                self::ITEMS => [
                                    "class" => [
                                        "type" => "input_text",
                                        "default_value" => ""
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
        ],
    ];
    
    public $options = [
        "html" => [
            "tag" => "form"
        ]
    ];
    
    public $collectable_as = ["form"];
    
    public function getHtmlElement()
    {
        if ($this->ignored_if) {
            return (new HtmlElement());
        }
        $el = $this->getSelfHtmlElement();
        $csrf = (new HtmlElement("input"))->add_attr("name", "_token")->add_attr("type", "hidden")->add_attr("value", csrf_token());
        //$ignored_fields = (new HtmlElement("input"))->add_attr("name", "formstorer_ignore_fields[]")->add_attr("type", "hidden")->add_attr("value", "")->add_class("formstorer_ignore_field");
        $contents = $this->getContentHtmlElements();
        $el->add_content($csrf)/*->add_content($ignored_fields)*/->add_content($contents);
        return $el;
    }
    
    public function collectValidationRules()
    {
        if (!$this->are_contents_converted) {
            $this->convertContentsToRenderables();
        }
        $fields = $this->collectItemsByTag("formfield");
        //dump($fields);
        $rules=[];
        foreach ($fields as $field) {
            /*$name =
                str_replace("]", "", (
                str_replace(["][", "["], ".", ($field->options["html"][self::ATTRIBUTES]["name"] ?? ""))
                ));*/
            $name = preg_replace('/(\[(.[^\[\]]*)\])/', '.$2', ($field->options["html"][self::ATTRIBUTES]["name"] ?? ""));
            $name = preg_replace('/(\[\])/', '', ($name ?? ""));
            
            $rule = ($field->options["rules"] ?? "");
            if($name && $rule){
                $rules[$name] = $rule;
            }
        }
        return $rules;
    }
    
    public function collectFields(){
        if (!$this->are_contents_converted) {
            $this->convertContentsToRenderables();
        }
        $fields = $this->collectItemsByTag("formfield");
        $flds = [];
    
        foreach ($fields as $field) {
            
            $dataset = [
                "tag"=>null,
                "type"=>null,
                "attr"=>[],
                "subs"=>[
        
                ]
            ];
            
            $name =
                str_replace("]", "", (
                str_replace(["][", "["], ".", ($field->options["html"][self::ATTRIBUTES]["name"] ?? ""))
                ));

            $tag = $field->options["html"]["tag"] ?? null;
            $dataset["tag"]=$tag;
            $dataset["attr"]=$field->options["html"][self::ATTRIBUTES] ?? [];
            switch($tag){
                case "input":
                    $dataset["type"]=$field->options["html"][self::ATTRIBUTES]["type"] ?? "text";
                    break;
                case "select":
                    $dataset["subs"]=$field->contents ?? [];
                    break;
                default:
                    break;
            }
            
            $flds[$name] = $dataset;
        }
        return $flds;
    }
}
