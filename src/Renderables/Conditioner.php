<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\HtmlElement;

class Conditioner extends BuilderRenderable
{

    public const DESCRIPTION = "description";
    public const ITEMS = "items";
    public const DEFAULT_VALUE = "default_value";
    public const CONDITIONS = "conditions";
    public $info = [
        "icon_key" => "question",
        "object_key" => "Conditioner",
        "object_class" => __CLASS__,
        "object_tags" => [
            "mediapress", "flow_control"
        ],
        "name" => "Koşullu Kapsayıcı",
        self::DESCRIPTION => "Sadece koşulu gerçekleşirse etkinleşen nesne",
        self::ITEMS => [
            "options" => [
                self::ITEMS => [
                    "html" => [
                        "key" => "html",
                        "name" => "HTML",
                        self::DESCRIPTION => "",
                        "type" => "group",
                        "custom_template" => "", // html - used if input_type is custom
                        self::ITEMS => [
                            "tag" => [
                                "type" => "input_text",
                                self::DEFAULT_VALUE => "div"
                            ],
                            "attributes" => [
                                self::ITEMS => [
                                    "class" => [
                                        "type" => "input_text",
                                        self::DEFAULT_VALUE => ""
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
            "params" => [
                self::ITEMS => [
                    self::CONDITIONS => [
                        "key" => self::CONDITIONS,
                        "name" => "Koşullar",
                        self::DESCRIPTION => "Yorumlanacak koşullar",
                        "type" => "condition",
                        "custom_template" => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "[]"
                    ]
                ]
            ]
        ]
    ];
    
    public $result = false;
    
    public function __construct(array $params = [], array $contents = [], array $options = [], array $data = [])
    {
        parent::__construct($params, $contents, $options, $data);
        $conditions = $this->params[self::CONDITIONS] ?? [];
        $this->result = $this->processConditionsArray($conditions);
    }
    
    protected function getSelfHtmlElement()
    {
        if ($this->result) {
            return parent::getSelfHtmlElement();
        }
        return new HtmlElement(null);
    }
    
    public function getContentHtmlElements()
    {
        if ($this->result) {
            return parent::getContentHtmlElements();
        }
        return [];
    }
    
    public function getHtmlElement()
    {
        if ($this->isIgnored()) {
            return (new HtmlElement());
        }
        if ($this->result) {
            return parent::getHtmlElement();
        }
        return $this->getSelfHtmlElement();
    }
    
}