<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\HtmlElement;
use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\Content\Models\Page;

class PageCategoriesControl extends BuilderRenderable
{
    public const CAPTION_MODE_SELF_ONLY = "self_only";
    public const CAPTION_MODE_ALL_CHAIN = "all_chain";
    public const CAPTION_MODE_SINGLE_ELLIPSIS = "single_ellipsis";
    public const CAPTION_MODE_MULTI_ELLIPSIS = "multi_ellipsis";
    public const CAPTION_MODE_LAST_TWO = "last_two";
    public const DESCRIPTION = "description";
    public const SELECTED = "selected";
    public const VALUE = "value";
    public const OPTION = "option";
    public const CAPTION = "caption";
    public const CONTENTS = "contents";
    public const FORMGROUP = "formgroup";
    public const COMPONENTS = "components";
    public const DEPTHS_ALLOWED = "depths_allowed";
    public const PAGE_MODEL = "page_model";
    public const CLASS1 = "class";
    public const ATTRIBUTES = "attributes";
    public const SELECT = "select";
    public const DEFAULT_TEXT = "default_text";
    public const SHOW_DEFAULT_OPTION = "show_default_option";
    public const MULTIPLE = "multiple";
    public const DEFAULT_VALUE = "default_value";
    public const CUSTOM_TEMPLATE = "custom_template";
    public const INPUT_TEXT = "input_text";
    public const TITLE = "title";
    public const OPTIONS = "options";
    public const ITEMS = "items";

    public $categories = [];

    public $caption_mode = "self_only";

    public $info = [
        "icon_key" => "caret-square-down",
        "object_key" => "PageCategoriesControl",
        "object_class" => __CLASS__,
        "object_tags" => [
            "mediapress", "form", "object_specific"
        ],
        "name" => "Page Categories",
        self::DESCRIPTION => "Verilen sayfanın kategorilerini listeleyen Select2",
        self::ITEMS => [
            self::OPTIONS => [
                self::ITEMS => [
                    self::TITLE => [
                        "key" => self::TITLE,
                        "name" => "Başlık",
                        self::DESCRIPTION => "Nesnenin başlığı",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ""
                    ],
                    self::MULTIPLE => [
                        "key" => self::MULTIPLE,
                        "name" => "Çoklu Seçim",
                        self::DESCRIPTION => "Birden fazla değer seçmeye izin verilip verilmeyeceğini belirler",
                        "type" => "radio",
                        "values" => [
                            ":null" => "Hiçbiri",
                            "0" => "Hayır",
                            "1" => "Evet",
                            ":custom" => "Özel",
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "0"
                    ],
                    self::SHOW_DEFAULT_OPTION => [
                        "key" => self::SHOW_DEFAULT_OPTION,
                        "name" => "Varsayılan Seçimi Göster",
                        self::DESCRIPTION => "Kullanıcı seçim yapmadan önce seçili görünecek bir değer olup olmayacağını belirler",
                        "type" => "radio",
                        "values" => [
                            "0" => "Hayır",
                            "1" => "Evet",
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ":null"
                    ],
                    self::DEFAULT_VALUE => [
                        "key" => self::DEFAULT_VALUE,
                        "name" => "Varsayılan eleman değeri",
                        self::DESCRIPTION => "",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ""
                    ],
                    self::DEFAULT_TEXT => [
                        "key" => self::DEFAULT_TEXT,
                        "name" => "Varsayılan eleman metni",
                        self::DESCRIPTION => "",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "---Seçiniz---"
                    ],
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "readonly_text",
                                self::DEFAULT_VALUE => self::SELECT
                            ],
                            self::ATTRIBUTES => [
                                self::ITEMS => [
                                    self::CLASS1 => [
                                        "type" => self::INPUT_TEXT,
                                        self::DEFAULT_VALUE => ""
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
            "params" => [
                self::ITEMS => [
                    self::PAGE_MODEL => [
                        "key" => self::PAGE_MODEL,
                        "name" => "Page Nesnesi",
                        self::DESCRIPTION => "Aracın bağlanacağı Page nesnesi",
                        "type" => "object_selector",
                        "object_selector_parameters" => [
                            "selector_type" => "",
                            "selectable_objects" => []
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => null
                    ],
                ]
            ]
        ],
    ];

    public $options = [
        "html" => [
            "tag" => self::SELECT,
            "void_element" => false,
            self::CLASS1 => "page-categories-control"
        ],
        self::TITLE => "Kategori",
        self::MULTIPLE => true,
        self::SHOW_DEFAULT_OPTION => true,
        self::DEFAULT_TEXT => "---- Seçim Yok ----",
        self::DEFAULT_VALUE => "",
        self::DEPTHS_ALLOWED => [],
        "caption_mode" => "last_two",
        self::COMPONENTS => [
            self::FORMGROUP => [
                "type" => "div",
                self::OPTIONS => [
                    "html" => [
                        self::ATTRIBUTES => [
                            self::CLASS1 => "form-group focus pagecategoriescontrol-form-group"
                        ]
                    ],
                    "collectable_as" => ["form-group"]
                ],
                self::CONTENTS => []

            ],
            self::CAPTION => [
                "type" => "bluetitle",
                self::OPTIONS => [
                    "collectable_as" => [self::CAPTION]
                ]
            ]
        ]
    ];


    public $params = [
        self::PAGE_MODEL => null
    ];


    public $collectable_as = [self::SELECT, "input", "formfield", "pagecategoriescontrol"];


    public function __construct(array $params = [], array $contents = [], array $options = [], array $data = [])
    {
        parent::__construct($params, $contents, $options, $data);

        extract($this->params);

        /** @var Page $page_model */

        if (!isset($page_model) || !$page_model || !is_a($page_model, Page::class)) {
            data_set($this->options, "html.tag", null, true);
            $this->ignored_if = true;
            $this->errors[] = "page_model parametresi bir " . Page::class . " nesnesi olmalıdır.";
            return false;
        }

        data_set(
            $this->options,
            "html.attributes.name",
            "page->" . ($page_model->exists ? $page_model->id : "new:" . $this->class_id . "()") . "->sync:categories[]",
            false
        );

        $this->extendStack("scripts", $this->script($page_model));


        $this->categories = $page_model->sitemap->categories()->with(['detail'])
            ->orderBy("lft")
            ->orderBy("depth")
            ->whereHas('detail');

        if (isset($this->options[self::DEPTHS_ALLOWED]) && count($this->options[self::DEPTHS_ALLOWED])) {
            $this->categories = $this->categories->whereIn("depth", $this->options[self::DEPTHS_ALLOWED]);
        }
        $this->categories = $this->categories->get();

        //////////
        $generated_content = [];
        //////////
        $values = $this->categories->pluck('detail.name', 'id')->toArray();


        $this->caption_mode = $caption_mode = $this->options["caption_mode"] ?? self::CAPTION_MODE_SELF_ONLY;

        switch ($caption_mode) {
            case self::CAPTION_MODE_ALL_CHAIN:
            case self::CAPTION_MODE_SINGLE_ELLIPSIS:
            case self::CAPTION_MODE_MULTI_ELLIPSIS:
            case self::CAPTION_MODE_LAST_TWO:
                $values = array_combine(array_keys($values), array_map([$this, 'getCategoryCaption'], array_keys($values), $values));

            case  self::CAPTION_MODE_SELF_ONLY:
            default:
                break;

        }
        ///////////////////////

        $option_cls_path = $this->options["renderable_types"][self::OPTION] ?? null;
        $selected_val = $page_model->categories->pluck('id')->toArray();

        if ($this->options[self::MULTIPLE]) {
            data_set($this->options, "html.attributes.multiple", "multiple ", true);
        } elseif (isset($this->options["html"][self::ATTRIBUTES][self::MULTIPLE])) {
            unset($this->options["html"][self::ATTRIBUTES][self::MULTIPLE]);
        }

        $default_option = null;
        if ($this->options[self::SHOW_DEFAULT_OPTION]) {
            if ($this->options[self::MULTIPLE]) {
                $this->warnings[] = "Select(" . $this->class_id . ") nesne ayarı aynı anda hem multiple=true hem show_default_option=true olamaz. show_default_option=false kabul edildi.";
            } else {
                $default_option = (new HtmlElement(self::OPTION, false))
                    ->add_attr(self::VALUE, $this->options[self::DEFAULT_VALUE])
                    ->data("display", "Seçin")
                    ->add_content($this->options[self::DEFAULT_TEXT]);
            }
        }
        ///////////////////////

        foreach ($values as $k => $v) {
            $selected = is_countable($selected_val) ? in_array(($k . ""), $selected_val) : ($k . "") === ($selected_val . "");
            if ($option_cls_path) {
                $options_of_opt = $selected ? [self::SELECTED => $selected] : [];
                $opt = new $option_cls_path([], [], $options_of_opt, [self::VALUE => $k, "text" => $v]);
            } else {
                $opt = (new HtmlElement(self::OPTION, false))->add_attr(self::VALUE, $k)->add_content($v);
                if ($selected) {
                    $opt->add_attr(self::SELECTED, self::SELECTED);
                }
            }
            $generated_content[] = $opt;
        }

        $this->contents = $generated_content;
        if ($default_option) {
            array_unshift($this->contents, $default_option);
        }

    }


    public function getHtmlElement()
    {
        if ($this->ignored_if) {
            return (new HtmlElement());
        }

        data_set($this->options, self::TITLE, "Kategori ", false);
        $el = parent::getHtmlElement();
        if($el->get_attr('name')) {
            $this->options[self::TITLE] = getPanelLangPartKey($el->get_attr('name'), $this->options[self::TITLE]);
        }
        /*$contents = $this->getContentHtmlElements();
        $el->add_content($contents);*/


        $classes = data_get($this->options, "html.attributes.class", "");
        $new_cls = $classes . " page-categories-control" . (isset($this->options[self::MULTIPLE]) && $this->options[self::MULTIPLE] ? " multiple" : "");
        data_set($this->options, "html.attributes.class", $new_cls, true);

        $formgroup = $this->options[self::COMPONENTS][self::FORMGROUP] ?? null;
        $caption = $this->options[self::COMPONENTS][self::CAPTION] ?? null;

        if ($formgroup && $caption) {
            $this->options[self::COMPONENTS][self::CAPTION][self::CONTENTS][] = $this->options[self::TITLE];
            $caption = $this->buildRenderableFromArray($this->options[self::COMPONENTS][self::CAPTION]);
        }


        if ($formgroup) {
            if ($caption) {
                $this->options[self::COMPONENTS][self::FORMGROUP][self::CONTENTS][self::CAPTION] = $caption;
            }
            $this->options[self::COMPONENTS][self::FORMGROUP][self::CONTENTS]["input"] = (new HtmlElement("div"))->add_class("row")->add_content((new HtmlElement("div"))->add_class("col")->add_content($el));
            $formgroup = $this->buildRenderableFromArray($this->options[self::COMPONENTS][self::FORMGROUP]);
        }
        return $formgroup ? $formgroup->getHtmlElement() : $el;
    }

    private function getCategoryCaption($cat_id, $cat_name)
    {

        $strs = [];
        $strs = [$cat_name];
        $cat = $this->categories->where("id", $cat_id)->first();

        switch ($this->caption_mode) {
            case self::CAPTION_MODE_ALL_CHAIN:
                while (true) {
                    $cat = $this->categories->where("id", $cat->category_id)->first();
                    if ($cat) {
                        $strs[] = $cat->detail->name;
                    } else {
                        break;
                    }
                }
                return implode(' >> ', array_reverse($strs));
                break;
            case self::CAPTION_MODE_SINGLE_ELLIPSIS:
                $parent_count = 0;
                $last_parent_name = "";
                while (true) {
                    $cat = $this->categories->where("id", $cat->category_id)->first();
                    if ($cat) {
                        $parent_count++;
                        $last_parent_name = $cat->detail->name;
                    } else {
                        if ($parent_count) {
                            if ($parent_count > 1) {
                                $strs[] = "...";
                            }
                            $strs[] = $last_parent_name;
                        }
                        break;
                    }
                }

                return implode(' >> ', array_reverse($strs));
                break;
            case self::CAPTION_MODE_MULTI_ELLIPSIS:
                $parent_count = 0;
                $last_parent_name = "";
                while (true) {
                    $cat = $this->categories->where("id", $cat->category_id)->first();
                    if ($cat) {
                        $strs[] = "...";
                        $parent_count++;
                        $last_parent_name = $cat->detail->name;
                    } else {
                        if ($parent_count) {
                            array_pop($strs);
                            $strs[] = $last_parent_name;
                        }
                        break;
                    }
                }

                return implode(' >> ', array_reverse($strs));
                break;
            case self::CAPTION_MODE_LAST_TWO:
                $parent_count = 0;
                while (true) {
                    $cat = $this->categories->where("id", $cat->category_id)->first();
                    if ($cat) {
                        $parent_count++;
                        if ($parent_count == 1) {
                            $strs[] = $cat->detail->name;
                        } elseif ($parent_count == 2) {
                            $strs[] = "...";
                            break;
                        }
                    } else {
                        break;
                    }
                }

                return implode(' >> ', array_reverse($strs));
                break;
            case self::CAPTION_MODE_SELF_ONLY:
            default:
                return $cat_name;
                break;

        }
    }

    private function script($page_model)
    {
        $script = <<<SCRIPT
    <script>
    $(document).ready(function(){
        $("#$this->class_id").change(function(){
            if(typeof pageCategoriesSelectionChanged === "function"){
                pageCategoriesSelectionChanged($page_model->id, $(this).val());
            }else{
                console.warn("MEDIAPRESS400: pageCategoriesSelectionChanged(page_id,category_ids_arr) fonksiyonu tanımlanmamış. Seçilen kategorilere göre işlem yapabilmek için bu fonksiyonu tanımlayın.")
            }
        }).change();
    });
</script>
SCRIPT;
        return $script;

    }


}
