<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\HtmlElement;

class BlankWithLabel extends BuilderRenderable
{
    public const ITEMS = "items";
    public const OPTIONS = "options";
    public const TITLE = "title";
    public const INPUT_TEXT = "input_text";
    public const DEFAULT_VALUE = "default_value";
    public const ATTRIBUTES = "attributes";
    public const COMPONENTS = "components";
    public const FORMGROUP = "formgroup";
    public const CONTENTS = "contents";
    public const LABEL = "label";
    public $info = [
        "icon_key" => "asterisk",
        "object_key" => "BlankWithLabel",
        "object_class" => __CLASS__,
        "object_tags" => [
            "bootstrap", "miscellaneous"
        ],
        "name" => "Etiketli Boş Nesne",
        "description" => "Bootstrap form-group içinde label etiketiyle sunulacak herhangi bir nesne",
        self::ITEMS => [
            self::OPTIONS => [
                self::ITEMS => [
                    self::TITLE => [
                        "key" => self::TITLE,
                        "name" => "Başlık",
                        "description" => "Etiket başlığı",
                        "type" => self::INPUT_TEXT,
                        "custom_template" => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ""
                    ],
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => self::INPUT_TEXT,
                                self::DEFAULT_VALUE => ":null"
                            ],
                            self::ATTRIBUTES => [
                                self::ITEMS => [
                                    "class" => [
                                        "type" => self::INPUT_TEXT,
                                        "custom_template" => "", // html - used if input_type is custom,
                                        self::DEFAULT_VALUE => ""
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
        ],
    ];
    
    public $options = [
        self::TITLE => "Etiket",
        "html" => [
            "tag" => null
        ],
        self::COMPONENTS => [
            self::FORMGROUP => [
                "type" => "blank",
                "params" => [
                    "html" => [
                        "tag" => "div",
                        "void_element" => false,
                        self::ATTRIBUTES => [
                            "class" => "form-group"
                        ]
                    ],
                    self::OPTIONS => [
                        "collectable_as" => ["form-group"]
                    ],
                ],
                self::CONTENTS => []
            
            ],
            self::LABEL => [
                "type" => "blank",
                "params" => [
                    "html" => [
                        "tag" => self::LABEL,
                        "void_element" => false,
                        self::ATTRIBUTES => []
                    ],
                    self::OPTIONS => [
                        "collectable_as" => [self::LABEL]
                    ]
                ],
                self::CONTENTS => []
            ]
        ]
    ];
    
    public function options()
    {
    
    }
    
    public function getHtmlElement()
    {
        if ($this->ignored_if) {
            return (new HtmlElement());
        }
        $this->options[self::COMPONENTS][self::LABEL][self::CONTENTS][] = $this->options[self::TITLE];
        $label = $this->buildRenderableFromArray($this->options[self::COMPONENTS][self::LABEL]);
        $el = parent::getHtmlElement();
        $contents = $this->getContentHtmlElements();
        $el->add_content($contents);
        $this->options[self::COMPONENTS][self::FORMGROUP][self::CONTENTS][self::LABEL] = $label;
        $this->options[self::COMPONENTS][self::FORMGROUP][self::CONTENTS]["input"] = $el;
        $formgroup = $this->buildRenderableFromArray($this->options[self::COMPONENTS][self::FORMGROUP]);
        
        return $formgroup->getHtmlElement();
    }
    
    
}