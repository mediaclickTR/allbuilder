<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\HtmlElement;

class StepTabs extends BuilderRenderable
{
    public const ITEMS = "items";
    public const DESCRIPTION = "description";
    public const CUSTOM_TEMPLATE = "custom_template";
    public const VALUE = "value";
    public const INPUT_TEXT = "input_text";
    public const DEFAULT_VALUE = "default_value";
    public const VALUES = "values";

    public $collectable_as = ["tabs", "steptabs"];


    public $info = [
        "icon_key" => "window-restore",
        "object_key" => "StepTabs",
        "object_class" => __CLASS__,
        "object_tags" => [
            "container"
        ],
        "name" => "İlerleyen Sekme Kapsayıcısı",
        "description" => "Sırayla ilerleyen sekme kapsayıcı",
        self::ITEMS => [
            "options" => [
                self::ITEMS => [
                    "show_buttons"=>[
                        "key" => "show_buttons",
                        "name" => "Düğmeleri Göster",
                        self::DESCRIPTION => "Düğme grubunu gösterip gizlemenizi sağlar",
                        "type" => "radio",
                        self::VALUES => [
                            "0" => "Hayır",
                            "1" => "Evet",
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => 1
                    ],
                    "show_backward_button"=>[
                        "key" => "show_backward_button",
                        "name" => "Geri Düğmesini Göster",
                        self::DESCRIPTION => "Geri düğmesini gösterip gizlemenizi sağlar",
                        "type" => "radio",
                        self::VALUES => [
                            "0" => "Hayır",
                            "1" => "Evet",
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => 1
                    ],
                    "show_forward_button"=>[
                        "key" => "show_forward_button",
                        "name" => "İleri Düğmesini Göster",
                        self::DESCRIPTION => "İleri düğmesini gösterip gizlemenizi sağlar",
                        "type" => "radio",
                        self::VALUES => [
                            "0" => "Hayır",
                            "1" => "Evet",
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => 1
                    ],
                    "show_submit_button"=>[
                        "key" => "show_submit_button",
                        "name" => "Kaydet Düğmesini Göster",
                        self::DESCRIPTION => "Kaydet düğmesini gösterip gizlemenizi sağlar",
                        "type" => "radio",
                        self::VALUES => [
                            "0" => "Hayır",
                            "1" => "Evet",
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => 1
                    ],
                    "html" => [
                        self::ITEMS => [
                            "attributes" => [
                                self::ITEMS => [
                                    "class" => [
                                        "type" => "input_text",
                                        "default_value" => "tab-list"
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
        ],
    ];

    public $options = [
        "forced_html_classes"=>["step-tabs"],
        "show_buttons"=>true,
        "show_backward_button"=>true,
        "show_forward_button"=>true,
        "show_submit_button"=>true,
        "components" => [
            "buttons_row" => [
                "type" => "ul",
                "options" => [
                    "html" => [
                        "attributes" => [
                            "class" => "pager wizard mt-1"
                        ]
                    ]
                ],
                "contents" => [
                    "backward_button" => [
                        "type"=>"listitem",
                        "options"=>[
                            "html"=>[
                                "attributes"=>[
                                    "class"=>"previous"
                                ]
                            ]
                        ],
                        "contents"=>[
                            "button"=>[
                                "type"=>"anchor",
                                "options"=>[
                                    "html"=>[
                                        "attributes"=>[
                                            "href"=>"javascript:;",
                                            "class"=>""
                                        ]
                                    ]
                                ],
                                "contents"=>[
                                    "icon"=>[
                                        "type"=>"faicon",
                                        "options"=>[
                                            "iconname"=> "angle-left"
                                        ],
                                    ],
                                    "Geri"
                                ]
                            ]
                        ]
                    ],
                    "forward_button" => [
                        "type"=>"listitem",
                        "options"=>[
                            "html"=>[
                                "attributes"=>[
                                    "class"=>"next"
                                ]
                            ]
                        ],
                        "contents"=>[
                            "button"=>[
                                "type"=>"anchor",
                                "options"=>[
                                    "html"=>[
                                        "attributes"=>[
                                            "href"=>"javascript:;",
                                            "class"=>"btn btn-primary text-white"
                                        ]
                                    ]
                                ],
                                "contents"=>[
                                    "İleri",
                                    "icon"=>[
                                        "type"=>"faicon",
                                        "options"=>[
                                            "iconname"=> "angle-double-right"
                                        ],
                                    ],
                                ]
                            ]
                        ]
                    ],
                    "submit_button" => [
                        "type"=>"listitem",
                        "contents"=>[
                            "button"=>[
                                "type" => "button",
                                "options" => [
                                    "html" => [
                                        "attributes" => [
                                            "type" => "submit",
                                            "class" => "btn btn-secondary btn-step-tabs-submit"
                                        ]
                                    ],
                                    "caption" => "Kaydet"
                                ]
                            ]
                        ]
                    ],
                ]
            ]
        ]
    ];


    public function __construct(array $params = [], array $contents = [], array $options = [], array $data = [])
    {
        parent::__construct($params, $contents, $options, $data);

        $show_buttons = ! (isset($this->options["show_buttons"]) && !$this->options["show_buttons"]);
        $show_forward_button = ! (isset($this->options["show_forward_button"]) && !$this->options["show_forward_button"]);
        $show_backward_button = ! (isset($this->options["show_backward_button"]) && !$this->options["show_backward_button"]);
        $show_submit_button = ! (isset($this->options["show_submit_button"]) && !$this->options["show_submit_button"]);


        if($show_buttons){
            if( ! $show_forward_button){ unset($this->options["components"]["buttons_row"]["contents"]["forward_button"]);}
            if( ! $show_backward_button){ unset($this->options["components"]["buttons_row"]["contents"]["backward_button"]);}
            if( ! $show_submit_button){ unset($this->options["components"]["buttons_row"]["contents"]["submit_button"]);}
            //$this->contents[] = $this->options["components"]["buttons_row"];
        }else{
            unset($this->options["components"]["buttons_row"]);
        }

    }

    public function getHtmlElement()
    {
        if ($this->ignored_if) {
            return (new HtmlElement());
        }

        $buttons_row = null;
        if(isset($this->options["components"]["buttons_row"]) && is_array($this->options["components"]["buttons_row"])){
            $buttons_row = $this->buildRenderableFromArray($this->options["components"]["buttons_row"]);
        }

        $el = $this->getSelfHtmlElement();
        $contents = $this->getContentHtmlElements();
        $nav = (new HTMLElement("ul", false));
        $counter = 0;
        //dd($contents, $this->contents);
        foreach ($contents as $key => $content) {
            //dump($content);
            if (is_a($content, "Mediapress\\Foundation\\HTMLElement") && $content->has_class("tab-pane") && $content->get_tag()) {
                $counter++;
                if ($counter == 1) {
                    $content->add_class("active");
                }
                $content->add_class("fade show");


                /*if (count($contents) - 1 != $key) {
                    $content->add_content('<a class="btn btn-primary float-right" onclick="tabForward($(this));">İlerle</a>');
                }
                if ($key != 0) {
                    $content->add_content('<a class="btn btn-primary float-left" onclick="tabBack($(this));">Geri</a>');
                }*/

                $button = (new HtmlElement('a', false))
                    ->add_attr("href", '#' . $content->get_id())
                    ->data('toggle', 'tab')
                    ->add_content($content->data('tab-title'))
                    ->add_class($counter == 1 ? "active show" : "");
                $nav->add_content((new HTMLElement("li", false))->add_content($button)->add_class($counter == 1 ? "active" : ""));
            }
        }

        //dd($this->contents);

        $nav = (new HtmlElement("div"))->add_class("navbar")->add_content(
            (new HtmlElement("div"))->add_class("navbar-inner")->add_content(
                (new HtmlElement("div"))->add_content(
                    $nav
                )));

        $bar = (new HtmlElement("div"))->set_id("bar")->add_class("progress progress-striped active")->add_content(
            (new HtmlElement("div"))->add_class("progress-bar")
        );

        $tabcontent = (new HTMLElement("div", false))
            ->add_class("tab-content")
            ->add_attr("style", "padding-top:40px;")
            ->add_content($contents);
        if($buttons_row){
            $tabcontent->add_content($buttons_row->getHtmlElement());
        }

            /*->add_content('
		<ul class="pager wizard">
			<li class="previous"><a href="javascript:;" class="btn btn-secondary text-white">Önceki</a></li>
		  	<li class="next"><a href="javascript:;" class="btn btn-primary text-white">Sonraki</a></li>
			<li class="finish_"><button type="submit" class="btn btn-secondary text-white">Kaydet</button></li>
		</ul>')*/
        $tabcontent->add_content("<div class='clearfix'></div>");


        $tabcontent = (new HtmlElement('div'))->add_class("form")/*->add_content((new HtmlElement('form'))*/->add_content($tabcontent);

        $el->add_class("rootwizard")->add_content($nav)
            ->add_content($bar)
            ->add_content(
                $tabcontent
            );

        /*$el = (new HtmlElement('div'))->add_class("col-md-12 pa")->add_content(
            (new HtmlElement('div'))->add_class("step")->add_content(
                $el
            ));*/
        return $el;
    }
}
