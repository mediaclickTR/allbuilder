<?php

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;

class ScriptPusher extends BuilderRenderable
{
    public const INPUT = "input";
    public const ITEMS = "items";
    public const DESCRIPTION = "description";
    public const VALUE = "value";
    public const INPUT_TEXT = "input_text";
    public $info = [
        "icon_key" => "code",
        "object_key" => "ScriptPusher",
        "object_class" => __CLASS__,
        "object_tags" => [
            "basic_html", "form"
        ],
        "name" => "ScriptPusher",
        "description" => "Verilen script'i scripts stack ' ine push eder.",
        self::ITEMS => [
            "options" => [
                self::ITEMS => [
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "readonly_text",
                                "default_value" => "div"
                            ],
                            "attributes" => [
                                self::ITEMS => [
                                ]
                            ]
                        ]
                    ],
                    "script" => [
                        "key" => "script",
                        "name" => "Script",
                        self::DESCRIPTION => "&lt;script&gt; etiketi ile push edilecek çıplak js kodu.",
                        "type" =>"input_textarea",
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "$(document).ready(function(){ alert(\"Your code here.\"); });"
                    ],
                ]
            ],
        ],
    ];


    public $options = [
        self::VALUE => null,
        "html" => [
            "tag" => self::INPUT,
            "void_element" => true,
            "attributes" => [
                "type" => "text"
            ]
        ],
        "script"=>"",
    ];

    public function __construct(array $params = [], array $contents = [], array $options = [], array $data = [])
    {
        parent::__construct($params, $contents, $options, $data);
        // Script push edildi
        $script = $this->options['script'] ?? "";
        $this->extendStack("scripts", "<script>".$script."</script>");
        $this->mergeStacks();

    }

    function getHtmlElement()
    {

        // Basılmayacak eleman döndür:
        return new \Mediapress\Foundation\HtmlElement();
    }

    public $collectable_as = [];

}
