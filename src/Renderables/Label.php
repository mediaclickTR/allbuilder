<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;

class Label extends BuilderRenderable
{
    public const LABEL = "label";
    public const ITEMS = "items";
    public $info = [
        "icon_key" => "terminal",
        "object_key" => "Label",
        "object_class" => __CLASS__,
        "object_tags" => [
        
        ],
        "name" => "HTML Label",
        "description" => "Standart Label etiketi",
        self::ITEMS => [
            "options" => [
                self::ITEMS => [
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "readonly_text",
                                "default_value" => self::LABEL
                            ],
                            "attributes" => [
                                self::ITEMS => [
                                    "class" => [
                                        "type" => "input_text",
                                        "default_value" => ""
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
        ],
    ];
    
    public $options = [
        "html" => [
            "tag" => self::LABEL,
            "void_element" => false,
        ]
    ];
    
    public $collectable_as = [self::LABEL];
}