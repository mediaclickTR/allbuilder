<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\HtmlElement;
use Mediapress\Modules\Content\Facades\Content;
use Mediapress\Modules\MPCore\Models\CountryGroup;

class DetailTabs extends BuilderRenderable
{
    public const DETAIL = "detail";
    public const DESCRIPTION = "description";
    public const ITEMS = "items";
    public const OPTIONS = "options";
    public const KEY = "key";
    public const NAME = "name";
    public const CUSTOM_TEMPLATE = "custom_template";
    public const DEFAULT_VALUE = "default_value";
    public const TYPE = "type";
    public const ATTRIBUTES = "attributes";
    public const CAPSULATE = "capsulate";
    public const META_VARIABLES = "meta_variables";
    public const INPUT_TEXT = "input_text";
    public const PARAMS = "params";
    public const KEYNAME = "keyname";
    public const ITEMNAME = "itemname";
    public const ARRAY1 = "array";
    public const OPTIONS_HTML_ATTRIBUTES_CLASS = "options.html.attributes.class";
    public $collectable_as = ["tabs", "detailtabs"];

    private $rename_search = self::DETAIL;
    private $rename_replace = self::DETAIL;


    public $info = [
        "icon_key" => "window-restore",
        "object_key" => "DetailTabs",
        "object_class" => __CLASS__,
        "object_tags" => [
            "mediapress", "container", "object_specific"
        ],
        self::NAME => "Çoklu Detail Sekmeleri",
        self::DESCRIPTION => "Verilen Detail nesnelerini sekme grubu olarak sunan nesne",
        self::ITEMS => [
            self::OPTIONS => [
                self::ITEMS => [
                    self::CAPSULATE => [
                        self::KEY => self::CAPSULATE,
                        self::NAME => "Detay Kapsül Grubu",
                        self::DESCRIPTION => "Birbirine bağımlı çalışabilen detay eksenli nesnelere, detay bilgilerini servis etmek için bu nesneyi kullanmanızı sağlar. Bir sayfada en fazla bir detay kapsül grubu olarak işaretlenmiş kontrol bulunabilir",
                        self::TYPE => "radio",
                        "values" => [
                            "1" => "Evet",
                            "0" => "Hayır",
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => true
                    ],
                    "delete_restore_details"=>[
                        self::KEY => "delete_restore_details",
                        self::NAME => "Detay Sil / Geri Getir",
                        self::DESCRIPTION => "Her detay başlığında o detayı silmek / geri getirmek için tuş gösterilir",
                        self::TYPE => "radio",
                        "values" => [
                            "1" => "Evet",
                            "0" => "Hayır",
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => false
                    ],
                    self::META_VARIABLES => [
                        self::KEY => self::META_VARIABLES,
                        self::NAME => "Detayların Meta Verilerini Çek",
                        self::DESCRIPTION => "Bu kontrolle sunulan detay nesnelerinin meta özellikleri işlenecekse bu seçeneği etkinleştirin.",
                        self::TYPE => "radio",
                        "values" => [
                            "1" => "Evet",
                            "0" => "Hayır",
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => true
                    ],
                    "html" => [
                        self::ITEMS => [
                            self::ATTRIBUTES => [
                                self::ITEMS => [
                                    "class" => [
                                        self::TYPE => self::INPUT_TEXT,
                                        self::DEFAULT_VALUE => "tab-list"
                                    ],
                                ]
                            ]
                        ]
                    ],
                ]
            ],
            self::PARAMS => [
                self::ITEMS => [
                    self::KEYNAME => [
                        self::KEY => self::KEYNAME,
                        self::NAME => "Döngü anahtar değişkeni adı",
                        self::DESCRIPTION => "Koleksiyon için girilen foreach döngüsünde, koleksiyon elemanının anahtar değerini taşıyacak değişkenin adı",
                        self::TYPE => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => self::KEY
                    ],
                    self::ITEMNAME => [
                        self::KEY => self::ITEMNAME,
                        self::NAME => "Döngü değer değişkeni adı",
                        self::DESCRIPTION => "Koleksiyon için girilen foreach döngüsünde, koleksiyon elemanının değerini taşıyacak değişkenin adı",
                        self::TYPE => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => self::DETAIL
                    ],
                    self::ARRAY1 => [
                        self::KEY => self::ARRAY1,
                        self::NAME => "Detail Koleksiyonu",
                        self::DESCRIPTION => "Detail koleksiyonu. Ebeveyn nesnelerin yönetimine bırakmak için devre dışı bırakın.",
                        self::TYPE => "input_array",
                        "collection_items" => [],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "[]"
                    ],
                    "details_parent" => [
                        self::KEY => "details_parent",
                        self::NAME => "Detail sahibi nesne",
                        self::DESCRIPTION => "Detail sınıflarına sahip nesne",
                        self::TYPE => "object_selector",
                        "object_selector_parameters" => [
                            "selector_type" => "",
                            "selectable_objects" => []
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "<var>page|sitemap|category|criteria|property</var>"
                    ]
                ]
            ]
        ],
    ];

    public $options = [
        "html" => [
            "tag" => "div",
            "void_element" => false
        ],
        "delete_restore_details"=>false,
        "forced_html_classes" => ["tab-list", "details-wrapper"]
    ];

    public $params = [
        "details" => [],
        "details_parent" => null
    ];


    // Bu

    public function __construct(array $params = [], array $contents = [], array $options = [], array $data = [])
    {
        parent::__construct($params, $contents, $options, $data);
        $contents = $this->contents;


        $keyname = $this->params[self::KEYNAME] ?? self::KEY;
        $itemname = $this->params[self::ITEMNAME] ?? self::DETAIL;
        $this->rename_search = $itemname;

        $details_parent = $this->params['details_parent'];

        $array = ((is_countable($this->params["details"]) && count($this->params["details"])) ? $this->params["details"] : null) ;

        $array = $array ?? (is_object($details_parent) ? $details_parent->details()->withTrashed()->get() : []);
        if(auth()->user()->country_group){
            $array = $array->where('country_group_id', auth()->user()->country_group);
        }
        $array = collect($array)->sortBy('country_group_id');
        /*dump($array);*/

        // be sure this element has "tab-list" class
        /*$current_class = data_get($this->options, self::OPTIONS_HTML_ATTRIBUTES_CLASS, "");
        $renewed_class = implode(" ", array_filter([$current_class, "tab-list"]));
        data_set($this->options, self::OPTIONS_HTML_ATTRIBUTES_CLASS, $renewed_class);*/

        $tabs = [];
        $modeltab = array_shift($this->contents);

        $current_class = data_get($modeltab, self::OPTIONS_HTML_ATTRIBUTES_CLASS, "");
        $renewed_class = implode(" ", array_filter([$current_class, "has-detail-in"]));
        data_set($modeltab, self::OPTIONS_HTML_ATTRIBUTES_CLASS, $renewed_class);

        if (isset($this->options[self::CAPSULATE]) && $this->options[self::CAPSULATE]) {
            $current_class = data_get($modeltab, self::OPTIONS_HTML_ATTRIBUTES_CLASS, "");
            $renewed_class = implode(" ", array_filter([$current_class, "detail-capsule"]));
            data_set($modeltab, self::OPTIONS_HTML_ATTRIBUTES_CLASS, $renewed_class);
        }

        foreach ($array as ${$keyname} => ${$itemname}) {
            $tab = $modeltab;
            $detail = ${$itemname};
            //dump($tab);
            $detail_class = get_class($detail);
            $parent_class = preg_replace('/Detail$/', '', $detail_class);

            $is_deleted = !is_null($detail->deleted_at);

            $meta_variables = (isset($this->options[self::META_VARIABLES]) && $this->options[self::META_VARIABLES]) ?
                Content::getMetaVariablesFor([$detail_class, ${$itemname}->id], []) :
                [];

            $meta_templates = (isset($this->options[self::META_VARIABLES]) && $this->options[self::META_VARIABLES]) ?
                Content::getMetaTemplatesOf(${$itemname}) :
                [];

            $url_prefix = isset($website) ? $website->id : "yok";

            data_set($tab, "options.html.attributes.data-detail-type", $detail_class);
            data_set($tab, "options.html.attributes.data-detail-deleted", $is_deleted ? 1 : 0);
            data_set($tab, "options.html.attributes.data-detail-id", ${$itemname}->id);
            data_set($tab, "options.html.attributes.data-language-id", ${$itemname}->language_id);
            data_set($tab, "options.html.attributes.data-country-group-id", ${$itemname}->country_group_id);
            data_set($tab, "options.html.attributes.data-parent-type", $parent_class);
            data_set($tab, "options.html.attributes.data-parent-id", ${$itemname}->parent_id);
            data_set($tab, "options.html.attributes.data-full-url", ($url_prefix . (${$itemname}->url->url ?? "")) ?? "");
            data_set($tab, "options.html.attributes.data-slug", ${$itemname}->slug ?? "");
            data_set($tab, "options.html.attributes.data-meta-variables", json_encode($meta_variables));
            data_set($tab, "options.html.attributes.data-meta-templates", json_encode($meta_templates));

            if($is_deleted){
                $tab = $this->forceClassOnRenderableArray($tab,["remove-on-submit", "formstorer-ignore-tree"]);
            }

            $find_alias = array_search($detail_class, $this->options["object_types"]);

            if ($find_alias !== false) {
                $this->rename_replace = $find_alias . "->" . ${$itemname}->id;
            }
            $tab = $this->map_raw_contents([$this, "renameFormField"], $tab);

            if (!isset($tab[self::PARAMS])) {
                $tab[self::PARAMS] = [];
            }

            data_set($tab, 'options.title', get_variation_local_title(${$itemname}), false);


            $tab[self::PARAMS][$itemname] = ${$itemname};
            $tabs[] = $tab;
        }
        $this->contents += $this->parseAnnotations($tabs);
        //dump($this);
    }

    protected function renameFormField($field)
    {
        $name = $field[self::OPTIONS]["html"][self::ATTRIBUTES][self::NAME] ?? null;
        if (!is_null($name)) {
            if (\Str::startsWith($name, $this->rename_search)) {
                $field[self::OPTIONS]["html"][self::ATTRIBUTES][self::NAME] = preg_replace('/^' . preg_quote($this->rename_search, '/') . '/', $this->rename_replace, $name);
            }
        }
        return $field;
    }


    public function getHtmlElement()
    {
        if ($this->ignored_if) {
            return (new HtmlElement());
        }
         $key=uniqid();
        $el = $this->getSelfHtmlElement();
        $contents = $this->getContentHtmlElements();
        $collection = collect($contents)->groupBy('attrvaluepairs.data-country-group-id')->toArray();

        if(count($collection)>1 && count($contents)>5) {
            $countryNavs = (new HTMLElement("ul", false))->add_class("nav nav-tabs");
            $cgEl = [];
            $count = 0;
            foreach ($collection as $cgKey=>$cgContent) {

                $cgHtml = new HtmlElement("div", false);
                $cg = CountryGroup::select('list_title')->find($cgKey);
                $cgHtml->add_attr('id','tab-cg-'.$cgKey.'-'.$key)->add_content($cgContent)->add_class('tab-pane')
                    ->add_attr('data-tab-title',$cg->list_title)
                    ->add_attr('data-data-country-group-id',$cgKey);
                $cgContent = $cgHtml;
                $nav = (new HTMLElement("ul", false))->add_class("nav nav-tabs");
                $counter = 0;
                $cgEl[$cgKey] = (new HtmlElement('div', false));
                $button = (new HtmlElement('a', false))
                    ->add_attr("href", '#' . $cgContent->get_id())
                    ->data('toggle', 'tab')
                    ->add_class($cgKey == array_key_first($collection) ? "active show" : "")
                    ->add_content($cgContent->data('tab-title'));
                $countryNavs->add_content((new HTMLElement("li", false))->add_content($button));

                foreach ($cgContent->contents() as $ckey => $content) {


                    if (is_a($content, "Mediapress\\Foundation\\HTMLElement") && $content->has_class("tab-pane") && $content->get_tag()) {
                        $counter++;
                        if ($counter == 1) {
                            $content->add_class("active show");
                        }
                        $content->add_class("fade");


                        $drb_class1 = $content->data("detail-deleted")==1 ? 'fa-plus' : 'fa-close';
                        $drb_class2 = $content->data("detail-deleted")==1 ? 'text-primary' : 'text-danger' ;

                        $delete_restore_button = (new HtmlElement('i', false))->add_class('fa')->add_class($drb_class1)->add_class($drb_class2);

                        $button = (new HtmlElement('a', false))
                            ->add_attr("href", '#' . $content->get_id())
                            ->data('toggle', 'tab')
                            ->add_class($counter == 1 ? "active show" : "")
                            ->add_content($content->data('tab-title'));

                        $copyButton = (new HtmlElement('i', false))
                            ->add_class('fa fa-copy')
                            ->add_attr('data-lg', $content->data('language-id'))
                            ->add_attr('data-cg', $content->data('country-group-id'));

                        $button->add_content($copyButton);

                        if($this->options["delete_restore_details"]){
                            $button->add_content($delete_restore_button);
                        }

                        if($content->data("detail-deleted")==1){
                            $button->add_class("disabled");
                        }




                        $nav->add_content((new HTMLElement("li", false))->add_content($button));
                    }

                }
                $tabcontent[$cgKey] = (new HTMLElement("div", false))->add_class("tab-content")->add_attr("style", "padding-top:40px;");

                $cgEl[$cgKey]->add_content($nav)
                    ->add_attr('id','tab-cg-'.$cgKey.'-'.$key)->add_class('tab-pane')
                    ->add_attr('data-tab-title',$cg->title)
                    ->add_attr('data-data-country-group-id',$cgKey)
                    ->add_content(
                        $tabcontent[$cgKey]->add_content($cgContent->contents())
                            ->add_content("<div class='clearfix'></div>")
                    );
                if($count++ ==0){
                    $cgEl[$cgKey]->add_class(' active show');
                }


            }
            $tabcontent = (new HTMLElement("div", false))->add_class("tab-content")->add_attr("style", "padding-top:40px;");

            $el->add_content($countryNavs)
                ->add_content(
                    $tabcontent->add_content($cgEl)
                        ->add_content("<div class='clearfix'></div>")
                );
        }else{
            $counter = 0;
            $nav = (new HTMLElement("ul", false))->add_class("nav nav-tabs");

            foreach ($contents as $ckey => $content) {
                if (is_a($content, "Mediapress\\Foundation\\HTMLElement") && $content->has_class("tab-pane") && $content->get_tag()) {
                    $counter++;
                    if ($counter == 1) {
                        $content->add_class("active show");
                    }
                    $content->add_class("fade");


                    $drb_class1 = $content->data("detail-deleted")==1 ? 'fa-plus' : 'fa-close';
                    $drb_class2 = $content->data("detail-deleted")==1 ? 'text-primary' : 'text-danger' ;

                    $delete_restore_button = (new HtmlElement('i', false))->add_class('fa')->add_class($drb_class1)->add_class($drb_class2);

                    $button = (new HtmlElement('a', false))
                        ->add_attr("href", '#' . $content->get_id())
                        ->data('toggle', 'tab')
                        ->add_class($counter == 1 ? "active show" : "")
                        ->add_content($content->data('tab-title'));

                    if($this->options["delete_restore_details"]){
                        $button->add_content($delete_restore_button);
                    }

                    if($content->data("detail-deleted")==1){
                        $button->add_class("disabled");
                    }

                    $nav->add_content((new HTMLElement("li", false))->add_content($button));
                }
            }

            $tabcontent = (new HTMLElement("div", false))->add_class("tab-content")->add_attr("style", "padding-top:40px;");

            if(count($contents) == 1){
                $nav = null;
            }
            $el->add_content($nav)
                ->add_content(
                    $tabcontent->add_content($contents)
                        ->add_content("<div class='clearfix'></div>")
                );
        }

        return $el;
    }
}
