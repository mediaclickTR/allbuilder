<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\HtmlElement;

class ToggleSwitch extends BuilderRenderable
{
    public const LABEL = "label";
    public const CONTENTS = "contents";
    public const WRAPPER = "wrapper";
    public const COMPONENTS_LABEL_OPTIONS_HTML_ATTRIBUTES_CLASS = "components.label.options.html.attributes.class";
    public const COMPONENTS = "components";
    public const OFF_TEXT = "off_text";
    public const OFF_VALUE = "off_value";
    public const ON_VALUE = "on_value";
    public const VALUE = "value";
    public const ITEMS = "items";
    public const DESCRIPTION = "description";
    public const ATTRIBUTES = "attributes";
    public const CLASS1 = "class";
    public const INPUT_TEXT = "input_text";
    public const CHECKBOX = "checkbox";
    public const CUSTOM_TEMPLATE = "custom_template";
    public const ON_TEXT = "on_text";
    public const OPTIONS = "options";
    public const INPUT = "input";
    public const DEFAULT_VALUE = "default_value";
    public $info = [
        "icon_key" => "check-circle",
        "object_key" => "ToggleSwitch",
        "object_class" => __CLASS__,
        "object_tags" => [
            "mediapress", "form"
        ],
        "name" => "Aç/Kapa Onay Kutusu",
        self::DESCRIPTION => "Aç/Kapa türü Onay Kutusu. Hem açıkken hem kapalıyken değer taşır.",
        self::ITEMS => [
            self::OPTIONS => [
                self::ITEMS => [
                    self::VALUE => [
                        "key" => self::VALUE,
                        "name" => "Değer",
                        self::DESCRIPTION => "",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => 1
                    ],
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "readonly_text",
                                self::DEFAULT_VALUE => self::INPUT
                            ],
                            self::ATTRIBUTES => [
                                self::ITEMS => [
                                    self::CLASS1 => [
                                        "type" => self::INPUT_TEXT,
                                        self::DEFAULT_VALUE => "form-check-input"
                                    ],
                                    "type" => [
                                        "key" => "type",
                                        "name" => "HTML Input Type",
                                        self::DESCRIPTION => "",
                                        "type" => self::INPUT_TEXT,
                                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                                        self::DEFAULT_VALUE => self::CHECKBOX
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
            "data" => [
                self::ITEMS => [
                    self::ON_VALUE => [
                        "key" => self::ON_VALUE,
                        "name" => "Nesne açık(işaretli) iken taşıyacağı değer",
                        self::DESCRIPTION => "Nesne işaretli olduğunda, bu değer sunucuya gönderilir",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => 1
                    ],
                    self::ON_TEXT => [
                        "key" => self::ON_TEXT,
                        "name" => "Nesnenin açık seçeneği için etiket",
                        self::DESCRIPTION => "",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "Evet"
                    ],
                    self::OFF_VALUE => [
                        "key" => self::OFF_VALUE,
                        "name" => "Nesne kapalı iken taşıyacağı değer",
                        self::DESCRIPTION => "Nesne işaretli olmadığında, bu değer sunucuya gönderilir",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => 0
                    ],
                    self::OFF_TEXT => [
                        "key" => self::OFF_TEXT,
                        "name" => "Nesnenin kapalı seçeneği için etiket",
                        self::DESCRIPTION => "",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "Hayır"
                    ]
                ]
            ]
        ],
    ];


    public $options = [
        self::VALUE => 0,
        "html" => [
            "tag" => self::INPUT,
            "void_element" => true,
            self::ATTRIBUTES => [
                "type" => self::CHECKBOX
            ]
        ],
        self::COMPONENTS => [
            self::WRAPPER => [
                "type" => "div",
                self::OPTIONS => [
                    "html" => [
                        self::ATTRIBUTES => [
                            self::CLASS1 => "on-off"
                        ]
                    ],
                    "collectable_as" => ["on-off-wrapper"]
                ],
                self::CONTENTS => []

            ],
            self::LABEL => [
                "type" => self::LABEL,
                self::OPTIONS => [
                    "html" => [
                        self::ATTRIBUTES => [
                            self::CLASS1 => "switch-toggle"
                        ]
                    ],
                    "collectable_as" => [self::LABEL, "checkbox-label"]
                ],
                self::CONTENTS => []
            ]
        ],
    ];

    public $collectable_as = [self::CHECKBOX, self::INPUT, "formfield"];

    public $data = [
        self::ON_VALUE => 1,
        self::OFF_VALUE => 0,
        self::ON_TEXT => "Evet",
        self::OFF_TEXT => "Hayır",
    ];


    public function getHtmlElement()
    {
        //$wrapper = isset($this->params["wrapper"]) ?

        if ($this->ignored_if) {
            return (new HtmlElement());
        }

        $use_old_input = !(isset($this->options["dont_use_old_input"]) && $this->options["dont_use_old_input"]);

        $ul = (new HtmlElement("ul", false));

        $for = data_get($this->options, "components.label.options.html.attributes.for", null);

        if (!$for) {
            data_set($this->options, "components.label.options.html.attributes.for", $this->createClassID(), false);
        }

        $class = data_get($this->options, self::COMPONENTS_LABEL_OPTIONS_HTML_ATTRIBUTES_CLASS, null);
        if (!$class || !substr_count($class, "switch-toggle")) {
            data_set($this->options, self::COMPONENTS_LABEL_OPTIONS_HTML_ATTRIBUTES_CLASS, $class . " switch-toggle", true);
        }

        data_set($this->options, "components.label.options.html.attributes.data-on", e($this->data[self::ON_VALUE]), true);
        data_set($this->options, "components.label.options.html.attributes.data-off", e($this->data[self::OFF_VALUE]), true);

        data_set($this->options, "html.attributes.checked", "checked", false);

        $label_class = "off";

        $name = data_get($this->options,"html.attributes.name",null);
        if($use_old_input && $name && array_key_exists($name, old())){
            $value = old($name);
        }else{
            $value = $this->options[self::VALUE] ?? null;
        }

        if (in_array($value, array($this->data[self::ON_VALUE], $this->data[self::OFF_VALUE]))) {
            data_set($this->options, "html.attributes.value", $value, true);
            if ($value == $this->data[self::ON_VALUE]) {
                $label_class = "on";
            }
        }



        $lbl_class = data_get($this->options, self::COMPONENTS_LABEL_OPTIONS_HTML_ATTRIBUTES_CLASS, null);
        $lbl_class = preg_split("/\s/", $lbl_class);
        if (!in_array($label_class, $lbl_class)) {
            $lbl_class[] = $label_class;
        }
        $lbl_class = implode(" ", $lbl_class);
        data_set($this->options, self::COMPONENTS_LABEL_OPTIONS_HTML_ATTRIBUTES_CLASS, $lbl_class);


        $label = $this->buildRenderableFromArray($this->options[self::COMPONENTS][self::LABEL])->getHtmlElement();

        $onText = \Str::slug(strip_tags($this->data[self::ON_TEXT]));
        $offText = \Str::slug(strip_tags($this->data[self::OFF_TEXT]));
        $this->data[self::ON_TEXT] = getPanelLangPartKey($onText, $this->data[self::ON_TEXT]);
        $this->data[self::OFF_TEXT] = getPanelLangPartKey($offText, $this->data[self::OFF_TEXT]);

        $span1 = (new HtmlElement("span", false))->add_content($this->data[self::ON_TEXT]);
        $li1 = (new HtmlElement("li", false))->add_content($span1);
        $span2 = (new HtmlElement("span", false))->add_content($this->data[self::OFF_TEXT]);
        $li2 = (new HtmlElement("li", false))->add_content($span2);

        $el = parent::getHtmlElement();

        $li_mid = (new HtmlElement("li", false))
            ->add_content($label)
            ->add_content($el);

        $ul->add_content($li2)->add_content($li_mid)->add_content($li1);


        $this->options[self::COMPONENTS][self::WRAPPER][self::CONTENTS][] = $ul;
        $wrapper = $this->buildRenderableFromArray($this->options[self::COMPONENTS][self::WRAPPER]);

        return $wrapper->getHtmlElement();
    }


}
