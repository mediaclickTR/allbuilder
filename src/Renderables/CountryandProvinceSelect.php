<?php
/**
 * Created by PhpStorm.
 * User: kahraman
 * Date: 16.01.2020
 * Time: 16:07
 * Status: country and province rederable eklendiği için , ve bu iki renderable birlikte çalışabildiği için eklenmesi iptal olmuştur.
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Facades\DataSourceEngine;
use Mediapress\Foundation\HtmlElement;

class CountryandProvinceSelect extends Select
{
    public const SELECTED = "selected";
    public const OPTION = "option";
    public const VALUE = "value";
    public const SELECT = "select";
    public const DEFAULT_TEXT = "default_text";
    public const MERGE = "merge";
    public const ADDITIONAL_CONTENT = "additional_content";
    public const SHOW_DEFAULT_OPTION = "show_default_option";
    public const VALUES = "values";
    public const RADIO = "radio";
    public const MULTIPLE = "multiple";
    public const DEFAULT_VALUE = "default_value";
    public const CUSTOM_TEMPLATE = "custom_template";
    public const INPUT_TEXT = "input_text";
    public const ITEMS = "items";
    public const DESCRIPTION = "description";
    public const TITLE = "title";
    public const COMPONENTS = "components";
    public const CLASS1 = "class";
    public const CONTENTS = "contents";
    public const LABEL = "label";
    public const WRAPPER = "wrapper";
    public const SEO_ACTIVE = "seo-active";
    public const INPUT = "input";


    public $info = [
        "icon_key" => "caret-square-down",
        "object_key" => "CountryandProvinceSelect",
        "object_class" => __CLASS__,
        "object_tags" => [
            "basic_html", "form"
        ],
        "name" => "Ülke ve Şehir Seçici",
        self::DESCRIPTION => "ülke ve ülkeye bağlı Şehir seçimi yapabileceğiniz bir HTML Select",
        self::ITEMS => [
            "options" => [
                self::ITEMS => [
                    "title" => [
                        "key" => "title",
                        "name" => "Başlık",
                        self::DESCRIPTION => "Nesnenin başlığı",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "Ülke ve Şehir Seçin"
                    ],
                    self::VALUE => [
                        "key" => self::VALUE,
                        "name" => "Değer",
                        self::DESCRIPTION => "Nesneye atanacak değer",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "<print>page->cint_4</print>"
                    ],
                    self::SHOW_DEFAULT_OPTION => [
                        "key" => self::SHOW_DEFAULT_OPTION,
                        "name" => "Varsayılan Seçimi Göster",
                        self::DESCRIPTION => "Kullanıcı seçim yapmadan önce seçili görünecek bir değer olup olmayacağını belirler",
                        "type" => self::RADIO,
                        self::VALUES => [
                            "1" => "Evet",
                            "0" => "Hayır",
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => null
                    ],
                    self::ADDITIONAL_CONTENT => [
                        "key" => self::ADDITIONAL_CONTENT,
                        "name" => "Data ile üretilen içerik",
                        self::DESCRIPTION => "Data parametrelerinden üretilen içeriği, hali hazırda bulunan içerikle değiştirilmesini ya da ona eklenmesini belirler",
                        "type" => self::RADIO,
                        self::VALUES => [
                            "replace" => "Yer değiştir",
                            self::MERGE => "Birleştir",
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => self::MERGE
                    ],
                    self::DEFAULT_VALUE => [
                        "key" => self::DEFAULT_VALUE,
                        "name" => "Varsayılan eleman değeri",
                        self::DESCRIPTION => "",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => null
                    ],
                    self::DEFAULT_TEXT => [
                        "key" => self::DEFAULT_TEXT,
                        "name" => "Varsayılan eleman metni",
                        self::DESCRIPTION => "",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "---Seçiniz---"
                    ],
                    "country_name" => [
                        "key" => "country_name",
                        "name" => "Ülke kayıt yeri",
                        self::DESCRIPTION => "",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "page-><print>page->id</print>->cint_3"
                    ],
                    "country_value" => [
                        "key" => "country_value",
                        "name" => "Ülke değeri",
                        self::DESCRIPTION => "",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "<print>page->cint_3</print>"
                    ],
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "readonly_text",
                                self::DEFAULT_VALUE => self::SELECT
                            ],
                            "attributes" => [
                                self::ITEMS => [
                                    "name" => [
                                        "type" => self::INPUT_TEXT,
                                        self::DEFAULT_VALUE => "page-><print>page->id</print>->cint_4"
                                    ],
                                    "class" => [
                                        "type" => self::INPUT_TEXT,
                                        self::DEFAULT_VALUE => ""
                                    ],
                                    "data-live-search" => [
                                        "key" => "data-live-search",
                                        "name" => "Seçeneklerde arama Yapılabilsin mi?",
                                        "type" => self::RADIO,
                                        self::VALUES => [
                                            "true" => "Evet",
                                            "false" => "Hayır",
                                        ],
                                        self::DESCRIPTION => "",
                                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                                        self::DEFAULT_VALUE => "true"
                                    ],
                                ]
                            ]
                        ]
                    ],
                ]
            ]

        ],
    ];

    public $options = [
        self::TITLE => "Ülke ve Şehir Seçin",
        self::VALUE => null,
        self::SEO_ACTIVE => true,
        "html" => [
            "tag" => self::SELECT,
            "void_element" => false,
            "attributes" => [
                "name" => "page-><print>page->id</print>->cint_4",
                "id" => "select-province",
                "data-live-search" => "true",
                "data-width" => "350",
                "data-size" => "8",
            ]
        ],
        "primary_key" => "id",
        self::MULTIPLE => false,
        self::ADDITIONAL_CONTENT => self::MERGE, // replace
        self::SHOW_DEFAULT_OPTION => true,
        self::DEFAULT_TEXT => "",
        self::DEFAULT_VALUE => "",
        "forced_html_classes" => ["selectpicker"],
        "country_name" => "page-><print>page->id</print>->cint_4",
        "country_value" => "<print>page->cint_4</print>",
        self::COMPONENTS => [
            self::WRAPPER => [
                "type" => "formgroup",
                self::CONTENTS => []
            ],
            self::SELECT => [
                "type" => "countryselect",
                "options" => [
                    "html" => [
                        "tag" => self::SELECT,
                        "void_element" => false,
                        self::ATTRIBUTES => ["id" => "select-country-special"],
                        "name" => ""
                    ],
                    "collectable_as" => ["select"],
                    "value" =>""
                ],
                self::CONTENTS => []
            ]
        ]
    ];

    public $data = [
        "values" => []
    ];


    public function __construct(array $params = [], array $contents = [], array $options = [], array $data = [])
    {

        $this->options = array_replace_recursive(
            $this->baseOptions(),
            array_replace_recursive(
                $this->options,
                array_replace_recursive(
                    $this->defaultOptions(),
                    $options)
            )
        );

        parent::__construct($params, $contents, $options, $data);
        $this->extendStack("scripts", $this->script());

    }


    public $collectable_as = [self::SELECT, "input", "formfield", "country_select"];

    private function script()
    {
        $script = <<<SCRIPT
    <script>
    $(document).ready(function(){             
        $("#select-country-special").change(function(){      
             var country_id = $(this).children("option:selected").val();           
            if(typeof pageCountryandProvinceSelectionChanged === "function"){ 
                pageCountryandProvinceSelectionChanged(country_id);
            }else{
                console.warn("MEDIAPRESS400: pageCategoriesSelectionChanged(page_id,category_ids_arr) fonksiyonu tanımlanmamış. Seçilen kategorilere göre işlem yapabilmek için bu fonksiyonu tanımlayın.")
            }
        }).change();
    });
</script>
SCRIPT;
        return $script;

    }

    public function getHtmlElement()
    {
        if ($this->ignored_if) {
            return (new HtmlElement());
        }

        $el = parent::getHtmlElement();
        $el->data("field-title", $this->options[self::TITLE]);
        $el->data(self::SEO_ACTIVE, $this->options[self::SEO_ACTIVE]);

        data_set($this->options[self::COMPONENTS][self::SELECT], "options.html.attributes.for", $el->get_id());
        data_set($this->options[self::COMPONENTS][self::SELECT], "options.html.attributes.name",  $this->options["country_name"]);
        data_set($this->options[self::COMPONENTS][self::SELECT], "options.value",  $this->options["country_value"]);
        data_set($this->options[self::COMPONENTS][self::SELECT], "options.value",  $this->options["country_value"]);
        data_set($this->options[self::COMPONENTS][self::SELECT], "options.title",   $this->options[self::TITLE]);

        $select = $this->buildRenderableFromArray($this->options[self::COMPONENTS][self::SELECT]);
        $this->options[self::COMPONENTS][self::WRAPPER][self::CONTENTS][self::SELECT] = $select;
        $this->options[self::COMPONENTS][self::WRAPPER][self::CONTENTS][self::INPUT] = $el;
        $wrapper = $this->buildRenderableFromArray($this->options[self::COMPONENTS][self::WRAPPER]);

        return $wrapper->getHtmlElement();
    }


}
