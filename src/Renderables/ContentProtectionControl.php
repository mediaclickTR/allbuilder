<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\HtmlElement;

class ContentProtectionControl extends BuilderRenderable
{

    public const ITEMS = "items";
    public const OPTIONS = "options";
    public const DEFAULT_VALUE = "default_value";
    public const ATTRIBUTES = "attributes";
    public const CLASS_STR = "class";
    public const COMPONENTS = "components";
    public const CHECKBOX = "checkbox";
    public const TITLE = "title";
    public const PROTECTION_MODE_SELECTOR = "protection_mode_selector";
    public const PASS_WORD_INPUT = "password_input";
    public const VALUE = "value";
    public const ALLOWED_ROLE_ID_INPUT = "allowed_role_id_input";
    public $info = [
        "icon_key" => "lock",
        "object_key" => "ContentProtectionControl",
        "object_class" => __CLASS__,
        "object_tags" => [
            "mediapress", "form", "object_specific"
        ],
        "name" => "İçerik Koruma Kontrolü",
        "description" => "Gerektiğinde bir içeriğe erişimi sınırlandırabilmenizi sağlayan kontrol",
        self::ITEMS => [
            self::OPTIONS => [
                self::ITEMS => [
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "input_text",
                                self::DEFAULT_VALUE => "div"
                            ],
                            self::ATTRIBUTES => [
                                self::ITEMS => [
                                    self::CLASS_STR => [
                                        "type" => "input_text",
                                        self::DEFAULT_VALUE => "col-6"
                                    ],
                                ]
                            ]
                        ]
                    ],
                ]
            ],
            "params"=>[
                self::ITEMS=>[
                    "object"=>[
                        "key" => "object",
                        "name" => "Nesne",
                        "description" => "Korunacak Ana Nesne",
                        "type" => "input_text",
                        "custom_template" => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "&lt;var&gt;object&lt;/var&gt;"
                    ]
                ]
            ]
        ],
    ];

    public $options = [
        "html" => [
            "tag" => "div",
            self::ATTRIBUTES => [
                self::CLASS_STR => "col-6"
            ]
        ],
        self::COMPONENTS => [
            "wrapper" => [
                "type" => "div",
                self::OPTIONS => [
                    "html" => [
                        self::ATTRIBUTES => [
                            self::CLASS_STR => "pass-box left"
                        ]
                    ],
                ]
            ],
            self::CHECKBOX => [
                "type" => "icheck",
                self::OPTIONS => [
                    self::TITLE => "Kısıtlamalı Sayfa ",
                    "html" => [
                        self::ATTRIBUTES => [
                            self::CLASS_STR => "cpc_protected_page_checkbox"
                        ]
                    ],
                ],
            ],
            self::PROTECTION_MODE_SELECTOR => [
                "type" => "toggleswitch",
                "data" => [
                    "on_value" => 1,
                    "off_value" => 0,
                    "on_text" => "Kullanıcı rolüne göre",
                    "off_text" => "Parola ile",
                ],
                self::OPTIONS => [
                    "html" => [
                        self::ATTRIBUTES => [
                            self::CLASS_STR => "protection-mode-toggler"
                        ]
                    ]
                ]
            ],
            self::PASS_WORD_INPUT => [
                "type" => "inputwithlabel",
                self::OPTIONS => [
                    self::TITLE => "Parola belirtin",
                    "html" => [
                        self::ATTRIBUTES => [
                            self::CLASS_STR => "password-input",
                            self::VALUE => null
                        ]
                    ]
                ]
            ],
            self::ALLOWED_ROLE_ID_INPUT => [
                "type" => "selectwithlabel",
                self::OPTIONS => [
                    "html" => [
                        self::ATTRIBUTES => [
                            self::CLASS_STR => "allowed-role-id-input"
                        ]
                    ],
                    self::COMPONENTS => [
                        "label" => null
                    ],
                    self::VALUE => null,
                    "show_default_option" => true,
                    "default_text" => "---- Seçim Yok ----",
                    self::DEFAULT_VALUE => ""
                ],
                "data" => [
                    "values" => [
                        "type" => "ds:Auth->GetUserRolesForContentProtection",
                        "params" => [
                            "pluck" => true,
                            "pluck_key" => "id",
                            "pluck_column" => self::TITLE
                        ]
                    ]
                ]
            ]
        ]
    ];

    public $params = [
        "object" => null
    ];


    public function __construct(array $params = [], array $contents = [], array $options = [], array $data = [])
    {

        $this->class = get_class($this);
        $this->params = array_replace_recursive($this->params, array_replace_recursive($this->defaultParams(), $params));
        $this->params = $this->parseAnnotations($this->params, true);

        $this->options = array_replace_recursive(
            $this->baseOptions(),
            array_replace_recursive(
                $this->options,
                array_replace_recursive(
                    $this->defaultOptions(),
                    $options)
            )
        );
        $this->options = $this->parseAnnotations($this->options, true);

        extract($this->params);

        $wrapper = $this->options[self::COMPONENTS]["wrapper"];
        $checkbox = $this->options[self::COMPONENTS][self::CHECKBOX];
        $pms = $this->options[self::COMPONENTS][self::PROTECTION_MODE_SELECTOR];
        $password = $this->options[self::COMPONENTS][self::PASS_WORD_INPUT];
        $user_role = $this->options[self::COMPONENTS][self::ALLOWED_ROLE_ID_INPUT];

        $object = $this->params["object"] ?? null;


        if (is_object($object)) {
            $object_class = get_class($object);
            $object_key = $this->getObjectKey($object_class);

            $wrapper_contents = [];

            $counter = 0;
            $has_pass = false;
            $has_role_id = false;
            $nuller_pass_input = [
                "type" => "input",
                self::OPTIONS => [
                    "html" => [
                        self::ATTRIBUTES => [
                            self::CLASS_STR => "object_password",
                            "type" => "hidden",
                            "name" => $object_key . "->" . $object->id . "->password",
                            self::VALUE => "",
                        ]
                    ]
                ]
            ];
            $nuller_role_id_input = [
                "type" => "input",
                self::OPTIONS => [
                    "html" => [
                        self::ATTRIBUTES => [
                            self::CLASS_STR => "object_role_id",
                            "type" => "hidden",
                            "name" => $object_key . "->" . $object->id . "->allowed_role_id",
                            self::VALUE => "",
                        ]
                    ]
                ]
            ];

            $password[self::OPTIONS]['html'][self::ATTRIBUTES]['name'] = $object_key . "->" . $object->id . "->password";
            $password[self::OPTIONS]['html'][self::ATTRIBUTES]['class'] = 'object_password password-input';

            if ($object->password) {
                $has_pass = true;
            }
            if ($object->allowed_role_id) {
                $has_role_id = true;
            }


            data_set($checkbox, "options.checked", ($has_pass || $has_role_id), true);

            if ($has_role_id) {
                data_set($user_role, "options.html.attributes.value", $object->allowed_role_id, true);
                data_set($pms, "options.value", 1, true);
                data_set($nuller_role_id_input, "options.html.attributes.disabled", "disabled", true);
            } elseif ($has_pass) {
                data_set($password, "options.html.attributes.value", $object->password, true);
                data_set($pms, "options.value", 0, true);
                data_set($nuller_pass_input, "options.html.attributes.disabled", "disabled", true);
            }

            $wrapper["contents"] = [
                "nuller_password_input" => $nuller_pass_input,
                "nuller_role_id_input" => $nuller_role_id_input,
                self::CHECKBOX => $checkbox,
                self::PROTECTION_MODE_SELECTOR => $pms,
                self::PASS_WORD_INPUT => $password,
                self::ALLOWED_ROLE_ID_INPUT => $user_role
            ];

            $this->contents[] = $wrapper;

        }

        parent::__construct($params, $contents, $options, $data);


    }

    public function getHtmlElement()
    {
        if(!env('HIDE_PAGE_PASSWORD')){
            if ($this->ignored_if) {
                return (new HtmlElement());
            }

            $htmlid = $this->class_id;

            $script = <<<"EOT"
<script>
$(document).ready(function(){
    $(".cpc_protected_page_checkbox","#$htmlid").on("ifChanged",function(){
    });
    $(".cpc_protected_page_checkbox","#$htmlid").on("ifChecked",function(){
        $(".protection-mode-toggler","#$htmlid").closest(".on-off").show();
        $(".protection-mode-toggler","#$htmlid").trigger("change");
    });
    $(".cpc_protected_page_checkbox","#$htmlid").on("ifUnchecked",function(){
        $(".allowed-role-id-input","#$htmlid").closest(".form-group").hide();
        $(".password-input","#$htmlid").closest(".form-group").hide();
        $(".protection-mode-toggler","#$htmlid").closest(".on-off").hide();

        $(".object_password","#$htmlid").val("");
        $(".object_role_id","#$htmlid").val("");
    });

    $(".protection-mode-toggler","#$htmlid").change(function(){
        if($(this).val() == "0"){
            $(".allowed-role-id-input","#$htmlid").closest(".form-group").hide();
            $(".password-input","#$htmlid").closest(".form-group").show();
            $(".object_password","#$htmlid").val($(".password-input","#$htmlid").val());
            $(".object_role_id","#$htmlid").val("");
        }else if($(this).val() == 1){
            $(".password-input","#$htmlid").closest(".form-group").hide();
            $(".allowed-role-id-input","#$htmlid").closest(".form-group").show();
            $(".object_role_id","#$htmlid").val($(".allowed-role-id-input","#$htmlid").val());
            $(".object_password","#$htmlid").val("");
        }
    });

    if($(".cpc_protected_page_checkbox","#$htmlid").prop("checked")){
        $(".cpc_protected_page_checkbox","#$htmlid").trigger("ifChecked");
    }else{
        $(".cpc_protected_page_checkbox","#$htmlid").trigger("ifUnchecked");
    }

    $(".password-input","#$htmlid").change(function(){
        $(".object_password","#$htmlid").val($(this).val());
    });

    $(".allowed-role-id-input","#$htmlid").change(function(){
        $(".object_role_id","#$htmlid").val($(this).val());
    });

});

</script>
EOT;

            $this->extendStack("scripts", $script);

            return parent::getHtmlElement();
        }

    }

}
