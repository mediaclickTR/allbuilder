<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;

class Blank extends BuilderRenderable
{

    public const ITEMS = "items";
    public $collectable_as = ["blank"];
    public $info = [
        "icon_key" => "asterisk",
        "object_key" => "Blank",
        "object_class" => __CLASS__,
        "object_tags" => [
            "miscellaneous"
        ],
        "name" => "Boş Nesne",
        "description" => "Sıfırdan doldurulmak üzere boş bir nesne",
        self::ITEMS => [
            "options" => [
                self::ITEMS => [
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "input_text",
                                "default_value" => "any_tag_you_want"
                            ],
                            "attributes" => [
                                self::ITEMS => [
                                    "class" => [
                                        "type" => "input_text",
                                        "default_value" => ""
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
        ],
    ];
    
    public $options = [
        "html" => [
            "tag" => null
        ]
    ];
    
}