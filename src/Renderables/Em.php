<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;

class Em extends BuilderRenderable
{

    public const ITEMS = "items";
    public $collectable_as = ["em"];
    public $info = [
        "icon_key" => "asterisk",
        "object_key" => "Em",
        "object_class" => __CLASS__,
        "object_tags" => [
            "basic_html"
        ],
        "name" => "HTML Em",
        "description" => "Standart HTML Em Etiketi",
        self::ITEMS => [
            "options" => [
                self::ITEMS => [
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "readonly_text",
                                "default_value" => "em"
                            ],
                            "attributes" => [
                                self::ITEMS => [
                                    "class" => [
                                        "type" => "input_text",
                                        "default_value" => ""
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
        ],
    ];
    
    public $options = [
        "html" => [
            "tag" => "em"
        ]
    ];
    
}