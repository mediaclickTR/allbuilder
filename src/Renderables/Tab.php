<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;

class Tab extends BuilderRenderable
{
    
    public const TITLE = "title";
    public const TAB_PANE = "tab-pane";
    public const ITEMS = "items";
    public $info = [
        "icon_key" => "window-maximize",
        "object_key" => "Tab",
        "object_class" => __CLASS__,
        "object_tags" => [
            "container"
        ],
        "name" => "Sekme",
        "description" => "Sekme kapsayıcısına (SimpleTabs, StepTabs, DetailsTab vb.) eklenebilecek bir sekme(tab)",
        self::ITEMS => [
            "options" => [
                self::ITEMS => [
                    "html" => [
                        self::ITEMS => [
                            "attributes" => [
                                self::ITEMS => [
                                    "class" => [
                                        "type" => "input_text",
                                        "default_value" => self::TAB_PANE
                                    ]
                                ]
                            ]
                        ]
                    ],
                    self::TITLE => [
                        "key" => self::TITLE,
                        "name" => "Başlık",
                        "description" => "Nesnenin başlığı",
                        "type" => "input_text",
                        "custom_template" => "", // html - used if input_type is custom,
                        "default_value" => ""
                    ],
                ]
            ],
        ],
    ];
    
    public $options = [
        "html" => [
            "tag" => "div",
            "void_element" => false,
            "attributes" => [
                "class" => self::TAB_PANE
            ]
        ]
    ];
    public $collectable_as = ["tab"];
    
    
    protected function getSelfHtmlElement()
    {
        
        $current_class = data_get($this->options, "html.attributes.class", "");
        $renewed_class = implode(" ", array_filter([$current_class, self::TAB_PANE]));
        data_set($this->options, "html.attributes.class", $renewed_class);
        
        if(count($this->params) == 0) {
            $name = \Str::slug($this->options[self::TITLE]);
            $this->options[self::TITLE] = getPanelLangPartKey($name, $this->options[self::TITLE], true);
        }
        
        return $this->createHTMLElement($this->options)->add_attr('data-tab-title', $this->options[self::TITLE] ?? "");
    }
    
}