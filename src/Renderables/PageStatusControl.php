<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\HtmlElement;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\MPCore\Models\BaseModel;

class PageStatusControl extends RadiosAmigos
{


    public const DATEPICKER = "datepicker";
    public const DATEFIELD = "datefield";
    public const CLASS1 = "class";
    public const ATTRIBUTES = "attributes";
    public const VALUES = "values";
    public const MULTILINE = "multiline";
    public const DEFAULT1 = "default";
    public const VALUE = "value";
    public const DEFAULT_VALUE = "default_value";
    public const CUSTOM_TEMPLATE = "custom_template";
    public const INPUT_TEXT = "input_text";
    public const TITLE = "title";
    public const OPTIONS = "options";
    public const ITEMS = "items";
    public const DESCRIPTION = "description";
    public $info = [
        "icon_key" => "bullseye",
        "object_key" => "PageStatusControl",
        "object_class" => __CLASS__,
        "object_tags" => [
            "mediapress", "form", "object_specific"
        ],
        "name" => "Sayfa Durumu",
        self::DESCRIPTION => "Sayfa durumunu ayarlamaya yarayan seçenekler",
        self::ITEMS => [
            self::OPTIONS => [
                self::ITEMS => [
                    self::TITLE => [
                        "key" => self::TITLE,
                        "name" => "Başlık",
                        self::DESCRIPTION => "Nesnenin başlığı",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ""
                    ],
                    self::VALUE => [
                        "key" => self::VALUE,
                        "name" => "Seçili değer",
                        self::DESCRIPTION => "",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ""
                    ],
                    self::DEFAULT1 => [
                        "key" => self::DEFAULT1,
                        "name" => "Varsayılan Seçili değer",
                        self::DESCRIPTION => "",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => BaseModel::DRAFT
                    ],
                    self::MULTILINE => [
                        "key" => self::MULTILINE,
                        "name" => "Çok satırlı yerleşim",
                        self::DESCRIPTION => "Seçeneklerin dikey sıralanmasını sağlar.",
                        "type" => "radio",
                        self::VALUES => [
                            "0" => "Hayır",
                            "1" => "Evet",
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => 1
                    ],
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => self::INPUT_TEXT,
                                self::DEFAULT_VALUE => "div"
                            ],
                            self::ATTRIBUTES => [
                                self::ITEMS => [
                                    self::CLASS1 => [
                                        "type" => self::INPUT_TEXT,
                                        self::DEFAULT_VALUE => "checkbox"
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
            "params" => [
                self::ITEMS => [
                    self::VALUES => [
                        "key" => self::VALUES,
                        "name" => "Seçenekler",
                        self::DESCRIPTION => "Seçilebilecek unsurları barındıran anahtar-değer eşleri dizisi.",
                        "type" => "input_array",
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "[]"
                    ],
                    "page_model"=>[
                        "key" => "object",
                        "name" => "Nesne",
                        "description" => "Durumu belirlenecek sayfa modeli",
                        "type" => "input_text",
                        "custom_template" => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "&lt;var&gt;page&lt;/var&gt;"
                    ]
                ]
            ]
        ],
    ];

    public $options = [
        self::TITLE => "Yayın Durumu",
        self::MULTILINE => true,
        self::VALUE => null,
        self::DEFAULT1 => BaseModel::DRAFT,
        "iRadio" => true,
        "html" => [
            "tag" => "div",
            "void_element" => false,
            self::ATTRIBUTES => [
                self::CLASS1 => "checkbox"
            ]
        ],
        "components" => [
            "formgroup" => [
                "type" => "div",
                self::OPTIONS => [
                    "html" => [
                        self::ATTRIBUTES => [
                            self::CLASS1 => "form-group"
                        ]
                    ],
                    self::OPTIONS => [
                        "collectable_as" => ["form-group pagestatuscontrol-form-group"]
                    ],
                ]

            ],
            "caption" => [
                "type" => "bluetitle",
                self::OPTIONS => [
                    "collectable_as" => ["caption"]
                ]
            ],
        ],
    ];

    public $contents = [
        self::DATEFIELD => [
            "type" => "input",
            self::OPTIONS => [
                "html" => [
                    self::ATTRIBUTES => [
                        "type" => "hidden",
                        self::CLASS1 => self::DATEFIELD
                    ]
                ]
            ]
        ],
        self::DATEPICKER => [
            "type" => "div",
            self::OPTIONS => [
                "html" => [
                    self::ATTRIBUTES => [
                        self::CLASS1 => "datepicks",
                        "style" => "visibility:hidden"
                    ]
                ]
            ],
            "contents" => [
                [
                    "type" => "input",
                    self::OPTIONS => [
                        "html" => [
                            self::ATTRIBUTES => [
                                self::CLASS1 => self::DATEPICKER,
                                "placeholder" => "Tarih Seçiniz",
                                "style" => "opacity:1;",
                            ]
                        ]
                    ],

                ]
            ]
        ]

    ];

    public $params = [
        "page_model" => null
    ];

    public $collectable_as = ["radiogroup"];


    public function __construct(array $params = [], array $contents = [], array $options = [], array $data = [])
    {
        $this->class = get_class($this);
        $this->params = array_replace_recursive($this->params, array_replace_recursive($this->defaultParams(), $params));
        $this->params = $this->parseAnnotations($this->params, true);


        $this->options = array_replace_recursive(
            $this->baseOptions(),
            array_replace_recursive(
                $this->options,
                array_replace_recursive(
                    $this->defaultOptions(),
                    $options)
            )
        );
        $this->options = $this->parseAnnotations($this->options, true);

        $this->info = array_replace_recursive(self::DEFAULT_INFO, $this->info);

        extract($this->params);

        if (!isset($page_model) || !$page_model || !is_a($page_model, Page::class)) {
            data_set($this->options, "html.tag", null, true);
            $this->ignored_if = true;
            $this->errors[] = "page_model parametresi bir " . Page::class . " nesnesi olmalıdır.";
            return false;
        }

        $value = $this->options[self::VALUE] ?? null;
        $values = [
            BaseModel::DRAFT => trans("ContentPanel::page.status.DRAFT"),
            BaseModel::ACTIVE => trans("ContentPanel::page.status.ACTIVE"),
            BaseModel::PASSIVE => trans("ContentPanel::page.status.PASSIVE"),
        ];

        if (config('allbuilder.allow_postdate_content')) {
            $values[BaseModel::POSTDATE] = trans("ContentPanel::page.status.POSTDATE");
        }

        data_set($this->data, self::VALUES, $values, true);
        data_set($this->options, "name", "page->" . $page_model->id . "->status", false);
        data_set($this->options, self::VALUE, $page_model->status, true);
        data_set($this->options, self::TITLE, "Yayın Durumu", false);

        $name = \Str::slug($this->options[self::TITLE]);
        $this->options[self::TITLE] = getPanelLangPartKey($name, $this->options[self::TITLE], true);

        if (isset($this->contents[self::DATEFIELD])) {
            data_set($this->contents, "datefield.options.html.attributes.name", "page->" . $page_model->id . "->published_at");
            if ($value == BaseModel::POSTDATE) {
                data_set($this->contents, "datefield.options.html.attributes.value", $page_model->published_at);
            }
        }

        parent::__construct($this->params, $contents, $this->options, $data);

    }

    public function handleGotContentsHtmlElements(&$htmlelements)
    {

        extract($this->params);

        $thisid = $this->class_id;

        $elid = null;
        foreach ($htmlelements as $el) {
            if ($el->has_class(self::DATEFIELD)) {
                $elid = $el->get_id();
            }
        }

        if ($elid) {
            $display_input_id = null;
            foreach ($htmlelements as $el) {

                if ($el->has_class("datepicks")) {
                    foreach ($el->contents() as $sub_el) {
                        if ($sub_el->has_class(self::DATEPICKER)) {
                            $display_input_id = $sub_el->get_id();
                        }
                    }
                }
            }
            if ($display_input_id) {

                $postdate_constant = BaseModel::POSTDATE;
                $script = <<<"ABC"
<script>
$(document).ready(function(){
    $( "#$display_input_id" ).datepicker({
        language: 'tr',
        format:"dd/mm/yyyy",
        dateFormat: "dd/mm/yy",
        altFormat: "yy-mm-dd",
        altField:"#$elid",
        monthNames: [ "Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık" ],
        dayNamesMin: [ "Pa", "Pt", "Sl", "Çş", "Pe", "Cu", "Ct" ],
        firstDay:1
    });

    $(document).on("ifChecked", "input[type=radio]","#$thisid", function() {
        showPostDate($(this));
    });
    $("input[type=radio]","#$thisid").iCheck("update");

    showPostDate($('input[name$="status"]:checked'));

    function showPostDate(el) {
      if(el.val() == "$postdate_constant"){
            $(".datepicks",el.closest(".checkbox")).css("visibility","visible");
            $(".datepicker",el.closest(".checkbox")).prop("disabled",false);
        }else{
            $(".datepicks",el.closest(".checkbox")).css("visibility","hidden");
            $(".datepicker",el.closest(".checkbox")).prop("disabled",true);
        }
    }
});
</script>
ABC;

                $current_stack_data = data_get($this->data, "stacks.scripts", "");
                //$this->extendStack("scripts",$current_stack_data.$script);
                data_set($this->data, "stacks.scripts", $current_stack_data . $script, true);
                $this->mergeStacks();


            }
        }

    }

}
