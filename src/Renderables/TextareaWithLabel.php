<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\HtmlElement;

class TextareaWithLabel extends BuilderRenderable
{
    public const DESCRIPTION = "description";
    public const CAPTION = "caption";
    public const COMPONENTS = "components";
    public const SEO_ACTIVE = "seo-active";
    public const CLASS1 = "class";
    public const ATTRIBUTES = "attributes";
    public const TEXTAREA = "textarea";
    public const DEFAULT_VALUE = "default_value";
    public const TITLE = "title";
    public const OPTIONS = "options";
    public const ITEMS = "items";
    public const VALUE = "value";
    public $info = [
        "icon_key" => "edit",
        "object_key" => "TextareaWithLabel",
        "object_class" => __CLASS__,
        "object_tags" => [
            "basic_html", "form"
        ],
        "name" => "Etiketli Textarea",
        self::DESCRIPTION => "Standart Textarea ve Başlık seti",
        self::ITEMS => [
            self::OPTIONS => [
                self::ITEMS => [
                    self::TITLE => [
                        "key" => self::TITLE,
                        "name" => "Başlık",
                        self::DESCRIPTION => "Textarea başlığı",
                        "type" => "input_text",
                        "custom_template" => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ""
                    ],
                    self::VALUE => [
                        "key" => self::VALUE,
                        "name" => "Değer",
                        self::DESCRIPTION => "Textarea içeriği",
                        "type" => "input_textarea",
                        "custom_template" => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ""
                    ],
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "readonly_text",
                                self::DEFAULT_VALUE => self::TEXTAREA
                            ],
                            self::ATTRIBUTES => [
                                self::ITEMS => [
                                    self::CLASS1 => [
                                        "type" => "input_text",
                                        self::DEFAULT_VALUE => "form-control"
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
        ],
    ];

    public $options = [
        self::VALUE => null,
        self::TITLE => null,
        self::SEO_ACTIVE => true,
        "html" => [
            "tag" => self::TEXTAREA,
            "void_element" => false,
            self::ATTRIBUTES => [self::CLASS1 => "form-control"]
        ],
        self::COMPONENTS => [
            "formgroup" => [
                "type" => "div",
                self::OPTIONS => [
                    "html" => [
                        self::ATTRIBUTES => [
                            self::CLASS1 => "form-group"
                        ]
                    ],
                    self::OPTIONS => [
                        "collectable_as" => ["form-group ckeditor-form-group"]
                    ],
                ]

            ],
            self::CAPTION => [
                "type" => "bluetitle",
                self::OPTIONS => [
                    "collectable_as" => [self::CAPTION]
                ]
            ]
        ],

    ];


    public $collectable_as = [self::TEXTAREA, "formfield", "input"];


    public function __construct(array $params = [], array $contents = [], array $options = [], array $data = [])
    {
        parent::__construct($params, $contents, $options, $data);


        $use_old_input = !(isset($this->options["dont_use_old_input"]) && $this->options["dont_use_old_input"]);

        $name = data_get($this->options, "html.attributes.name",null);
        if($use_old_input && $name && array_key_exists($name, old())){
            $old = old($name);
            $val = is_null($old) ? "" : $old ;
        }else{
            $val = $this->options[self::VALUE] ?? "";
        }
        $this->contents[] = $val;
    }


    public function getHtmlElement()
    {
        if ($this->ignored_if) {
            return (new HtmlElement());
        }
        $el = parent::getHtmlElement();
        if($el->get_attr('name')) {
            $this->options[self::TITLE] = getPanelLangPartKey($el->get_attr('name'), $this->options[self::TITLE]);
        }

        $caption = $this->options[self::COMPONENTS][self::CAPTION] ?? null;
        $formgroup = $this->options[self::COMPONENTS]["formgroup"] ?? null;
        $title = $this->options[self::TITLE] ?? null;

        $el->data("field-title", $title);
        $el->data(self::SEO_ACTIVE, $this->options[self::SEO_ACTIVE]);

        if ($caption && $title) {
            data_set($caption, "contents.title", $title, true);
            if ($formgroup) {
                data_set($formgroup, "contents.caption", $caption, true);
            }
        }


        $el2return = $el;

        if ($formgroup) {
            data_set($formgroup, "contents.input", $el, true);
            $el2return = $this->buildRenderableFromArray($formgroup)->getHtmlElement();
        }

        return $el2return;
    }


}
