<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;

class BlueTitle extends BuilderRenderable
{
    public const ITEMS = "items";
    public $info = [
        "icon_key" => "hand-point-right",
        "object_key" => "BlueTitle",
        "object_class" => __CLASS__,
        "object_tags" => [
            "miscellaneous", "mediapress"
        ],
        "name" => "Mediapress Mavi Başlık",
        "description" => "Mediapress'te tanımlanmış mavi renkli bir başlık",
        self::ITEMS => [
            "options" => [
                self::ITEMS => [
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "input_text",
                                "default_value" => "div"
                            ],
                            "attributes" => [
                                self::ITEMS => [
                                    "class" => [
                                        "type" => "input_text",
                                        "default_value" => "name"
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
        ],
    ];
    
    public $options = [
        "html" => [
            "tag" => "div",
            "void_element" => false,
            "attributes" => [
                "class" => "name"
            ]
        ]
    ];
    
}