<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\HtmlElement;

class CKEditor extends BuilderRenderable
{

    public const DESCRIPTION = "description";
    public const ITEMS = "items";
    public const OPTIONS = "options";
    public const TITLE = "title";
    public const KEY = "key";
    public const NAME = "name";
    public const TYPE = "type";
    public const CUSTOM_TEMPLATE = "custom_template";
    public const DEFAULT_VALUE = "default_value";
    public const VALUE = "value";
    public const TEXTAREA = "textarea";
    public const ATTRIBUTES = "attributes";
    public const CLASS_STR = "class";
    public const SEO_ACTIVE = "seo-active";
    public const COMPONENTS = "components";
    public const CAPTION = "caption";
    public $info = [
        "icon_key" => "edit",
        "object_key" => "CKEditor",
        "object_class" => __CLASS__,
        "object_tags" => [
            "form"
        ],
        self::NAME => "CKEditor HTML Düzenleme Aracı",
        self::DESCRIPTION => "HTML Düzenleme Aracı (WYSIWYG)",
        self::ITEMS => [
            self::OPTIONS => [
                self::ITEMS => [
                    self::TITLE => [
                        self::KEY => self::TITLE,
                        self::NAME => "Başlık",
                        self::DESCRIPTION => "Editörün başlığı",
                        self::TYPE => "input_text",
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ""
                    ],
                    self::VALUE => [
                        self::KEY => self::VALUE,
                        self::NAME => "Değer",
                        self::DESCRIPTION => "Editörün içeriği",
                        self::TYPE => "input_textarea",
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ""
                    ],
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                self::TYPE => "readonly_text",
                                self::DEFAULT_VALUE => self::TEXTAREA
                            ],
                            self::ATTRIBUTES => [
                                self::ITEMS => [
                                    self::CLASS_STR => [
                                        self::TYPE => "input_text",
                                        self::DEFAULT_VALUE => "form-control ckeditor"
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
        ],
    ];

    public $options = [
        self::VALUE => null,
        self::TITLE => null,
        self::SEO_ACTIVE => true,
        "html" => [
            "tag" => self::TEXTAREA,
            "void_element" => false,
            self::ATTRIBUTES => [
                self::CLASS_STR => "form-control ckeditor",
                "data-backup-function" => "backupCKEditor"
            ]
        ],
        "forced_html_classes"=>[
            "form-control", "ckeditor"
        ],
        self::COMPONENTS => [
            "formgroup" => [
                self::TYPE => "div",
                self::OPTIONS => [
                    "html" => [
                        self::ATTRIBUTES => [
                            self::CLASS_STR => "form-group"
                        ]
                    ],
                    self::OPTIONS => [
                        "collectable_as" => ["form-group"]
                    ],
                ],
                "contents" => []

            ],
            self::CAPTION => [
                self::TYPE => "div",
                self::OPTIONS => [
                    "html" => [
                        self::ATTRIBUTES => [self::CLASS_STR => self::NAME]
                    ],
                    self::OPTIONS => [
                        "collectable_as" => [self::CAPTION]
                    ]
                ],
                "contents" => []
            ]
        ],

    ];


    public $collectable_as = [self::TEXTAREA, "formfield", "input"];

    public function __construct(array $params = [], array $contents = [], array $options = [], array $data = [])
    {
        parent::__construct($params, $contents, $options, $data);

        $name = data_get($this->options, "html.attributes.name",null);
        if($name && array_key_exists($name, old())){
            $old = old($name);
            $val = is_null($old) ? "" : $old ;
        }else{
            $val = $this->options[self::VALUE] ?? "";
        }
        $this->contents[] = $val;
    }

    public function getHtmlElement()
    {
        if ($this->ignored_if) {
            return (new HtmlElement());
        }

        $el = parent::getHtmlElement();
        if($el->get_attr('name')) {
            $this->options[self::TITLE] = getPanelLangPartKey($el->get_attr('name'), $this->options[self::TITLE]);
        }

        $caption = $this->options[self::COMPONENTS][self::CAPTION] ?? null;
        $formgroup = $this->options[self::COMPONENTS]["formgroup"] ?? null;
        $title = $this->options[self::TITLE] ?? null;

        $el->data("field-title", $title);
        $el->data(self::SEO_ACTIVE, $this->options[self::SEO_ACTIVE]);

        if ($caption && $title) {
            data_set($caption, "contents.title", $title, true);
            if ($formgroup) {
                data_set($formgroup, "contents.caption", $caption, true);
            }
        }


        $el2return = $el;

        if ($formgroup) {
            data_set($formgroup, "contents.input", $el, true);
            $el2return = $this->buildRenderableFromArray($formgroup)->getHtmlElement();
        }

        return $el2return;
    }


}
