<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;

class ForEacher extends BuilderRenderable
{
    
    const DESCRIPTION = "description";
    const ARRAY1 = "array";
    const ITEMNAME = "itemname";
    const CUSTOM_TEMPLATE = "custom_template";
    const KEYNAME = "keyname";
    const DEFAULT_VALUE = "default_value";
    const INPUT_TEXT = "input_text";
    const ITEMS = "items";
    public $info = [
        "icon_key" => "undo-alt",
        "object_key" => "ForEacher",
        "object_class" => __CLASS__,
        "object_tags" => [
            "mediapress", "flow_control"
        ],
        "name" => "ForEach Döngü Nesnesi",
        self::DESCRIPTION => "Her dönüş için içindeki içeriği çoklayan nesne",
        self::ITEMS => [
            "options" => [
                self::ITEMS => [
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => self::INPUT_TEXT,
                                self::DEFAULT_VALUE => "div"
                            ],
                            "attributes" => [
                                self::ITEMS => [
                                    "class" => [
                                        "type" => self::INPUT_TEXT,
                                        self::DEFAULT_VALUE => "tab-list"
                                    ],
                                ]
                            ]
                        ]
                    ],
                ]
            ],
            "params" => [
                self::ITEMS => [
                    self::KEYNAME => [
                        "key" => self::KEYNAME,
                        "name" => "Döngü anahtar değişkeni adı",
                        self::DESCRIPTION => "Koleksiyon için girilen foreach döngüsünde, koleksiyon elemanının anahtar değerini taşıyacak değişkenin adı",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "key"
                    ],
                    self::ITEMNAME => [
                        "key" => self::ITEMNAME,
                        "name" => "Döngü değer değişkeni adı",
                        self::DESCRIPTION => "Koleksiyon için girilen foreach döngüsünde, koleksiyon elemanının değerini taşıyacak değişkenin adı",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "detail"
                    ],
                    self::ARRAY1 => [
                        "key" => self::ARRAY1,
                        "name" => "Detail Koleksiyonu",
                        self::DESCRIPTION => "Detail koleksiyonu. Ebeveyn nesnelerin yönetimine bırakmak için devre dışı bırakın.",
                        "type" => "collection",
                        "collection_items" => [],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "[]"
                    ]
                ]
            ]
        ],
    
    
    ];
    
    public $data = [
        self::ARRAY1 => []
    ];
    
    
    public function __construct(array $params = [], array $contents = [], array $options = [], array $data = [])
    {
        parent::__construct($params, $contents, $options, $data);
        
        $contents = $this->contents;
        $new_contents = [];
        $array = $this->data[self::ARRAY1] ?? [];
        $keyvar = $this->data[self::KEYNAME] ?? 'key';
        $itemvar = $this->data[self::ITEMNAME] ?? 'item';
        $counter = 0;
        foreach ($array as ${$keyvar} => ${$itemvar}) {
            foreach ($contents as $__con__ten__t__key__ => $__con__ten__t__) {
                $new_contents[$__con__ten__t__key__ . $counter] = $__con__ten__t__;
            }
            $counter++;
        }
        
        $this->contents = $this->parseAnnotations($new_contents);
        
    }
}