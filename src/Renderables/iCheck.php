<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\HtmlElement;

class iCheck extends BuilderRenderable
{
    public const DESCRIPTION = "description";
    public const CHECKED_IF_VALUE_IS = "checked_if_value_is";
    public const LABEL = "label";
    public const CONTENTS = "contents";
    public const CHECKBOX_DIV = "checkbox-div";
    public const COMPONENTS = "components";
    public const VOID_ELEMENT = "void_element";
    public const INPUT = "input";
    public const CHECKBOX = "checkbox";
    public const CLASS1 = "class";
    public const ATTRIBUTES = "attributes";
    public const INPUT_TEXT = "input_text";
    public const TITLE = "title";
    public const CHECKED = "checked";
    public const DEFAULT_VALUE = "default_value";
    public const CUSTOM_TEMPLATE = "custom_template";
    public const AFTER = "after";
    public const BEFORE = "before";
    public const LABEL_PLACEMENT = "label_placement";
    public const OPTIONS = "options";
    public const ITEMS = "items";
    public $info = [
        "icon_key" => "check-circle",
        "object_key" => "iCheck",
        "object_class" => __CLASS__,
        "object_tags" => [
            "basic_html", "form", "mediapress"
        ],
        "name" => "Onay Kutusu (iCheck)",
        self::DESCRIPTION => "iCheck türü Onay Kutusu",
        self::ITEMS => [
            self::OPTIONS => [
                self::ITEMS => [
                    self::LABEL_PLACEMENT => [
                        "key" => self::LABEL_PLACEMENT,
                        "name" => "Etiket yerleşimi",
                        self::DESCRIPTION => "Etiketin, onay kutusunun öncesinde mi sonrasında mı gösterileceğini belirler",
                        "type" => "select",
                        "values" => [
                            self::AFTER => "Sonra",
                            self::BEFORE => "Önce",
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => self::AFTER
                    ],
                    self::CHECKED => [
                        "key" => self::CHECKED,
                        "name" => "İşaretli",
                        self::DESCRIPTION => "Onay kutusunun işaretli olup olmayacağını belirler (zorla)",
                        "type" => "radio",
                        "values" => [
                            ":null" => "Hiçbiri",
                            "0" => "Hayır",
                            "1" => "Evet",
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ":null"
                    ],
                    self::TITLE => [
                        "key" => self::TITLE,
                        "name" => "Başlık",
                        self::DESCRIPTION => "Nesnenin başlığı",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ""
                    ],
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "readonly_text",
                                "default_value" => "input"
                            ],
                            self::ATTRIBUTES => [
                                self::ITEMS => [
                                    self::CLASS1 => [
                                        "type" => self::INPUT_TEXT,
                                        self::DEFAULT_VALUE => "form-check-input"
                                    ],
                                    "type" => [
                                        "key" => "type",
                                        "name" => "HTML Input Type",
                                        self::DESCRIPTION => "",
                                        "type" => self::INPUT_TEXT,
                                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                                        self::DEFAULT_VALUE => self::CHECKBOX
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
        ],
    ];

    public $options = [
        self::TITLE => "Etiket",
        self::LABEL_PLACEMENT => self::AFTER, //after | before
        "html" => [
            "tag" => self::INPUT,
            self::VOID_ELEMENT => true,
            self::ATTRIBUTES => [
                "type" => self::CHECKBOX,
                self::CLASS1 => "form-check-input"
            ]
        ],
        self::CHECKED => null,
        //"checked_if_value_is" => 1,
        self::COMPONENTS => [
            self::CHECKBOX_DIV => [
                "type" => "div",
                self::OPTIONS => [
                    "html" => [
                        "tag" => "div",
                        self::VOID_ELEMENT => false,
                        self::ATTRIBUTES => [
                            self::CLASS1 => self::CHECKBOX
                        ]
                    ],
                    self::OPTIONS => [
                        "collectable_as" => ["checkbox-wrapper"]
                    ],
                ],
                self::CONTENTS => []

            ],
            self::LABEL => [
                "type" => "blank",
                self::OPTIONS => [
                    "html" => [
                        "tag" => self::LABEL,
                        self::VOID_ELEMENT => false,
                        self::ATTRIBUTES => [
                            self::CLASS1 => "form-check-label"
                        ]
                    ],
                    self::OPTIONS => [
                        "collectable_as" => [self::LABEL]
                    ]
                ],
                self::CONTENTS => []
            ]
        ]
    ];


    public $collectable_as = [self::CHECKBOX, self::INPUT, "formfield"];


    public function getHtmlElement()
    {

        if ($this->ignored_if) {
            return (new HtmlElement());
        }

        $name = \Str::slug($this->options[self::TITLE]);
        $this->options[self::TITLE] = getPanelLangPartKey($name, $this->options[self::TITLE], true);

        $this->options[self::COMPONENTS][self::LABEL][self::CONTENTS][] = $this->options[self::TITLE];

        $el = parent::getHtmlElement();
        $value = $el->get_attr("value");

        data_set($this->options[self::COMPONENTS][self::LABEL], "options.html.attributes.for", $el->get_id());
        data_set($this->options, self::LABEL_PLACEMENT, self::AFTER, false);
        $label = $this->buildRenderableFromArray($this->options[self::COMPONENTS][self::LABEL]);

        if (data_get($this->options, self::LABEL_PLACEMENT, self::AFTER) == self::BEFORE) {
            $this->options[self::COMPONENTS][self::CHECKBOX_DIV][self::CONTENTS][self::LABEL] = $label;
        }

        $this->options[self::COMPONENTS][self::CHECKBOX_DIV][self::CONTENTS][self::INPUT] = $el;

        if (data_get($this->options, self::LABEL_PLACEMENT, self::AFTER) == self::AFTER) {
            $this->options[self::COMPONENTS][self::CHECKBOX_DIV][self::CONTENTS][self::LABEL] = $label;
        }

        if (isset($this->options[self::CHECKED_IF_VALUE_IS]) && !is_null($this->options[self::CHECKED_IF_VALUE_IS])) {
            if ($this->options[self::CHECKED_IF_VALUE_IS] == $value) {
                $el->add_attr(self::CHECKED, self::CHECKED);
            }
        }

        if (isset($this->options[self::CHECKED])
            &&
            ($this->options[self::CHECKED] === true ||
                (is_array($this->options[self::CHECKED]) && $this->processConditionsArray($this->options[self::CHECKED]))
            )
        ) {
            $el->add_attr(self::CHECKED, self::CHECKED);
        }

        $cbdiv = $this->buildRenderableFromArray($this->options[self::COMPONENTS][self::CHECKBOX_DIV]);

        return $cbdiv->getHtmlElement();
    }


}
