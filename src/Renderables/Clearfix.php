<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;

class Clearfix extends BuilderRenderable
{
    public const ITEMS = "items";
    public const CLEARFIX = "clearfix";
    public $info = [
        "icon_key" => "hand-paper",
        "object_key" => "Clearfix",
        "object_class" => __CLASS__,
        "object_tags" => [
            "miscellaneous"
        ],
        "name" => "HTML Clearfix",
        "description" => "Yüzen elemanlar için yüksekliksiz kapsayıcı sorununa çözüm üreten eleman",
        "options" => [
            self::ITEMS => [
                "html" => [
                    self::ITEMS => [
                        "tag" => [
                            "type" => "input_text",
                            "default_value" => "div"
                        ],
                        "attributes" => [
                            self::ITEMS => [
                                "class" => [
                                    "type" => "readonly_text",
                                    "default_value" => self::CLEARFIX
                                ]
                            ]
                        ]
                    ]
                
                ],
            
            
            ]
        ],
    
    
    ];
    public $options = [
        "html" => [
            "tag" => "div",
            "attributes" => [
                "class" => self::CLEARFIX
            ],
            "void_element" => false,
        ],
        "forced_html_classes"=>["clearfix"]
    ];
    
    public $collectable_as = ["div", self::CLEARFIX];
    
}