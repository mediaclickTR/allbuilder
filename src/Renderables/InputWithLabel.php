<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\HtmlElement;

class InputWithLabel extends BuilderRenderable
{
    public const DESCRIPTION = "description";
    public const LABEL = "label";
    public const CONTENTS = "contents";
    public const WRAPPER = "wrapper";
    public const COMPONENTS = "components";
    public const SEO_ACTIVE = "seo-active";
    public const ATTRIBUTES = "attributes";
    public const INPUT = "input";
    public const DEFAULT_VALUE = "default_value";
    public const TITLE = "title";
    public const ITEMS = "items";
    public $info = [
        "icon_key" => "edit",
        "object_key" => "InputWithLabel",
        "object_class" => __CLASS__,
        "object_tags" => [

        ],
        "name" => "Başlıklı Metin Kutusu",
        self::DESCRIPTION => "Başlıklı metin kutusu seti",
        self::ITEMS => [
            "options" => [
                self::ITEMS => [
                    self::TITLE => [
                        "key" => self::TITLE,
                        "name" => "Başlık",
                        self::DESCRIPTION => "Nesnenin başlığı",
                        "type" => "input_text",
                        "custom_template" => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ""
                    ],
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "readonly_text",
                                self::DEFAULT_VALUE => self::INPUT
                            ],
                            self::ATTRIBUTES => [
                                self::ITEMS => [
                                    "type" => [
                                        "key" => "type",
                                        "name" => "HTML Input Type",
                                        self::DESCRIPTION => "",
                                        "type" => "select",
                                        "values" => [
                                            "text" => "Metin (text)",
                                            "hidden" => "Gizli (hidden)",
                                            "password" => "Parola (password)",
                                            "number" => "Sayı (number)",
                                            "tel" => "Telefon (tel)",
                                            "email" => "E-Posta (email)",
                                            "button" => "Düğme (button)",
                                            "checkbox" => "Onay Kutusu (checkbox)",
                                            "radio" => "Seçenek (radio)",
                                            "range" => "Aralık (range)",
                                            "file" => "Dosya (file)",
                                            "date" => "Tarih (date)",
                                            "datetime-local" => "Yerel Tarih/Saat (datetime)",
                                            "time" => "Saat (time)",
                                            "month" => "Ay (month)",
                                            "week" => "Hafta (week)",
                                            "search" => "Arama (search)",
                                            "reset" => "Sıfırla (reset)",
                                            "submit" => "Gönder (submit)",
                                            "color" => "Renk (color)",
                                            "image" => "Görsel (image)",
                                            "url" => "URL (url)",
                                        ],
                                        "custom_template" => "", // html - used if input_type is custom,
                                        self::DEFAULT_VALUE => "text"
                                    ],
                                    "class" => [
                                        "type" => "input_text",
                                        self::DEFAULT_VALUE => ""
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
        ],
    ];

    public $options = [
        self::TITLE => "Etiket",
        self::SEO_ACTIVE => true,
        "html" => [
            "tag" => self::INPUT,
            "void_element" => true,
            self::ATTRIBUTES => [
                "type" => "text"
            ]
        ],
        self::COMPONENTS => [
            self::WRAPPER => [
                "type" => "formgroup",
                self::CONTENTS => []

            ],
            self::LABEL => [
                "type" => "blank",
                "options" => [
                    "html" => [
                        "tag" => self::LABEL,
                        "void_element" => false,
                        self::ATTRIBUTES => ["class" => ""]
                    ],
                    "collectable_as" => [self::LABEL]
                ],
                self::CONTENTS => []
            ]
        ]

    ];


    public $collectable_as = ["textinput", self::INPUT, "formfield", "fakeable"];


    public function getHtmlElement()
    {
        if ($this->ignored_if) {
            return (new HtmlElement());
        }

        $el = parent::getHtmlElement();
        if($el->get_attr('name')) {
            $this->options[self::TITLE] = getPanelLangPartKey($el->get_attr('name'), $this->options[self::TITLE]);
        }

        $this->options[self::COMPONENTS][self::LABEL][self::CONTENTS][] = $this->options[self::TITLE];

        $use_old_input = !(isset($this->options["dont_use_old_input"]) && $this->options["dont_use_old_input"]);


        if($use_old_input){
            $input_name = $el->get_attr("name");
            if($input_name && array_key_exists($input_name,old())){
                $el->set_attr("value", old($input_name));
            }
        }

        $el->data("field-title", $this->options[self::TITLE]);
        $el->data(self::SEO_ACTIVE, $this->options[self::SEO_ACTIVE]);
        data_set($this->options[self::COMPONENTS][self::LABEL], "options.html.attributes.for", $el->get_id());
        $label = $this->buildRenderableFromArray($this->options[self::COMPONENTS][self::LABEL]);
        $this->options[self::COMPONENTS][self::WRAPPER][self::CONTENTS][self::LABEL] = $label;
        $this->options[self::COMPONENTS][self::WRAPPER][self::CONTENTS][self::INPUT] = $el;
        $wrapper = $this->buildRenderableFromArray($this->options[self::COMPONENTS][self::WRAPPER]);

        return $wrapper->getHtmlElement();
    }


}
