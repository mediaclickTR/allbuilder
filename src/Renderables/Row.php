<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;

class Row extends BuilderRenderable
{
    const ITEMS = "items";
    public $info = [
        "icon_key" => "vector-square",
        "object_key" => "Row",
        "object_class" => __CLASS__,
        "object_tags" => [
            "basic_html",
            "bootstrap"
        ],
        "name" => "Bootstrap Row",
        "description" => "Row sınıfıyla Standart DIV etiketi",
        self::ITEMS => [
            "options" => [
                self::ITEMS => [
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "readonly_text",
                                "default_value" => "div"
                            ],
                            "attributes" => [
                                self::ITEMS => [
                                    "class" => [
                                        "type" => "input_text",
                                        "default_value" => "row"
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
        ],
    ];
    
    public $options = [
        "html" => [
            "tag" => "div",
            "void_element" => false,
            "attributes"=>[
                "class"=>"row"
            ]
        ],
        "forced_html_classes"=>["row"]
    ];
    
}
