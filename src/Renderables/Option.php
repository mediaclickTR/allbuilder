<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\HtmlElement;

class Option extends BuilderRenderable
{
    public const VALUE = "value";
    public const OPTION = "option";
    public const INPUT_TEXT = "input_text";
    public const SELECTED_IF_VALUE_IS = "selected_if_value_is";
    public const DEFAULT_VALUE = "default_value";
    public const CUSTOM_TEMPLATE = "custom_template";
    public const SELECTED = "selected";
    public const ITEMS = "items";
    public const DESCRIPTION = "description";
    public $info = [
        "icon_key" => "dot-circle",
        "object_key" => "Option",
        "object_class" => __CLASS__,
        "object_tags" => [
            "basic_html", "form"
        ],
        "name" => "HTML Option",
        self::DESCRIPTION => "Standart Option etiketi",
        self::ITEMS => [
            "options" => [
                self::ITEMS => [
                    self::SELECTED => [
                        "key" => self::SELECTED,
                        "name" => "Seçili",
                        self::DESCRIPTION => "Seçeneğin seçili olup olmayacağını belirler (zorla)",
                        "type" => "radio",
                        "values" => [
                            "0" => "Hayır",
                            "1" => "Evet",
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ":null"
                    ],
                    self::SELECTED_IF_VALUE_IS => [
                        "key" => self::SELECTED_IF_VALUE_IS,
                        "name" => "Değer şuysa seçili",
                        self::DESCRIPTION => "Değerin koşula uyması durumunda nesne seçili gelecektir.",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ":null"
                    ],
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "readonly_text",
                                self::DEFAULT_VALUE => self::OPTION
                            ],
                            "attributes" => [
                                self::ITEMS => [
                                    "class" => [
                                        "type" => self::INPUT_TEXT,
                                        self::DEFAULT_VALUE => ""
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
            "params" => [
                self::ITEMS => [
                    self::VALUE => [
                        "key" => self::VALUE,
                        "name" => "Nesne değeri",
                        self::DESCRIPTION => "Nesne seçili olduğunda, bu değer sunucuya gönderilir",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ""
                    ],
                    "text" => [
                        "key" => "text",
                        "name" => "Nesnenin metni",
                        self::DESCRIPTION => "",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ""
                    ]
                ]
            ]
        ],
    ];
    public $options = [
        "html" => [
            "tag" => self::OPTION,
            "void_element" => false,
        ],
        self::SELECTED => null,
        self::SELECTED_IF_VALUE_IS => null,
    ];
    
    public $data = [
        self::VALUE => null,
        "text" => null,
    ];
    public $collectable_as = [self::OPTION];
    
    public function getHtmlElement()
    {
        
        if ($this->ignored_if) {
            return (new HtmlElement());
        }
        $el = parent::getHtmlElement();
        $value = $el->get_attr(self::VALUE);
        
        
        if (!is_null($this->data[self::VALUE])) {
            $el->add_attr(self::VALUE, $this->data[self::VALUE]);
        }
        if (!is_null($this->data["text"])) {
            $el->clear_content()->add_content($this->data["text"]);
        }
        
        if (!is_null($this->options[self::SELECTED_IF_VALUE_IS]) && $this->options[self::SELECTED_IF_VALUE_IS] == $value) {
                $el->add_attr(self::SELECTED, self::SELECTED);
        }
        
        if ($this->options[self::SELECTED]) {
            $el->add_attr(self::SELECTED, self::SELECTED);
        }
        
        return $el;
        
    }
    
}