<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\HtmlElement;

class iRadiosAmigos extends BuilderRenderable
{

    public const CHECKED = "checked";
    public const CAPTION = "caption";
    public const COMPONENTS = "components";
    public const VALUES = "values";
    public const CLASS1 = "class";
    public const ATTRIBUTES = "attributes";
    public const VALUE = "value";
    public const DEFAULT_VALUE = "default_value";
    public const CUSTOM_TEMPLATE = "custom_template";
    public const INPUT_TEXT = "input_text";
    public const TITLE = "title";
    public const OPTIONS = "options";
    public const ITEMS = "items";
    public const DESCRIPTION = "description";
    public $info = [
        "icon_key" => "bullseye",
        "object_key" => "iRadiosAmigos",
        "object_class" => __CLASS__,
        "object_tags" => [

        ],
        "name" => "Seçenekler (iRadio)",
        self::DESCRIPTION => "iRadio türü seçenek grubu",
        self::ITEMS => [
            self::OPTIONS => [
                self::ITEMS => [
                    self::TITLE => [
                        "key" => self::TITLE,
                        "name" => "Başlık",
                        self::DESCRIPTION => "Nesnenin başlığı",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ""
                    ],
                    self::VALUE => [
                        "key" => self::VALUE,
                        "name" => "Seçili değer",
                        self::DESCRIPTION => "",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ""
                    ],
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "readonly_text",
                                self::DEFAULT_VALUE => "div"
                            ],
                            self::ATTRIBUTES => [
                                self::ITEMS => [
                                    self::CLASS1 => [
                                        "type" => self::INPUT_TEXT,
                                        self::DEFAULT_VALUE => "checkbox"
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
            "params" => [
                self::ITEMS => [
                    self::VALUES => [
                        "key" => self::VALUES,
                        "name" => "Seçenekler",
                        self::DESCRIPTION => "Seçilebilecek unsurları barındıran anahtar-değer eşleri dizisi.",
                        "type" => "input_array",
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "[]"
                    ]
                ]
            ]
        ],
    ];

    public $options = [
        self::TITLE => null,
        self::VALUE => null,
        "html" => [
            "tag" => "div",
            "void_element" => false,
            self::ATTRIBUTES => [
                self::CLASS1 => "checkbox",
                "style" => ""
            ]
        ],
        self::COMPONENTS => [
            "formgroup" => [
                "type" => "div",
                self::OPTIONS => [
                    "html" => [
                        self::ATTRIBUTES => [
                            self::CLASS1 => "form-group"
                        ]
                    ],
                    self::OPTIONS => [
                        "collectable_as" => ["form-group pagestatuscontrol-form-group"]
                    ],
                ]

            ],
            self::CAPTION => [
                "type" => "bluetitle",
                self::OPTIONS => [
                    "collectable_as" => [self::CAPTION]
                ]
            ]
        ],
    ];

    public $collectable_as = ["radiogroup", "fakeable"];


    public function __construct(array $params = [], array $contents = [], array $options = [], array $data = [])
    {
        parent::__construct($params, $contents, $options, $data);

        $values = $this->data[self::VALUES] ?? null;

        $name = $this->options["name"] ?? null;

        $radio_cls_path = $this->options["renderable_types"]["radio"];

        $checked_val = $this->options[self::VALUE] ?? null;

        data_set($this->options, "default", rand(), false);

        $default = $this->options["default"];

        $checked_found = false;
        $additional_content = [];

        $use_old_input = !(isset($this->options["dont_use_old_input"]) && $this->options["dont_use_old_input"]);

        if ($values) {
            foreach ($values as $k => $v) {
                if($use_old_input && array_key_exists($name,old()) && old($name)==$k){
                    $checked=true;
                }else{
                    $checked = ($k . "") === ($checked_val . "");
                }
                if ($checked) {
                    $checked_found = true;
                }
                if ($radio_cls_path) {
                    $radio = new $radio_cls_path([], [], [self::TITLE => $v, "html" => [self::ATTRIBUTES => [self::VALUE => $k, "name" => $name]], self::CHECKED => $checked, self::COMPONENTS => ["wrapper" => null]], []);
                } else {
                    $radio = (new HtmlElement("option", false))->add_attr(self::VALUE, $k)->add_content($v);
                    if ($checked) {
                        $radio->add_attr("selected", "selected");
                    }
                }
                $additional_content[] = $radio;
            }

        }

        $additional_content_strategy = $this->options["additional_content"] ?? "";

        switch ($additional_content_strategy) {
            case "replace":
                $this->contents = $additional_content;
                break;
            case "merge":
            default:
                $this->contents = array_merge($this->contents, $additional_content);
                break;
        }


        if (count($this->contents)) {
            if (!$checked_found) {
                foreach ($this->contents as &$content) {
                    if (is_a($content, BuilderRenderable::BUILDER_RENDERABLE_CLASS_PATH)) {
                        /** @var BuilderRenderable $addcontent */
                        $value = $content->getOption("html.attributes.value", null);
                        if ($value == $checked_val) {
                            $content->setOption(self::CHECKED, true, true);
                            $content->setOption("html.attributes.checked", self::CHECKED, true);
                            $checked_found = true;
                        }
                    } elseif (is_a($content, "Mediapress\\Foundation\\HtmlElement")) {
                        /** @var HtmlElement $addcontent */
                        $value = $content->get_attr(self::VALUE);
                        if ($value == $checked_val) {
                            $content->add_attr(self::CHECKED, self::CHECKED);
                            $checked_found = true;
                        }
                    }
                }

            }


            if (!$checked_found && !is_null($default)) {
                foreach ($this->contents as &$content) {
                    if (is_a($content, BuilderRenderable::BUILDER_RENDERABLE_CLASS_PATH)) {
                        /** @var BuilderRenderable $addcontent */
                        $value = $content->getOption("html.attributes.value", null);
                        if ($value == $default) {
                            $content->setOption(self::CHECKED, true, true);
                            $content->setOption("html.attributes.checked", self::CHECKED, true);
                            $checked_found = true;
                        }
                    } elseif (is_a($content, "Mediapress\\Foundation\\HtmlElement")) {
                        /** @var HtmlElement $addcontent */
                        $value = $content->get_attr(self::VALUE);
                        if ($value == $default) {
                            $content->add_attr(self::CHECKED, self::CHECKED);
                            $checked_found = true;
                        }
                    }
                }

            }

        }

        /*dump($additional_content);
        dump($this->contents);*/


    }


    public function getHtmlElement()
    {
        if ($this->ignored_if) {
            return (new HtmlElement());
        }
        $el = parent::getHtmlElement();

        $caption = $this->options[self::COMPONENTS][self::CAPTION] ?? null;
        $formgroup = $this->options[self::COMPONENTS]["formgroup"] ?? null;
        $title = $this->options[self::TITLE] ?? null;

        if ($caption && $title) {
            data_set($caption, "contents.title", $title, true);
            if ($formgroup) {
                data_set($formgroup, "contents.caption", $caption, true);
            }
        }


        $el2return = $el;

        if ($formgroup) {
            data_set($formgroup, "contents.input", $el, true);
            $el2return = $this->buildRenderableFromArray($formgroup)->getHtmlElement();
        }

        return $el2return;
    }


}
