<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;

class Span extends BuilderRenderable
{
    public const ITEMS = "items";
    public $info = [
        "icon_key" => "terminal",
        "object_key" => "Span",
        "object_class" => __CLASS__,
        "object_tags" => [
            "basic_html"
        ],
        "name" => "HTML Span",
        "description" => "Standart Span etiketi",
        self::ITEMS => [
            "options" => [
                self::ITEMS => [
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "readonly_text",
                                "default_value" => "span"
                            ],
                            "attributes" => [
                                self::ITEMS => [
                                    "class" => [
                                        "type" => "input_text",
                                        "default_value" => ""
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
        ],
    ];
    
    public $options = [
        "html" => [
            "tag" => "span"
        ]
    ];
    
}