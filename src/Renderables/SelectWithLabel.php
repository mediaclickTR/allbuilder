<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Renderables\Select as SelectRenderable;
use Mediapress\Foundation\HtmlElement;

class SelectWithLabel extends SelectRenderable
{

    public const LABEL = "label";
    public const CONTENTS = "contents";
    public const FORMGROUP = "formgroup";
    public const TITLE = "title";
    public const INPUT_TEXT = "input_text";
    public const DEFAULT_TEXT = "default_text";
    public const VALUES = "values";
    public const VALUE = "value";
    public const CUSTOM_TEMPLATE = "custom_template";
    public const DEFAULT_VALUE = "default_value";
    public const OPTIONS = "options";
    public const SELECT = "select";
    public const ITEMS = "items";
    public const RADIO = "radio";
    public const MULTIPLE = "multiple";
    public const COMPONENTS = "components";
    public const DESCRIPTION = "description";
    public $info = [
        "icon_key" => "caret-square-down",
        "object_key" => "SelectWithLabel",
        "object_class" => __CLASS__,
        "object_tags" => [
            "basic_html", "form"
        ],
        "name" => "HTML Select, Başlıklı",
        self::DESCRIPTION => "Standart HTML Select Etiketi, Başlıklı",
        self::ITEMS => [
            self::OPTIONS => [
                self::ITEMS => [
                    self::TITLE => [
                        "key" => self::TITLE,
                        "name" => "Başlık",
                        self::DESCRIPTION => "Nesnenin başlığı",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ""
                    ],
                    self::MULTIPLE => [
                        "key" => self::MULTIPLE,
                        "name" => "Çoklu Seçim",
                        self::DESCRIPTION => "Birden fazla değer seçmeye izin verilip verilmeyeceğini belirler",
                        "type" => self::RADIO,
                        self::VALUES => [
                            "1" => "Evet",
                            "0" => "Hayır",
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "0"
                    ],
                    self::VALUE => [
                        "key" => self::VALUE,
                        "name" => "Değer",
                        self::DESCRIPTION => "Nesneye atanacak değer",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ""
                    ],
                    "show_default_option" => [
                        "key" => "show_default_option",
                        "name" => "Varsayılan Seçimi Göster",
                        self::DESCRIPTION => "Kullanıcı seçim yapmadan önce seçili görünecek bir değer olup olmayacağını belirler",
                        "type" => self::RADIO,
                        self::VALUES => [
                            "1" => "Evet",
                            "0" => "Hayır",
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ":null"
                    ],
                    "additional_content" => [
                        "key" => "additional_content",
                        "name" => "Data ile üretilen içerik",
                        self::DESCRIPTION => "Data parametrelerinden üretilen içeriği, hali hazırda bulunan içerikle değiştirilmesini ya da ona eklenmesini belirler",
                        "type" => self::RADIO,
                        self::VALUES => [
                            "replace" => "Yer değiştir",
                            "merge" => "Birleştir",
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "merge"
                    ],
                    self::DEFAULT_VALUE => [
                        "key" => self::DEFAULT_VALUE,
                        "name" => "Varsayılan eleman değeri",
                        self::DESCRIPTION => "",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ""
                    ],
                    self::DEFAULT_TEXT => [
                        "key" => self::DEFAULT_TEXT,
                        "name" => "Varsayılan eleman metni",
                        self::DESCRIPTION => "",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "---Seçiniz---"
                    ],
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "readonly_text",
                                self::DEFAULT_VALUE => self::SELECT
                            ],
                            "attributes" => [
                                self::ITEMS => [
                                    "class" => [
                                        "type" => self::INPUT_TEXT,
                                        self::DEFAULT_VALUE => ""
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
            "data" => [
                self::ITEMS => [
                    self::VALUES => [
                        "key" => self::VALUES,
                        "name" => "Seçenekler",
                        self::DESCRIPTION => "Seçilebilecek unsurları barındıran anahtar-değer eşleri dizisi.",
                        "type" => "input_array",
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "[]"
                    ]
                ]
            ]
        ],
    ];

    public $options = [
        "value" => null,
        "html" => [
            "tag" => self::SELECT,
            "void_element" => false,
        ],
        self::TITLE => null,
        self::MULTIPLE => true,
        "show_default_option" => true,
        self::DEFAULT_TEXT => "---- Seçim Yok ----",
        self::DEFAULT_VALUE => "",
        self::COMPONENTS => [
            self::FORMGROUP => [
                "type" => "div",
                self::OPTIONS => [
                    "html" => [
                        "attributes" => [
                            "class" => "form-group focus"
                        ]
                    ],
                    "collectable_as" => ["form-group"]
                ],
                self::CONTENTS => []

            ],
            self::LABEL => [
                "type" => self::LABEL,
                self::OPTIONS => [
                    "collectable_as" => [self::LABEL],
                    "html"=>[
                        "attributes"=>[
                            "class"=>"position-relative"
                        ]
                    ]
                ],
                self::CONTENTS => []
            ]
        ]
    ];

    public $collectable_as = [self::SELECT, "input", "formfield"];

    public function __construct(array $params = [], array $contents = [], array $options = [], array $data = [])
    {
        parent::__construct($params, $contents, $options, $data);

        extract($this->params);


        $default_option = null;
        if ($this->options["show_default_option"]) {
            if ($this->options[self::MULTIPLE]) {
                $this->warnings[] = "Select(" . $this->class_id . ") nesne ayarı aynı anda hem multiple=true hem show_default_option=true olamaz. show_default_option=false kabul edildi.";
            } else {
                /*$default_option = (new HtmlElement("option", false))
                    ->add_attr("value", $this->options[self::DEFAULT_VALUE])
                    ->data("display", "Seçin")
                    ->add_content($this->options[self::DEFAULT_TEXT]);
                array_unshift($this->contents, $default_option);*/
            }
        }

    }


    public function getHtmlElement()
    {
        if ($this->ignored_if) {
            return (new HtmlElement());
        }
        data_set($this->options, self::TITLE, "Seçin: ", false);
        $el = parent::getHtmlElement();
        if($el->get_attr('name')) {
            $this->options[self::TITLE] = getPanelLangPartKey($el->get_attr('name'), $this->options[self::TITLE]);
        }

        data_set($this->options, self::TITLE, "Seçim ", false);

        $formgroup = $this->options[self::COMPONENTS][self::FORMGROUP] ?? null;
        $label = $this->options[self::COMPONENTS][self::LABEL] ?? null;

        if ($formgroup && $label) {
            $this->options[self::COMPONENTS][self::LABEL][self::CONTENTS][] = $this->options[self::TITLE];
            $label = $this->buildRenderableFromArray($this->options[self::COMPONENTS][self::LABEL]);
        }

        if ($formgroup) {
            if ($label) {
                $this->options[self::COMPONENTS][self::FORMGROUP][self::CONTENTS][self::LABEL] = $label;
            }
            $this->options[self::COMPONENTS][self::FORMGROUP][self::CONTENTS]["input"] = $el;
            $formgroup = $this->buildRenderableFromArray($this->options[self::COMPONENTS][self::FORMGROUP]);
        }

        return $formgroup ? $formgroup->getHtmlElement() : $el;
    }


}
