<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;

class DetailSlugControlV2 extends BuilderRenderable
{

    public const ITEMS = "items";
    public $collectable_as = ["slug-control"];
    public $info = [
        "icon_key" => "terminal",
        "object_key" => "DetailSlugControlV2",
        "object_class" => __CLASS__,
        "object_tags" => [
            "mediapress"
        ],
        "name" => "Slug Kontrolü v2",
        "description" => "Bir Detail nesnesi için Slug düzenleme aracı",
        self::ITEMS => [
            "options" => [
                self::ITEMS => [
                    "initial_mode"=>[
                        "key" => "initial_mode",
                        "name" => "Başlangıçtaki Mod",
                        "description" => "Sayfa yüklendiğinde nesnenin hangi modda olacağını seçin",
                        "type" => "radio",
                        "values" => [
                            "manual" => "Bekleme Modu",
                            "auto" => "Düzenleme Modu",
                            "status_based" => 'İçerik Durumuna Göre (Önerilen) <i class="fa fa-info-circle text-info" title="(İçerik Oluştururken: düzenleme | İçerik Düzenlerken: bekleme)"></i>',
                        ],
                        "custom_template" => "", // html - used if input_type is custom,
                        "default_value" => "status_based"
                    ],
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "key"=>"tag",
                                "type" => "readonly_text",
                                "default_value" => "div"
                            ],
                            "attributes" => [
                                self::ITEMS => [
                                    "class" => [
                                        "type" => "input_text",
                                        "default_value" => ""
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
            "params"=>[
                self::ITEMS => [
                    "detail" => [
                        "key" => "detail",
                        "name" => "Detail Nesnesi",
                        "description" => "Slug kontrolünün bağlanacağı *Detail objesi",
                        "type"=>"input_text",
                        /*"type" => "object_selector",
                        "object_selector_parameters" => [
                            "selector_type" => "",
                            "selectable_objects" => [
                                "Sayfa Yapısı Detayı" => "Mediapress\\Modules\\Content\\Models\\SitemapDetail",
                                "Sayfa Detayı" => "Mediapress\\Modules\\Content\\Models\\PageDetail",
                                "Kategori Detayı" => "Mediapress\\Modules\\Content\\Models\\CategoryDetail",
                            ]
                        ],*/
                        "custom_template" => "", // html - used if input_type is custom,
                        "default_value" => "<var>detail</var>"
                    ],
                ]
            ]
        ],
    ];

    public $options = [
        "initial_mode" => "status_based",
        "html" => [
            "tag" => "div",
            "attributes"=>[
                "class"=>"mb-4"
            ]
        ],
        "rules"=>"required",
        "forced_html_classes" => [
            "slug-control-v2",
        ],
        "components" => [
            "style"=>[
                "type"=>"blank",
                "options"=>[
                    "html"=>[
                        "tag"=>"style"
                    ]
                ],
                "contents"=>[

                        ".slug-control-v2 .input-group-append .btn-cancel, .slug-control-v2 .input-group-append .btn-edit, .slug-control-v2 .input-group-append .loading {
  border-top-right-radius: 0.25rem !important;
  border-bottom-right-radius: 0.25rem !important; }"
                    ,
                ]
            ],
            "input_group" => [
                "type" => "div",
                "options" => [
                    "html" => [
                        "attributes" => [
                            "class" => "input-group"
                        ]
                    ]
                ],
                "contents" => [
                    "input_group_prepend" => [
                        "type" => "div",
                        "options" => [
                            "html" => [
                                "attributes" => [
                                    "class" => "input-group-prepend"
                                ]
                            ]
                        ],
                        "contents" => [
                            "input_group_text" => [
                                "type" => "span",
                                "options" => [
                                    "html" => [
                                        "attributes" => [
                                            "class" => "input-group-text "//bg-primary text-white-50
                                        ]
                                    ]
                                ],
                               "contents" => [
                                    "root_url"=>"getWebsiteRootUrl() :)"
                                ]
                            ]
                        ]
                    ],
                    "slug_input" => [
                        "type" => "input",
                        "options" => [
                            "html" => [
                                "attributes" => [
                                    "type" => "text",
                                    "class"=>"form-control  slug-input",//bg-primary text-white
                                    "value"=>"<current_slug>"
                                ]
                            ],
                            "forced_html_classes"=>[
                                "slug-input"
                            ],
                            "collectible_as"=>["formfield"],
                            "rules"=>"required"
                        ]
                    ],
                    "input_group_append"=>[
                        "type"=>"div",
                        "options" => [
                            "html"=>[
                                "attributes"=>[
                                    "class"=>"input-group-append"
                                ]
                            ],
                            "forced_html_classes"=>[
                                "input-group-append"
                            ]
                        ],
                        "contents"=>[
                            "button_confirm"=>[
                                "type"=>"button",
                                "options"=>[
                                    "html"=>[
                                        "attributes"=>[
                                            "class"=>"btn btn-success btn-confirm d-none",
                                            "type"=>"button"
                                        ]
                                    ],
                                    "forced_html_classes"=>[
                                        "btn-confirm"
                                    ]
                                ],
                                "contents"=>[
                                    [
                                        "type"=>"faicon",
                                        "options"=>[
                                            "iconname"=>"check"
                                        ]
                                    ]
                                ]
                            ],
                            "button_cancel"=>[
                                "type"=>"button",
                                "options"=>[
                                    "html"=>[
                                        "attributes"=>[
                                            "class"=>"btn btn-danger btn-cancel d-none",
                                            "type"=>"button"
                                        ]
                                    ],
                                    "forced_html_classes"=>[
                                        "btn-cancel"
                                    ]
                                ],
                                "contents"=>[
                                    [
                                        "type"=>"faicon",
                                        "options"=>[
                                            "iconname"=>"times"
                                        ]
                                    ]
                                ]
                            ],
                            "button_edit"=>[
                                "type"=>"button",
                                "options"=>[
                                    "html"=>[
                                        "attributes"=>[
                                            "class"=>"btn btn-success btn-edit",
                                            "type"=>"button"
                                        ]
                                    ],
                                    "forced_html_classes"=>[
                                        "btn-edit"
                                    ]
                                ],
                                "contents"=>[
                                    [
                                        "type"=>"faicon",
                                        "options"=>[
                                            "iconname"=>"pen"
                                        ]
                                    ]
                                ]
                            ],
                            "loading"=>[
                                "type"=>"span",
                                "options"=>[
                                    "html"=>[
                                        "attributes"=>[
                                            "class"=>"input-group-text loading"
                                        ]
                                    ]
                                ],
                                "contents"=>[
                                    [
                                        "type"=>"faicon",
                                        "options"=>[
                                            "iconname"=>"spinner",
                                            "html"=>[
                                                "attributes"=>[
                                                    "class"=>"fa-spin"
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ];

    public function __construct(array $params = [], array $contents = [], array $options = [], array $data = [])
    {

        $this->options = array_replace_recursive(
            $this->baseOptions(),
            array_replace_recursive(
                $this->options,
                array_replace_recursive(
                    $this->defaultOptions(),
                    $options)
            )
        );
        if(isset($this->options["rules"])){
            if(isset($this->options["components"]["input_group"]["contents"]["slug_input"])){
                $this->options["components"]["input_group"]["contents"]["slug_input"]["options"]["rules"] = $this->options["rules"];
            }
        }
        $this->contents = $this->options["components"];
        unset($this->options["components"]);
        //data_set($this->contents,'input_group.contents.input_group_prepend.contents.input_group_text.contents.root_url', "Constructorde atanan",true);
        //data_set($this->contents,'input_group.contents.input_group_prepend.contents.input_group_text.contents.root_url', getWebsiteRootUrl(1),true);

        parent::__construct($params, $contents, $options, $data);

        $detail = $this->params["detail"] ?? null;
        if (is_null($detail) || !$detail || !is_object($detail)) {
            return false;
        }

        $object_key = $this->getObjectKey(get_class($detail));

        $mode = $this->options["initial_mode"] ?? "status_based";

        if(!in_array($mode, ["manual", "auto", "status_based"])){
            $mode = "status_based";
        }

        if($mode == "status_based"){
            $mode = $detail->deleted_at== null ? (request()->has("isNew") && request()->isNew ? "auto" : "manual") : "auto" ;
        }


        $detail_class = get_class($detail);
        $parent_class = get_class($detail->parent);

        $url_base ="";
        switch ($detail_class){
            case 'Mediapress\Modules\Content\Models\CategoryDetail':
                $url_base = getSitemapCategoryUrlBase($detail->parent->sitemap, $detail->country_group_id, $detail->language_id);
                break;
            case 'Mediapress\Modules\Content\Models\PageDetail':
                $url_base = getSitemapUrlBase($detail->parent->sitemap, $detail->country_group_id, $detail->language_id);
                break;
                default:
                    $url_base = getSitemapUrlBase($detail->parent->sitemap, $detail->country_group_id, $detail->language_id);
                break;
        }

        data_set($this->options, "html.attributes.data-detail-type", $detail_class);
        data_set($this->options, "html.attributes.data-detail-id", $detail->id);
        data_set($this->options, "html.attributes.data-language-id", $detail->language_id);
        data_set($this->options, "html.attributes.data-country-group-id", $detail->country_group_id);
        data_set($this->options, "html.attributes.data-parent-type", $parent_class);
        data_set($this->options, "html.attributes.data-parent-id", $detail->parent_id);

        data_set($this->options, "html.attributes.data-mode", $mode);
        data_set($this->options, "html.attributes.data-last-confirmed", ltrim($detail->slug,"/"));
        data_set($this->contents, "input_group.contents.slug_input.options.html.attributes.value", ltrim($detail->slug,"/") ?? "");
        data_set($this->contents, "input_group.contents.slug_input.options.html.attributes.name", $object_key."->".$detail->id."->slug");

        data_set($this->contents, 'input_group.contents.input_group_prepend.contents.input_group_text.contents.root_url', $url_base,true);

    }

    public function getSelfHtmlElement()
    {
        return parent::getSelfHtmlElement(); // TODO: Change the autogenerated stub
    }


}
/*if(is_a($this,"Mediapress\\AllBuilder\\Renderables\\SlugControlDev")){

        $oku = data_get($this->contents,'input_group.contents.input_group_prepend.contents.input_group_text.contents.root_url')." getHtmlElement"; dump($oku);
 }*/
