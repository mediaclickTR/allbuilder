<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;

class InputGroup extends BuilderRenderable
{
    public const ITEMS = "items";
    public $info = [
        "icon_key" => "object-group",
        "object_key" => "InputGroup",
        "object_class" => __CLASS__,
        "object_tags" => [
            "bootstrap"
        ],
        "name" => "Bootstrap Input Group",
        "description" => "Bootstrap Input Group elemanı",
        self::ITEMS => [
            "options" => [
                self::ITEMS => [
                    "html" => [
                        self::ITEMS => [
                            "attributes" => [
                                self::ITEMS => [
                                    "class" => [
                                        "type" => "input_text",
                                        "default_value" => "form-group focus"
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
        ],
    ];
    
    public $options = [
        "html" => [
            "tag" => "div",
            "attributes" => [
                "class" => "form-group"
            ],
            "void_element" => false,
        ]
    ];
    
    public $collectable_as = ["div", "form-group"];
    
}