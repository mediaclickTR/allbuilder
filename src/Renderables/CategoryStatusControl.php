<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Renderables\RadiosAmigos;
use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\MPCore\Models\BaseModel;

class CategoryStatusControl extends RadiosAmigos
{
    
    
    public const DATEPICKER = "datepicker";
    public const ATTRIBUTES = "attributes";
    public const VALUE = "value";
    public const INPUT_TEXT = "input_text";
    public const DEFAULT_VALUE = "default_value";
    public const TITLE = "title";
    public const CUSTOM_TEMPLATE = "custom_template";
    public const OPTIONS = "options";
    public const DEFAULT1 = "default";
    public const CLASS1 = "class";
    public const VALUES = "values";
    public const ITEMS = "items";
    public const MULTILINE = "multiline";
    public const DESCRIPTION = "description";
    public $info = [
        "icon_key" => "bullseye",
        "object_key" => "SitemapStatusControl",
        "object_class" => __CLASS__,
        "object_tags" => [
            "mediapress", "form", "object_specific"
        ],
        "name" => "Kategori Durumu",
        self::DESCRIPTION => "Kategori durumunu ayarlamaya yarayan seçenekler",
        self::ITEMS => [
            self::OPTIONS => [
                self::ITEMS => [
                    self::TITLE => [
                        "key" => self::TITLE,
                        "name" => "Başlık",
                        self::DESCRIPTION => "Nesnenin başlığı",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ""
                    ],
                    self::VALUE => [
                        "key" => self::VALUE,
                        "name" => "Seçili değer",
                        self::DESCRIPTION => "",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ""
                    ],
                    self::DEFAULT1 => [
                        "key" => self::DEFAULT1,
                        "name" => "Varsayılan Seçili değer",
                        self::DESCRIPTION => "",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => BaseModel::DRAFT
                    ],
                    self::MULTILINE => [
                        "key" => self::MULTILINE,
                        "name" => "Çok satırlı yerleşim",
                        self::DESCRIPTION => "Seçeneklerin dikey sıralanmasını sağlar.",
                        "type" => "radio",
                        self::VALUES => [
                            "0" => "Hayır",
                            "1" => "Evet",
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => 1
                    ],
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => self::INPUT_TEXT,
                                self::DEFAULT_VALUE => "div"
                            ],
                            self::ATTRIBUTES => [
                                self::ITEMS => [
                                    self::CLASS1 => [
                                        "type" => self::INPUT_TEXT,
                                        self::DEFAULT_VALUE => "checkbox"
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
            "params" => [
                self::ITEMS => [
                    self::VALUES => [
                        "key" => self::VALUES,
                        "name" => "Seçenekler",
                        self::DESCRIPTION => "Seçilebilecek unsurları barındıran anahtar-değer eşleri dizisi.",
                        "type" => "input_array",
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "[]"
                    ],
                    "category_model"=>[
                        "key" => "object",
                        "name" => "Nesne",
                        "description" => "Durumu belirlenecek kategori modeli",
                        "type" => "input_text",
                        "custom_template" => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "&lt;var&gt;category&lt;/var&gt;"
                    ]
                ]
            ]
        ],
    ];
    
    public $options = [
        self::TITLE => "Yayın Durumu",
        self::MULTILINE => true,
        self::VALUE => null,
        self::DEFAULT1 => BaseModel::DRAFT,
        "iRadio" => true,
        "html" => [
            "tag" => "div",
            "void_element" => false,
            self::ATTRIBUTES => [
                self::CLASS1 => "checkbox"
            ]
        ],
        "components" => [
            "formgroup" => [
                "type" => "div",
                self::OPTIONS => [
                    "html" => [
                        self::ATTRIBUTES => [
                            self::CLASS1 => "form-group"
                        ]
                    ],
                    self::OPTIONS => [
                        "collectable_as" => ["form-group categorystatuscontrol-form-group"]
                    ],
                ]
            
            ],
            "caption" => [
                "type" => "bluetitle",
                self::OPTIONS => [
                    "collectable_as" => ["caption"]
                ]
            ],
        ],
    ];
    
    public $contents = [
        /*"datefield" => [
            "type" => "input",
            "options" => [
                "html" => [
                    "attributes" => [
                        "type" => "hidden",
                        "class" => "datefield"
                    ]
                ]
            ]
        ],*/
        self::DATEPICKER => [
            "type" => "div",
            self::OPTIONS => [
                "html" => [
                    self::ATTRIBUTES => [
                        self::CLASS1 => "datepicks",
                        "style" => "visibility:hidden"
                    ]
                ]
            ],
            "contents" => [
                [
                    "type" => "input",
                    self::OPTIONS => [
                        "html" => [
                            self::ATTRIBUTES => [
                                self::CLASS1 => self::DATEPICKER,
                                "placeholder" => "Tarih Seçiniz",
                                "style" => "opacity:1;",
                            ]
                        ]
                    ],
                
                ]
            ]
        ]
    
    ];
    
    public $params = [
        "category_model" => null
    ];
    
    public $collectable_as = ["radiogroup"];
    
    
    public function __construct(array $params = [], array $contents = [], array $options = [], array $data = [])
    {
        $this->class = get_class($this);
        $this->params = array_replace_recursive($this->params, array_replace_recursive($this->defaultParams(), $params));
        $this->params = $this->parseAnnotations($this->params, true);
    
    
        $this->options = array_replace_recursive(
            $this->baseOptions(),
            array_replace_recursive(
                $this->options,
                array_replace_recursive(
                    $this->defaultOptions(),
                    $options)
            )
        );
        $this->options = $this->parseAnnotations($this->options, true);
        
        $this->info = array_replace_recursive(self::DEFAULT_INFO, $this->info);
        
        extract($this->params);
        
        if (!isset($category_model) || !$category_model || !is_a($category_model, Category::class)) {
            data_set($this->options, "html.tag", null, true);
            $this->ignored_if = true;
            $this->errors[] = "category_model parametresi bir " . Category::class . " nesnesi olmalıdır.";
            return false;
        }
        
        $value = $this->options[self::VALUE] ?? null;
        $values = [
            BaseModel::DRAFT => trans("ContentPanel::page.status.DRAFT"),
            BaseModel::ACTIVE => trans("ContentPanel::page.status.ACTIVE"),
            BaseModel::PASSIVE => trans("ContentPanel::page.status.PASSIVE"),
        ];
        
        /*if (env("ALLOW_POSTDATE_CONTENT", true)) {
            $values[BaseModel::POSTDATE] = trans("ContentPanel::page.status.POSTDATE");
        }*/
        
        data_set($this->data, self::VALUES, $values, true);
        data_set($this->options, "name", "category->" . $category_model->id . "->status", false);
        data_set($this->options, self::VALUE, $category_model->status, true);
        data_set($this->options, self::TITLE, "Yayın Durumu", false);
        if (isset($this->contents["datefield"])) {
            data_set($this->contents, "datefield.options.html.attributes.name", "category->" . $category_model->id . "->published_at");
            /*if ($value == BaseModel::POSTDATE) {
                data_set($this->contents, "datefield.options.html.attributes.value", category_model->published_at);
            }*/
        }
        
        parent::__construct($this->params, $contents, $this->options, $data);
        
    }
    
    public function handleGotContentsHtmlElements(&$htmlelements)
    {
        
        extract($this->params);
        
        $thisid = $this->class_id;
        
        $elid = null;
        foreach ($htmlelements as $el) {
            if ($el->has_class("datefield")) {
                $elid = $el->get_id();
            }
        }
        
        if ($elid) {
            $display_input_id = null;
            foreach ($htmlelements as $el) {
                
                if ($el->has_class("datepicks")) {
                    foreach ($el->contents() as $sub_el) {
                        if ($sub_el->has_class(self::DATEPICKER)) {
                            $display_input_id = $sub_el->get_id();
                        }
                    }
                }
            }
            if ($display_input_id) {
                $postdate_constant = BaseModel::POSTDATE;
                $script = <<<"ABC"
<script>
$(document).ready(function(){
    $( "#$display_input_id" ).datepicker({
        language: 'tr',
        format:"dd/mm/yyyy",
        dateFormat: "dd/mm/yy",
        altFormat: "yy-mm-dd",
        altField:"#$elid",
        monthNames: [ "Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık" ],
        dayNamesMin: [ "Pa", "Pt", "Sl", "Çş", "Pe", "Cu", "Ct" ],
        firstDay:1
    });
    $("input[type=radio]","#$thisid").on("ifChecked",function(){
        if($(this).val() == "$postdate_constant"){
            $(".datepicks",$(this).closest(".checkbox")).css("visibility","visible");
            $(".datepicker",$(this).closest(".checkbox")).prop("disabled",false);
        }else{
            $(".datepicks",$(this).closest(".checkbox")).css("visibility","hidden");
            $(".datepicker",$(this).closest(".checkbox")).prop("disabled",true);
        }
    });
    $("input[type=radio]","#$thisid").iCheck("update");
});
</script>
ABC;
                
                $current_stack_data = data_get($this->data, "stacks.scripts", "");
                //$this->extendStack("scripts",$current_stack_data.$script);
                data_set($this->data, "stacks.scripts", $current_stack_data . $script, true);
                $this->mergeStacks();
                
                
            }
        }
        
    }
    
}
