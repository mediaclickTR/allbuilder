<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\HtmlElement;
use Mediapress\Modules\Content\Models\Page;

class PageCriteriasControl extends BuilderRenderable
{
    public const CONTENTS = "contents";
    public const PAGE_MODEL = "page_model";
    public const ATTRIBUTES = "attributes";
    public const DEFAULT_VALUE = "default_value";
    public const OPTIONS = "options";
    public const ITEMS = "items";
    public const DESCRIPTION = "description";
    public $collectable_as = ["tabs", "stacktabs", "pagecriteriascontrol", "pagecriteriastabs"];


    public $info = [
        "icon_key" => "tasks",
        "object_key" => "PageCriteriasControl",
        "object_class" => __CLASS__,
        "object_tags" => [
            "mediapress", "form", "object_specific"
        ],
        "name" => "Page Criterias",
        self::DESCRIPTION => "Verilen sayfanın kriterlerini listeleyen sekme",
        self::ITEMS => [
            self::OPTIONS => [
                self::ITEMS => [
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "input_text",
                                self::DEFAULT_VALUE => "div"
                            ],
                            self::ATTRIBUTES => [
                                self::ITEMS => [
                                    "id" => [
                                        "key" => "id",
                                        "name" => "HTML ID",
                                        self::DESCRIPTION => "",
                                        "type" => "input_text",
                                        "custom_template" => "", // html - used if input_type is custom,
                                        self::DEFAULT_VALUE => "kriter"
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
            "params" => [
                self::ITEMS => [
                    self::PAGE_MODEL => [
                        "key" => self::PAGE_MODEL,
                        "name" => "Page Nesnesi",
                        self::DESCRIPTION => "Aracın bağlanacağı Page nesnesi",
                        "type" => "object_selector",
                        "object_selector_parameters" => [
                            "selector_type" => "",
                            "selectable_objects" => []
                        ],
                        "custom_template" => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => null
                    ],
                ]
            ]
        ],
    ];


    public $options = [
        "html" => [
            "tag" => "div",
            self::ATTRIBUTES => [
                "id" => "kriter",
                "class"=>"page-criterias-control"
            ]
        ],
        "forced_html_classes"=>["page-criterias-control"],
        "components"=>[
            "nuller"=>[
                "type"=>"input",
                "options"=>[
                    "html"=>[
                        "attributes"=>[
                            "type"=>"hidden",
                            "value"=>""
                        ]
                    ]
                ]
            ],
            "row"=>[
                "type"=>"row",
                "contents"=>[
                    "col_left"=>[
                        "type"=>"div",
                        "options"=>[
                            "html"=>[
                                "attributes"=>[
                                    "class"=>"col-3"
                                ]
                            ]
                        ],
                        "contents"=>[
                            "tab"=>[
                                "type"=>"div",
                                "options"=>[
                                    "html"=>[
                                        "attributes"=>[
                                            "class"=>"tab"
                                        ]
                                    ]
                                ],
                            ]
                        ]
                    ],
                    "col_right"=>[
                        "type"=>"div",
                        "options"=>[
                            "html"=>[
                                "attributes"=>[
                                    "class"=>"col-9"
                                ]
                            ]
                        ],
                        "contents"=>[
                            "tab"=>[
                                "type"=>"div",
                                "options"=>[
                                    "html"=>[
                                        "attributes"=>[
                                            "class"=>"txt"
                                        ]
                                    ]
                                ],
                            ]
                        ]

                    ]
                ]
            ]
        ]
    ];

    public $params = [
        self::PAGE_MODEL => null
    ];


    public function __construct(array $params = [], array $contents = [], array $options = [], array $data = [])
    {

        $this->contents = $this->options["components"];
        unset($this->options["components"]);
        parent::__construct($params, $contents, $options, $data);

        extract($this->params);
        /** @var Page $page_model */

        if (!isset($page_model) || !$page_model || !is_a($page_model, 'Mediapress\Modules\Content\Models\Page')) {
            data_set($this->options, "html.tag", null, true);
            $this->ignored_if = true;
            $this->errors[] = "page_model parametresi bir " . Page::class . " nesnesi olmalıdır.";
            return [];
        }
        data_set($this->contents,"nuller.options.html.attributes.name",$this->getObjectKey('Mediapress\Modules\Content\Models\Page')."->".$page_model->id."->sync:criterias[]");

        $pages_criterias = $page_model->criterias()->select("id")->pluck("id")->toJson();
        data_set($this->options,"html.attributes.data-criteria-ids",$pages_criterias);
        data_set($this->options,"html.attributes.data-page-id", $page_model->id);

    }
}
