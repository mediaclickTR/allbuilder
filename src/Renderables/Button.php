<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;

class Button extends BuilderRenderable
{
    
    public const BUTTON_STR = self::BUTTON;
    public const ICONNAME_STR = "iconname";
    public const ICONSIZE_STR = "iconsize";
    public const BUTTON = "button";
    public const DESCRIPTION = "description";
    public const ITEMS = "items";
    public const INPUT_TEXT = "input_text";
    public const CUSTOM_TEMPLATE = "custom_template";
    public const DEFAULT_VALUE = "default_value";
    public const CAPTION = "caption";

    public $info = [
        "icon_key" => "square",
        "object_key" => "Button",
        "object_class" => __CLASS__,
        "object_tags" => [
            "basic_html", "form"
        ],
        "name" => "Düğme",
        self::DESCRIPTION => "Simgeli ve metinli düğme",
        self::ITEMS => [
            "options" => [
                self::ITEMS => [
                    
                    self::ICONNAME_STR => [
                        "key" => self::ICONNAME_STR,
                        "name" => "Simge adı",
                        self::DESCRIPTION => "Düğmede kullanılacak simgenin kodu (fa- hariç)",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "check"
                    ],
                    self::ICONSIZE_STR => [
                        "key" => self::ICONSIZE_STR,
                        "name" => "Simge boyutu",
                        self::DESCRIPTION => "Düğmede kullanılacak simgenin boyutu (1-4)",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "1"
                    ],
                    //TODO: Buton Tipi özelliği kodlanacak:
                    "button_type" => [
                        "key" => "button_type",
                        "name" => "Düğme Türü (tematik)",
                        self::DESCRIPTION => "Düğmenin anlamsal temasını seçin",
                        "type" => "select",
                        "values" => [
                            "" => "Hiçbiri",
                            "default" => "Varsayılan (default)",
                            "primary" => "Birincil (primary)",
                            "success" => "Başarı (success)",
                            "info" => "Bilgi (info)",
                            "warning" => "Uyarı (warning)",
                            "danger" => "Tehlike (danger)",
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "default"
                    ],
                    self::CAPTION => [
                        "key" => self::CAPTION,
                        "name" => "Başlık",
                        self::DESCRIPTION => "Düğme metni",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "..."
                    ],
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => self::INPUT_TEXT,
                                self::DEFAULT_VALUE => self::BUTTON,
                            ],
                            "attributes" => [
                                self::ITEMS => [
                                    "class" => [
                                        "type" => self::INPUT_TEXT,
                                        self::DEFAULT_VALUE => "btn"
                                    ],
                                    "type" => [
                                        "key" => "type",
                                        "name" => "Düğme tipi (HTML)",
                                        self::DESCRIPTION => "",
                                        "type" => "select",
                                        "values" => [
                                            self::BUTTON => "Standart Düğme",
                                            "reset" => "Form Sıfırla",
                                            "submit" => "Form Gönder",
                                        ],
                                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                                        self::DEFAULT_VALUE => self::BUTTON
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
        ]
    ];
    
    public $options = [
        "html" => [
            "tag" => self::BUTTON,
            "attributes" => [
                "class" => "btn",
                "type" => self::BUTTON
            ]
        ],
        "forced_html_classes"=>["btn"],
        self::ICONNAME_STR => "",
        self::ICONSIZE_STR => 1,
        self::CAPTION => ""
    ];
    public $collectable_as = [self::BUTTON];
    
    public function __construct(array $params = [], array $contents = [], array $options = [], array $data = [])
    {
        parent::__construct($params, $contents, $options, $data);
        
        
        $iconname = $this->options[self::ICONNAME_STR] ?? null;
        $iconsize = $this->options[self::ICONSIZE_STR] ?? 1;
        
        if ($iconname) {
            $this->contents =
                [
                    [
                        "type" => "faicon",
                        "options" => [
                            self::ICONNAME_STR => $iconname,
                            "size-x" => $iconsize
                        ]
                    ]
                ];
        }
        
        $caption = $this->options[self::CAPTION] ?? "";
        
        if ($caption) {
            $this->contents[] = $caption;
        }
        
    }
    
}
