<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\HtmlElement;

class DetailMetasControl extends BuilderRenderable
{
    public const DESCRIPTION = "description";
    public const ITEMS = "items";
    public const OPTIONS = "options";
    public const ATTRIBUTES = "attributes";
    public const CLASS1 = "class";
    public const INPUTWITHLABEL = "inputwithlabel";
    public const TITLE = "title";
    public const DATA_TEMPLATE_KEY = "data-template-key";
    
    public $detail_str = "detail";
    public $info = [
        "icon_key" => "code",
        "object_key" => "DetailMetasControl",
        "object_class" => __CLASS__,
        "object_tags" => [
            "mediapress", "form", "object_specific"
        ],
        "name" => "İçerik Meta Editörü",
        self::DESCRIPTION => "İçeriğin meta verilerini girebileceğiniz bir editör",
        self::ITEMS => [
            self::OPTIONS => [
                self::ITEMS => [
                    "html" => [
                        self::ITEMS => [
                            self::ATTRIBUTES => [
                                self::ITEMS => [
                                    self::CLASS1 => [
                                        "type" => "input_text",
                                        "default_value" => "detail-metas-control"
                                    ],
                                ]
                            ]
                        ]
                    ],
                ]
            ],
            "params" => [
                self::ITEMS => [
                    "detail" => [
                        "key" => "detail",
                        "name" => "Detail Nesnesi",
                        self::DESCRIPTION => "Meta editörünün bağlanacağı *Detail objesi",
                        "type"=>"input_text",
                        /*"type" => "object_selector",
                        "object_selector_parameters" => [
                            "selector_type" => "",
                            "selectable_objects" => [
                                "Sayfa Yapısı Detayı" => "Mediapress\\Modules\\Content\\Models\\SitemapDetail",
                                "Sayfa Detayı" => "Mediapress\\Modules\\Content\\Models\\PageDetail",
                                "Kategori Detayı" => "Mediapress\\Modules\\Content\\Models\\CategoryDetail",
                            ]
                        ],*/
                        "custom_template" => "", // html - used if input_type is custom,
                        "default_value" => "<var>detail</var>"
                    ],
                ]
            ]
        ],
    
    
    ];
    
    public $options = [
        "html" => [
            "tag" => "div",
            self::ATTRIBUTES => [
                self::CLASS1 => "detail-metas-control",
            ]
        ],
        "components" => [
            "modcontrol" => [
                "type" => "toggleswitch",
                "data" => [
                    "on_value" => 1,
                    "off_value" => 0,
                    "on_text" => "Otomatik Seo",
                    "off_text" => "Elle",
                ],
                self::OPTIONS => [
                    "html" => [
                        self::ATTRIBUTES => [
                            self::CLASS1 => "meta-mode-toggler"
                        ]
                    ]
                ]
            ]
        ]
    ];
    
    
    public $params = [
    ];
    
    public function getHtmlElement()
    {
        if ($this->ignored_if) {
            return (new HtmlElement());
        }
        extract($this->params);
        if (!isset($detail) || !$detail || !is_object($detail)) {
            $el = parent::getHtmlElement();
            $el->clear_content();
            $el->add_content((new HtmlElement("p"))->add_content("DetailMetasControl için &lt;detail&gt; parametresi gönderilmemiş."));
            return $el;
        }
        $detail_class = get_class($detail);
        $detail_class_object_key = $this->getObjectKey($detail_class);
        $this->detail_str = $detail_class_object_key."->".$detail->id;
        
        $parent_class = preg_replace('/Detail$/', '', $detail_class);
        if (isset($detail) && is_object($detail)) {
            data_set($this, "options.html.attributes.data-detail-type", $detail_class);
            data_set($this, "options.html.attributes.data-detail-id", $detail->id);
            data_set($this, "options.html.attributes.data-language-id", $detail->language_id);
            data_set($this, "options.html.attributes.data-country-group-id", $detail->country_group_id);
            data_set($this, "options.html.attributes.data-parent-type", $parent_class);
            data_set($this, "options.html.attributes.data-parent-id", $detail->parent_id);
        }
        
        data_set($this->options, "components.modcontrol.options.value", ($detail->manual_meta ? 1 : 0));

        
        
        $el = parent::getHtmlElement();
        
        
        $detail_metas = $detail->url->metas;
        
        $recorded_title = $detail_metas->title;
        $recorded_short_title = $detail_metas->short_title;
        //$recorded_short_title = $detail_metas->short_title;
        
        $modcontrol = $this->buildRenderableFromArray($this->options["components"]["modcontrol"]);
        $modcontrol = $modcontrol->getHtmlElement();
        
        return $el->add_content($this->getPreviewBoxAndModControl($detail, $modcontrol));
    }
    
    
    private function getPreviewBoxAndModControl($detail, $modcontrol)
    {
        
        $row = (new HtmlElement("div"))->add_class("row")->add_content(
            (new HtmlElement("div"))->add_class("col-8")->add_content(
                (new HtmlElement("div"))->add_class(["boxes", "boxes-pro", "seo-preview-box"])->add_content(
                    (new HtmlElement("h2"))->add_content(
                        (new HtmlElement("a"))->add_attr("href", "javascript:void(0);")->add_class("seo-preview-title")->add_content(
                            "<i>Ödüllü İstanbul Web Tasarım Ajansı MediaClick ®</i>"
                        )->add_content(
                            (new HtmlElement("span"))->add_content("Kurumsal web tasarım ajansı")
                        )
                    )->add_content(
                        (new HtmlElement("p"))->add_content(
                            "Kurumsal web tasarım ajansı MediaClick İstanbul, SEO ve mobil uyumlu profesyonel web site tasarımları ile web sitesi tasarımı yarışmalarında ... Kendimizi kısaca 360 derece bütünleşik dijital iletişim hizmeti veren bir ajans olarak tanımlıyoruz."
                        )
                    )
                )
            )
        )->add_content(
            (new HtmlElement("div"))->add_class("col-4")->add_content(
                $modcontrol
            )/*->add_content(
                    (new HtmlElement("button"))->add_content("Aç")->add_attr("type","button")->add_attr("onclick","javascript:metainputlariac(this);")
                )*/
        );
        
        
        return [$row, $this->accordion()];
        
        
    }
    
    private function accordion()
    {
        return (new Accordion([], ["basic" => $this->basicSeoAccordionPane(), "custom" => $this->customSeoAccordionPane()], [], []))->getHtmlElement();
    }
    
    private function basicSeoAccordionPane()
    {
        
        $title_input = [
            "type" => self::INPUTWITHLABEL,
            self::OPTIONS => [
                self::TITLE => "Title",
                "html" => [
                    self::ATTRIBUTES => [
                        self::CLASS1 => "meta-text-input meta-title",
                        self::DATA_TEMPLATE_KEY => self::TITLE,
                        "name"=>$this->detail_str. "->url->metas->title"
                    ]
                ]
            ]
        ];
        
        $mobile_title_input = [
            "type" => self::INPUTWITHLABEL,
            self::OPTIONS => [
                self::TITLE => "Title (mobil)",
                "html" => [
                    self::ATTRIBUTES => [
                        self::CLASS1 => "meta-text-input meta-title-mobile",
                        self::DATA_TEMPLATE_KEY => "title_mobile",
                        "name"=>$this->detail_str. "->url->metas->title_mobile"
                    ]
                ]
            ]
        ];
        
        $desc_input = [
            "type" => self::INPUTWITHLABEL,
            self::OPTIONS => [
                self::TITLE => "Description",
                "html" => [
                    self::ATTRIBUTES => [
                        self::CLASS1 => "meta-text-input meta-description",
                        self::DATA_TEMPLATE_KEY => self::DESCRIPTION,
                        "name"=>$this->detail_str. "->url->metas->description"
                    ]
                ]
            ]
        ];
        
        $keywords_input = [
            "type" => self::INPUTWITHLABEL,
            self::OPTIONS => [
                self::TITLE => "Keywords",
                "html" => [
                    self::ATTRIBUTES => [
                        self::CLASS1 => "meta-text-input meta-keywords",
                        self::DATA_TEMPLATE_KEY => "keywords",
                        "name"=>$this->detail_str. "->url->metas->keywords"
                    ]
                ]
            ]
        ];
        
        
        return (new AccordionPane([], ["title_input" => $title_input, "mobile_title_input" => $mobile_title_input, "desc_input" => $desc_input, "keywords_input" => $keywords_input,], [self::TITLE => "Temel Seo Ayarları"], []))->getHtmlElement();
    }
    
    private function customSeoAccordionPane()
    {
        return (new AccordionPane([], ["deneme metin"], [self::TITLE => "Özel Seo Ayarları"], []))->getHtmlElement();
    }
    
}