<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\HtmlElement;

class InputWithImgBtn extends BuilderRenderable
{
    public const ITEMS = "items";
    public const CONTENTS = "contents";
    public const COMPONENTS = "components";
    public const CLASS1 = "class";
    public const IMAGE = "image";
    public const ATTRIBUTES = "attributes";
    public const INPUT = "input";
    public const DEFAULT_VALUE = "default_value";
    public const OPTIONS = "options";
    public $info = [
        "icon_key" => "edit",
        "object_key" => "InputWithImgBtn",
        "object_class" => __CLASS__,
        "object_tags" => [

        ],
        "name" => "Metin Kutusu, Görsel ve Düğme",
        "description" => "Görsel, metin kutusu ve düğme seti",
        self::ITEMS => [
            self::OPTIONS => [
                self::ITEMS => [
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "readonly_text",
                                self::DEFAULT_VALUE => self::INPUT
                            ],
                            self::ATTRIBUTES => [
                                self::ITEMS => [
                                    "type" => [
                                        "key" => "type",
                                        "name" => "HTML Input Type",
                                        "description" => "",
                                        "type" => "select",
                                        "values" => [
                                            "text" => "Metin (text)",
                                            "hidden" => "Gizli (hidden)",
                                            "password" => "Parola (password)",
                                            "number" => "Sayı (number)",
                                            "tel" => "Telefon (tel)",
                                            "email" => "E-Posta (email)",
                                            "button" => "Düğme (button)",
                                            "checkbox" => "Onay Kutusu (checkbox)",
                                            "radio" => "Seçenek (radio)",
                                            "range" => "Aralık (range)",
                                            "file" => "Dosya (file)",
                                            "date" => "Tarih (date)",
                                            "datetime-local" => "Yerel Tarih/Saat (datetime)",
                                            "time" => "Saat (time)",
                                            "month" => "Ay (month)",
                                            "week" => "Hafta (week)",
                                            "search" => "Arama (search)",
                                            "reset" => "Sıfırla (reset)",
                                            "submit" => "Gönder (submit)",
                                            "color" => "Renk (color)",
                                            self::IMAGE => "Görsel (image)",
                                            "url" => "URL (url)",
                                        ],
                                        "custom_template" => "", // html - used if input_type is custom,
                                        self::DEFAULT_VALUE => "text"
                                    ],
                                    self::CLASS1 => [
                                        "type" => "input_text",
                                        self::DEFAULT_VALUE => ""
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
        ],
    ];

    public $options = [
        "html" => [
            "tag" => self::INPUT,
            "void_element" => true,
            self::ATTRIBUTES => [
                "type" => "text"
            ]
        ],
        self::COMPONENTS => [
            "wrapper" => [
                "type" => "div",
                self::OPTIONS => [
                    "html" => [
                        self::ATTRIBUTES => [
                            self::CLASS1 => "form-group col-12"
                        ]
                    ]
                ]
            ],
            self::IMAGE => [
                "type" => "blank",
                self::OPTIONS => [
                    "html" => [
                        "tag" => "img",
                        "void_element" => false,
                        self::ATTRIBUTES => [
                            "src" => "/vendor/mediapress/images/flags/TR.png"
                        ]
                    ]
                ]
            ],
            "caption" => [
                "type" => "span",
                self::OPTIONS => [
                    "html" => [self::ATTRIBUTES => [self::CLASS1 => "locale-name"]]]
            ],
            "image_wrapper" => [
                "type" => "div",
                self::OPTIONS => ["html" => [self::ATTRIBUTES => [self::CLASS1 => "lang"]]]
            ],
            "input_group" => [
                "type" => "div",
                self::OPTIONS => ["html" => [self::ATTRIBUTES => [self::CLASS1 => "input-group"]]]
            ],
            "buttons" => [
                /*"button1"=>[
                    "type"=>"button",
                    "options"=>[
                        "html"=>[
                            "attributes"=>[
                                "class"=>"btn btn-primary",
                                "type"=>"button"
                            ]
                        ]
                    ],
                    "contents"=>[
                        "caption"=>"Buton 1"
                    ]
                ]*/
            ],
            "cols" => [
                "col1" => [
                    "type" => "div",
                    self::OPTIONS => [
                        "html" => [
                            self::ATTRIBUTES => [
                                self::CLASS1 => "col-2 pa float-left"
                            ]
                        ]
                    ]
                ],
                "col2" => [
                    "type" => "div",
                    self::OPTIONS => [
                        "html" => [
                            self::ATTRIBUTES => [
                                self::CLASS1 => "col-7 pa float-left"
                            ]
                        ]
                    ]
                ],
                "col3" => [
                    "type" => "div",
                    self::OPTIONS => [
                        "html" => [
                            self::ATTRIBUTES => [
                                self::CLASS1 => "col-3 pa float-left"
                            ]
                        ]
                    ]
                ]
            ]
        ],
    ];

    public $collectable_as = ["textinput", self::INPUT, "formfield", "fakeable"];


    public function getHtmlElement()
    {

        if ($this->ignored_if) {
            return (new HtmlElement());
        }
        $el = parent::getHtmlElement();


        $use_old_input = !(isset($this->options["dont_use_old_input"]) && $this->options["dont_use_old_input"]);
        if($use_old_input){
            $input_name = $el->get_attr("name");
            if($input_name && array_key_exists($input_name,old())){
                $el->set_attr("value", old($input_name));
            }
        }

        $el_group = $el;

        $input_group = $this->options[self::COMPONENTS]["input_group"] ?? null;


        if ($input_group) {
            $input_group[self::CONTENTS] = [$el];
            $el_group = $input_group;
        }

        data_set($this->options, "components.cols.col2", [
            "type" => "div",
            self::OPTIONS => [
                "html" => [
                    self::ATTRIBUTES => [
                        self::CLASS1 => "col-8 pa float-left"
                    ]
                ]
            ]
        ], false);

        $wrapper = $this->options[self::COMPONENTS]["wrapper"] ?? null;

        if (!$wrapper) {
            $this->errors[] = "InputWithImgBtn renderable'ı, wrapper bileşeni tanımlanmadan kullanılamaz";
            return (new HtmlElement());
        }

        $col1 = $this->options[self::COMPONENTS]["cols"]["col1"] ?? null;
        $col2 = $this->options[self::COMPONENTS]["cols"]["col2"] ?? null;
        $col3 = $this->options[self::COMPONENTS]["cols"]["col3"] ?? null;


        $img = $this->options[self::COMPONENTS][self::IMAGE] ?? null;
        $caption = $this->options[self::COMPONENTS]["caption"] ?? null;
        if ($caption && is_array($caption) && !isset($caption[self::CONTENTS])) {
            $caption[self::CONTENTS] = [];
        }
        $img_wrapper = $this->options[self::COMPONENTS]["image_wrapper"] ?? null;

        $img_group = $img;
        if ($img_wrapper) {
            if ($caption) {
                array_unshift($caption[self::CONTENTS], $img);
                data_set($img_wrapper, "contents.span", $caption, true);
            } else {
                data_set($img_wrapper, "contents.img", $img, true);
            }
            $img_group = $img_wrapper;
        }

        if ($col1 && $img) {
            data_set($col1, "contents.img", $img_group, true);
            $wrapper[self::CONTENTS]["col1"] = $col1;
        }

        data_set($col2, "contents.input", $el_group);
        data_set($wrapper, "contents.col2", $col2);

        $buttons = data_get($this->options, "components.buttons", null);

        if ($col3 && $buttons) {
            $col3[self::CONTENTS] = $buttons;
            data_set($wrapper, "contents.col3", $col3);
        }
        return $this->buildRenderableFromArray($wrapper)->getHtmlElement();


    }

}
