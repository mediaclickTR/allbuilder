<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;

class Img extends BuilderRenderable
{

    public const ITEMS = "items";
    public $collectable_as = ["img"];
    public $info = [
        "icon_key" => "image",
        "object_key" => "Img",
        "object_class" => __CLASS__,
        "object_tags" => [
            "basic_html"
        ],
        "name" => "HTML Image",
        "description" => "Standart HTML Image nesnesi (img)",
        self::ITEMS => [
            "options" => [
                self::ITEMS => [
                    "quick_src"=>[
                        "key" => "quick_src",
                        "name" => "Kaynak (src), Hızlı ayar",
                        "description" => "Görsel kaynağı belirtin",
                        "type" => "input_text",
                        "custom_template" => "", // html - used if input_type is custom,
                        "default_value" => "/vendor/mediapress/images/flags/0.png"
                    ],
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "readonly_text",
                                "default_value" => "img"
                            ],
                            "attributes" => [
                                self::ITEMS => [
                                    "src" => [
                                        "key" => "src",
                                        "name" => "Kaynak (src)",
                                        "description" => "Görsel kaynağı belirtin",
                                        "type" => "input_text",
                                        "custom_template" => "", // html - used if input_type is custom,
                                        "default_value" => "/vendor/mediapress/images/flags/0.png"
                                    ],
                                    "class" => [
                                        "type" => "input_text",
                                        "default_value" => ""
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
        ],
    ];
    
    public $options = [
        "html" => [
            "tag" => "img",
            "void_element"=>true
        ]
    ];
    
    public function __construct(array $params = [], array $contents = [], array $options = [], array $data = [])
    {
        
        parent::__construct($params, $contents, $options, $data);
        $quick_src = $this->options["quick_src"] ?? null;
        $current_src = $this->options["html"]["attributes"]["src"] ?? null;
        if(is_null($current_src) && !is_null($quick_src)){
            $this->options["html"]["attributes"]["src"] = $quick_src;
        }
    }
    
}