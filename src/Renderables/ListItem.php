<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;

class ListItem extends BuilderRenderable
{
    public const ITEMS = "items";
    public $info = [
        "icon_key" => "stream",
        "object_key" => "ListItem",
        "object_class" => __CLASS__,
        "object_tags" => [
            "basic_html"
        ],
        "name" => "HTML ListItem",
        "description" => "Standart LI etiketi",
        self::ITEMS => [
            "options" => [
                self::ITEMS => [
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "readonly_text",
                                "default_value" => "li"
                            ],
                            "attributes" => [
                                self::ITEMS => [
                                    "class" => [
                                        "type" => "input_text",
                                        "default_value" => ""
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
        ],
    ];
    public $options = [
        "html" => [
            "tag" => "li"
        ]
    ];
    
    public $collectable_as = ["li", "listitem"];
    
}