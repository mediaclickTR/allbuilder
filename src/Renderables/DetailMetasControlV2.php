<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\HtmlElement;
use Mediapress\Modules\Content\Models\Meta;
use Mediapress\Modules\Content\Models\Sitemap;

class DetailMetasControlV2 extends BuilderRenderable
{

    public const ITEMS = "items";
    public $detail_str = null;
    public $collectable_as = ["detailmetascontrolv2"];
    public $info = [
        "icon_key" => "code",
        "object_key" => "DetailMetasControlV2",
        "object_class" => __CLASS__,
        "object_tags" => [
            "mediapress", "form", "object_specific"
        ],
        "name" => "İçerik Meta Editörü v2",
        "description" => "İçeriğin meta verilerini girebileceğiniz bir editör",
        self::ITEMS => [
            "options" => [
                self::ITEMS => [
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "input_text",
                                "default_value" => "div"
                            ],
                            "attributes" => [
                                self::ITEMS => [
                                    "class" => [
                                        "type" => "input_text",
                                        "default_value" => "col-12"
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
            "params" => [
                self::ITEMS => [
                    "detail" => [
                        "key" => "detail",
                        "name" => "Detail Nesnesi",
                        "description" => "Meta editörünün bağlanacağı *Detail objesi",
                        "type" => "input_text",
                        /*"type" => "object_selector",
                        "object_selector_parameters" => [
                            "selector_type" => "",
                            "selectable_objects" => [
                                "Sayfa Yapısı Detayı" => "Mediapress\\Modules\\Content\\Models\\SitemapDetail",
                                "Sayfa Detayı" => "Mediapress\\Modules\\Content\\Models\\PageDetail",
                                "Kategori Detayı" => "Mediapress\\Modules\\Content\\Models\\CategoryDetail",
                            ]
                        ],*/
                        "custom_template" => "", // html - used if input_type is custom,
                        "default_value" => "<var>detail</var>"
                    ],
                ]
            ]
        ],
    ];

    public $options = [
        "html" => [
            "tag" => "div",
            "attributes" => [
                "class" => "detail-metas-control-v2"
            ]
        ],
        "forced_html_classes" => [
            "detail-metas-control-v2",
        ],
        "components" => [
            "clearfix" => [
                "type" => "clearfix",
            ],
            "add_snippet_area" => [
                "type" => "div",
                "options" => [
                    "html" => [
                        "attributes" => [
                            "class" => "addSnippetArea",
                        ]
                    ]
                ],
                "contents" => [
                    [
                        "type" => "button",
                        "options" => [
                            "caption" => "Meta Anahtarı Ekle",
                            "iconname"=>"plus",
                            "html"=>[
                                "attributes"=>[
                                    "class"=>"float-right"
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            "meta_row_model" => [
                "type" => "div",
                "options" => [
                    "html" => [
                        "attributes" => [
                            "class" => "lists stitle",
                            "data-meta-name" => "title",
                            "data-id" => "",
                            "data-meta-device" => "desktop"
                        ]
                    ]
                ],
                "contents" => [
                    "row" => [
                        "type" => "row",
                        "options"=>[
                            "html"=>[
                                "attributes"=>[
                                    "class"=>"float-none w-auto"
                                ]
                            ]
                        ],
                        "contents" => [
                            "col2" => [
                                "type" => "div",
                                "options" => [
                                    "html" => [
                                        "attributes" => [
                                            "class" => "col-sm-2"
                                        ]
                                    ]
                                ],
                                "contents" => [
                                    "counter" => [
                                        "type" => "small",
                                        "options" => [
                                            "html" => [
                                                "attributes" => [
                                                    "class" => "sCounter",
                                                    "data-limit-low" => 55,
                                                    "data-limit-high" => 65,
                                                ]
                                            ]
                                        ],
                                        "contents" => [
                                            "metakey" => "",
                                            "em" => [
                                                "type" => "em",
                                                "contents" => [
                                                    0
                                                ]
                                            ]
                                        ]
                                    ],
                                ]
                            ],
                            "col8" => [
                                "type" => "div",
                                "options" => [
                                    "html" => [
                                        "attributes" => [
                                            "class" => "col-sm-8",
                                            "style" => "padding-left: 0"
                                        ]
                                    ]
                                ],
                                "contents" => [
                                    "seoInput" => [
                                        "type" => "div",
                                        "options" => [
                                            "html" => [
                                                "attributes" => [
                                                    "class" => "divInput",
                                                    "spellcheck" => "false",
                                                    //"contenteditable"=>"true"
                                                ]
                                            ]
                                        ],
                                        "contents" => [
                                            "metavalue" => "Mediaclick Web Tasarım"
                                        ]
                                    ]
                                ]
                            ],
                            "col2last" => [
                                "type" => "div",
                                "options" => [
                                    "html" => [
                                        "attributes" => [
                                            "class" => "col-sm-2",
                                            "style" => "padding-left: 0"
                                        ]
                                    ]
                                ],
                                "contents" => [
                                    "add" => [
                                        "type" => "div",
                                        "options" => [
                                            "html" => [
                                                "attributes" => [
                                                    "class" => "ekle",
                                                    "style" => "padding-left: 0;"
                                                ]
                                            ]
                                        ],
                                        "contents" => [
                                            "Değişken Ekle"
                                        ]
                                    ],
                                ]
                            ]
                        ]
                    ],
                ]
            ],
            "desktop_box" => [
                "type" => "div",
                "options" => [
                    "html" => [
                        "attributes" => [
                            "class" => "box-list desktop",
                            "data-meta-name-prefix" => "desktop_"
                        ]
                    ]
                ],
                "contents" => [
                    "head_div" => [
                        "type" => "div",
                        "options" => [
                            "html" => [
                                "attributes" => [
                                    "class" => "head"
                                ]
                            ]
                        ],
                        "contents" => [
                            "clearfix" => [
                                "type" => "clearfix"
                            ],
                            "img" => [
                                "type" => "img",
                                "options" => [
                                    "quick_src" => "/vendor/mediapress/images/web-site-icon.png"
                                ]
                            ],
                            "Masaüstü"
                        ]
                    ],
                    "preview_div" => [
                        "type" => "div",
                        "options" => [
                            "html" => [
                                "attributes" => [
                                    "class" => "boxes boxes-pro hidden"
                                ]
                            ]
                        ],
                    ],
                    "inputs_area" => [
                        "type" => "div",
                        "options" => [
                            "html" => [
                                "attributes" => [
                                    "class" => "select-item"
                                ]
                            ]
                        ],
                        "contents" => [
                        ]

                    ]
                ]
            ],
            "modal" => [
                "type" => "div",
                "options" => [
                    "html" => [
                        "attributes" => [
                            "class" => "sModal"
                        ]
                    ]
                ],
                "contents" => [
                    "centered" => [
                        "type" => "div",
                        "options" => [
                            "html" => [
                                "attributes" => [
                                    "class" => "centered"
                                ]
                            ]
                        ],
                        "contents" => [
                            "times" => [
                                "type" => "div",
                                "options" => [
                                    "html" => [
                                        "attributes" => [
                                            "class" => "clse"
                                        ]
                                    ]
                                ],
                                "contents" => [
                                    "&times;"
                                ]
                            ],
                            "input" => [
                                "type" => "input",
                                "options" => [
                                    "html" => [
                                        "attributes" => [
                                            "placeholder" => "Ara..."
                                        ]
                                    ]
                                ],
                            ],
                            "ul" => [
                                "type" => "ul",
                                "contents" => [
                                    "<li>Eleman 1</li><li>Eleman 2</li><li>Eleman 3</li>"
                                ]
                            ],
                            "add" => [
                                "type" => "button",
                                "options" => [
                                    "caption" => "Değişken Ekle"
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ];


    public function __construct(array $params = [], array $contents = [], array $options = [], array $data = [])
    {
        $meta_row_model = data_get($this->options, "components.meta_row_model", null);
        if (!$meta_row_model) {
            $this->errors[] = $this->contents = "DetailMetasControlV2 için options.components.meta_row_model belirtilmemiş";
            return;
        }
        unset($this->options["components"]["meta_row_model"]);
        $this->contents = $this->options["components"];
        parent::__construct($params, $contents, $options, $data);
        data_set($this->data, "stacks.scripts", $this->getJavascript());

        $detail = $this->params["detail"] ?? null;
        if (is_null($detail) || !$detail || !is_object($detail)) {
            return false;
        }

        $meta_object_key = $this->getObjectKey(Meta::class);

        $detail_class = get_class($detail);
        $detail_class_object_key = $this->getObjectKey($detail_class);
        $this->detail_str = $detail_class_object_key . "->" . $detail->id;
        data_set($this->options, "html.attributes.data-field-name-prefix", $this->detail_str);

        $parent_class = preg_replace('/Detail$/', '', $detail_class);

        data_set($this, "options.html.attributes.data-detail-type", $detail_class);
        data_set($this, "options.html.attributes.data-meta-object-key", $meta_object_key);
        data_set($this, "options.html.attributes.data-url-type", get_class($detail->url));
        data_set($this, "options.html.attributes.data-url-id", $detail->url->id);
        data_set($this, "options.html.attributes.data-detail-id", $detail->id);
        data_set($this, "options.html.attributes.data-language-id", $detail->language_id);
        data_set($this, "options.html.attributes.data-country-group-id", $detail->country_group_id);
        data_set($this, "options.html.attributes.data-parent-type", $parent_class);
        data_set($this, "options.html.attributes.data-parent-id", $detail->parent_id);

        $metas = $detail->url->metas ?? collect();


        $keys_ = $metas->pluck("key")->toArray();
        $keys = array_filter($keys_, function ($value) {
            return !in_array($value, ["title", "description", "keywords"]);
        });

        array_unshift($keys, "og:image");
        array_unshift($keys, "og:description");
        array_unshift($keys, "og:title");
        array_unshift($keys, "keywords");
        array_unshift($keys, "description");
        array_unshift($keys, "title");

        //dump($keys);

        foreach ($keys as $metakey) {

            if (!$metakey) {
                continue;
            }
            //dd($detail->url);

            if($detail->parent && is_a($detail->parent, Sitemap::class) && $detail->parent->category && $detail->categoryUrl) {
                $model = $detail->categoryUrl->metas->$metakey->model;
            } else {
                $model = $detail->url->metas->$metakey->model;
            }
            //dump($model->id);
            if (!$model) {
                continue;
            }

            $opt = [
                "html" => [
                    "attributes" => [
                        "value" => $model->desktop_value,
                        "type" => "hidden",
                        "name" => $meta_object_key . "->" . $model->id . "->desktop_value",
                        "data-meta-name" => $metakey,
                        "data-meta-device" => "desktop",
                        "class"=>"meta_base_input"
                    ]
                ]
            ];
            $row_model = $meta_row_model;

            $el = new Input([], [], $opt, []);

            data_set($this->contents, $metakey . "_desktop_hidden_input", $el);

            data_set($row_model, "options.html.attributes.data-meta-name", $metakey, true);  // satırın meta-name attribute'u
            data_set($row_model, "options.html.attributes.data-meta-exists", 1, true);  // satırın meta-name attribute'u
            data_set($row_model, "options.html.attributes.data-id", $model->id, true);  // satırın meta-id attribute'u
            data_set($row_model, "contents.row.contents.col2.contents.counter.contents.metakey", $metakey, true); // Satırın Başlığı
            //data_set($row_model,"options.html.attributes.data-meta-device","desktop",true);  // satırın meta-device attribute'u
            data_set($row_model, "contents.row.contents.col8.contents.seoInput.contents.metavalue", $model->desktop_value ? htmlspecialchars($model->desktop_value) : "", true); // Satırın Değeri

            //dump($row_model);
            data_set($this->contents, "desktop_box.contents.inputs_area.contents." . $metakey . "_meta_row", $row_model);

        }

    }

    public function getHtmlElement()
    {
        $id = $this->class_id;
        $detail = $this->params["detail"] ?? null;

        $detailMeta = $detail->url->metas;
        $seoManuelStatus = empty($detailMeta->title->desktop_value) && empty($detailMeta->description->desktop_value) && empty($detailMeta->keywords->desktop_value);

        if (is_null($detail) || !$detail || !is_object($detail)) {
            $el = parent::getHtmlElement();
            $el->clear_content();
            $el->add_content((new HtmlElement("p"))->add_content("DetailMetasControlV2 için &lt;detail&gt; parametresi gönderilmemiş."));
        }else{
            $toggler_id = $id."_toggler";
            $wrapper_id = $id."_wrapper";
            $outer_wrapper = (new HtmlElement("div"))->add_class("detail-metas-control-v2-outer-wrapper row");
            $col_left = (new HtmlElement("div"))->add_class("col-md-6 col-lg-8 previewBox mb-5");
            $col_right = (new HtmlElement("div"))->add_class("col-md-6 col-lg-4 p-3");



            $wrapper = (new HtmlElement("div"))->add_class("col-md-12 detail-metas-control-v2-wrapper")->set_id($wrapper_id);
            $toggler = (new ToggleSwitch([],[],["value"=> $seoManuelStatus ? 'auto' : 'manual' ],["on_value"=>"manual","off_value"=>"auto","on_text"=>"Manuel","off_text"=>"Otomatik SEO",]))->getHtmlElement()->set_id($toggler_id);
            $this->extendStack("scripts",$this->getTogglerJs($toggler_id,$wrapper_id));

            $el = parent::getHtmlElement();
            $wrapper->add_content($el);
            $col_right->add_content($toggler);
            $outer_wrapper->add_content($col_right)->add_content($col_left)->add_content($wrapper);

            $el = $outer_wrapper;

        }

        return $el;

    }

    public function setPreview() {

    }

    public function getTogglerJs($toggler_el_id, $wrapper_el_id){

        return <<<"EOT"
<script>
$(document).ready(function(){
    $("#$toggler_el_id input").change(function(){
        if($(this).val()=="manual"){
            $("#$wrapper_el_id").slideDown(500);
        }else{
            $("#$wrapper_el_id").slideUp(500);
        }
    }).change();
});
</script>
EOT;

    }

    public function getJavascript()
    {
        $metacontrolid = $this->class_id;
        return <<<"EOT"
<script>
$(document).ready(function(){
    $('.divInput').attr("contenteditable","true");
    $(".divInput","#$metacontrolid").each(function(i,e){
        var firstContent = $(e).contents()[0];
        if(firstContent.nodeType == 3){
            firstContent.nodeValue=firstContent.nodeValue.trim();
        }
    });
    $("#$metacontrolid").delegate(".divInput","blur", function(){
        var contents = [];
        var container = $("#$metacontrolid");
        var device = $(this).closest(".lists").data("meta-device");
        var meta_name = $(this).closest(".lists").data("meta-name");
        $.each($(this).contents(), function(i,e){
            switch(e.nodeType){
                case 1:
                    if($(e).prop("tagName")=="SPAN"){
                        //contents.push($(e).clone().children().remove().end().text());
                        contents.push("%"+$(e).data("meta-key")+"%");
                    }
                    break;
                case 3:
                    //javascript remove new lines: .replace(/\\r?\\n|\\r/gm,"")
                    //javascript replace &nbsp; s : .replace(/\u00a0/g, " ")
                    contents.push(e.nodeValue.replace(/\u00a0/g, " ").replace(/\\r?\\n|\\r/gm,""));
                    break;
                default:
                    break;
            }
        });
        // Reduce multiple spaces to one: s.replace(/ +(?= )/g,'')
        var content = contents.join("").trim().replace(/ +(?= )/g,'');
        var hidden_input = $("input[type=hidden][data-meta-name="+meta_name.replace(/:/, '\\\\:')+"][data-meta-device="+device+"]",container);
        if(!hidden_input.length){
            //TODO: yeni eklenen metalar için yöntem bul:
            var name_prefix = $(container).data("field-name-prefix");
            $(container).append('<input type="hidden" name="'+name_prefix+"->url->metas->"+meta_name+'" value="'+content+'" data-meta-name="'+meta_name+'">');
        }else{
            hidden_input.val(content);
        }

        //console.log($(this).contents());
    });
        $('[data-meta-name^="og\:"] em').remove();
});
</script>
EOT;

    }


}
