<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;

class Accordion extends BuilderRenderable
{
    public const ITEMS = "items";
    public $info = [
        "icon_key" => "bars",
        "object_key" => "Accordion",
        "object_class" => __CLASS__,
        "object_tags" => [
            "container", "mediapress"
        ],
        "name" => "Akordeon Grubu",
        "description" => "Birden fazla Akordeon nesnesinin gruplandığı ana kapsayıcı",
        self::ITEMS => [
            "options" => [
                self::ITEMS => [
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "input_text",
                                "default_value" => "div"
                            ],
                            "attributes" => [
                                self::ITEMS => [
                                    "class" => [
                                        "type" => "input_text",
                                        "default_value" => "accordion row"
                                    ]
                                ]
                            ]
                        ]
                    
                    ],
                
                
                ]
            ],
        ],
    ];
    
    public $options = [
        "html" => [
            "tag" => "div",
            "attributes" => [
                "class" => "accordion row"
            ]
        ]
    ];
    
}