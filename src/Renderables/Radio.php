<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\HtmlElement;

class Radio extends BuilderRenderable
{
    public const LABEL = "label";
    public const COMPONENTS = "components";
    public const CLASS1 = "class";
    public const ATTRIBUTES = "attributes";
    public const TITLE = "title";
    public const INPUT_TEXT = "input_text";
    public const CHECKED_IF_VALUE_IS = "checked_if_value_is";
    public const DEFAULT_VALUE = "default_value";
    public const CUSTOM_TEMPLATE = "custom_template";
    public const RADIO = "radio";
    public const CHECKED = "checked";
    public const ITEMS = "items";
    public const DESCRIPTION = "description";
    public $info = [
        "icon_key" => "dot-circle",
        "object_key" => "Radio",
        "object_class" => __CLASS__,
        "object_tags" => [
            "basic_html", "form"
        ],
        "name" => "Html Radio",
        self::DESCRIPTION => "Standart Html Seçenek Etiketi",
        self::ITEMS => [
            "options" => [
                self::ITEMS => [
                    self::CHECKED => [
                        "key" => self::CHECKED,
                        "name" => "İşaretli",
                        self::DESCRIPTION => "Seçim kutusunun seçili olup olmayacağını belirler (zorla)",
                        "type" => self::RADIO,
                        "values" => [
                            "0" => "Hayır",
                            "1" => "Evet",
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ":null"
                    ],
                    self::CHECKED_IF_VALUE_IS => [
                        "key" => self::CHECKED_IF_VALUE_IS,
                        "name" => "Değer şuysa seçili",
                        self::DESCRIPTION => "Değerin koşula uyması durumunda nesne seçili gelecektir.",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ":null"
                    ],
                    self::TITLE => [
                        "key" => self::TITLE,
                        "name" => "Başlık",
                        self::DESCRIPTION => "Nesnenin başlığı",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ""
                    ],
                    "html" => [
                        self::ITEMS => [
                            self::ATTRIBUTES => [
                                self::ITEMS => [
                                    self::CLASS1 => [
                                        "type" => self::INPUT_TEXT,
                                        self::DEFAULT_VALUE => ""
                                    ],
                                    "type" => [
                                        "key" => "type",
                                        "name" => "HTML Input Type",
                                        self::DESCRIPTION => "",
                                        "type" => self::INPUT_TEXT,
                                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                                        self::DEFAULT_VALUE => self::RADIO
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
        ],
    ];

    public $options = [
        self::TITLE => "",
        self::CHECKED => null,
        self::CHECKED_IF_VALUE_IS => null,
        "html" => [
            "tag" => "input",
            "void_element" => false,
            self::ATTRIBUTES => [
                "type" => self::RADIO,
                self::CLASS1 => "form-check-input",
                "value" => ""
            ]
        ],
        self::COMPONENTS => [
            self::LABEL => [
                "type" => self::LABEL,
                "options" => [
                    "html" => [
                        "tag" => self::LABEL,
                        "void_element" => false,
                        self::ATTRIBUTES => [
                            self::CLASS1 => "form-check-label",
                            "style" => "margin-right:30px;"
                        ]
                    ],
                    "collectable_as" => [self::LABEL]
                ],
                "contents" => []
            ]
        ]
    ];

    public $collectable_as = [self::RADIO, "input", "formfield"];

    /**
     * @return \Mediapress\Foundation\HtmlElement
     */
    public function getHtmlElement()
    {
        if ($this->ignored_if) {
            return (new HtmlElement());
        }
        $el = parent::getHtmlElement();
        $value = $el->get_attr("value");
        $name = $el->get_attr("name");

        $use_old_input = !(isset($this->options["dont_use_old_input"]) && $this->options["dont_use_old_input"]);

        /*if (!is_null($this->data["value"])) {
            $el->add_attr("value", $this->data["value"]);
        }*/
        if (isset($this->options[self::TITLE])) {
            $el->clear_content()->add_content($this->options[self::TITLE]);
        }



        if($use_old_input && array_key_exists($name, old())){
            $el->add_attr(self::CHECKED, self::CHECKED);
        }else{
            if (!is_null($this->options[self::CHECKED_IF_VALUE_IS]) && $this->options[self::CHECKED_IF_VALUE_IS] == $value) {
                $el->add_attr(self::CHECKED, self::CHECKED);
            }

            if (isset($this->options[self::CHECKED])
                &&
                ($this->options[self::CHECKED] === true ||
                    (is_array($this->options[self::CHECKED]) && $this->processConditionsArray($this->options[self::CHECKED]))
                )
            ) {
                $el->add_attr(self::CHECKED, self::CHECKED);
            }
        }

        data_set($this->options[self::COMPONENTS][self::LABEL], "options.html.attributes.for", $el->get_id());

        $label = $this->buildRenderableFromArray($this->options[self::COMPONENTS][self::LABEL])->getHtmlElement();
        $label->add_content($el);


        return $label;

    }

}
