<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\HtmlElement;

class Alert extends BuilderRenderable
{
    public const DESCRIPTION = "description";
    public const ITEMS = "items";
    public const ALERT_TYPE = "alert_type";
    public const PRIMARY = "primary";
    public const CUSTOM_TEMPLATE = "custom_template";
    public const DEFAULT_VALUE = "default_value";
    public const HEADING = "heading";
    public const DISMISSABLE = "dismissable";
    public const INPUT_TEXT = "input_text";
    public const ALERT = "alert";
    public $info = [
        "icon_key" => "bell",
        "object_key" => "Alert",
        "object_class" => __CLASS__,
        "object_tags" => [
            "bootstrap"
        ],
        "name" => "Bootstrap Alert",
        self::DESCRIPTION => "Önemli mesajlarınızı göstermenize yarayacak bildirim kutusu",
        self::ITEMS =>[
            "options" => [
                self::ITEMS => [
            
                    self::ALERT_TYPE => [
                        "key" => self::ALERT_TYPE,
                        "name" => "Bildirim Türü",
                        self::DESCRIPTION => "Bildirimin anlamsal temasını seçin",
                        "type" => "select",
                        "values" => [
                            "default" => "Varsayılan (default)",
                            self::PRIMARY => "Birincil (primary)",
                            "success" => "Başarı (success)",
                            "info" => "Bilgi (info)",
                            "warning" => "Uyarı (warning)",
                            "danger" => "Tehlike (danger)",
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => self::PRIMARY
                    ],
                    self::HEADING => [
                        "key" => self::HEADING,
                        "name" => "Başlık",
                        self::DESCRIPTION => "Kalın gösterilecek bildirim başlığı",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ""
                    ],
                    self::DISMISSABLE => [
                        "key" => self::DISMISSABLE,
                        "name" => "Kapatılabilir",
                        self::DESCRIPTION => "Kullanıcının bildirimi kapatıp kapatamayacağını seçin",
                        "type" => "radio",
                        "values" => [
                            "1" => "Evet",
                            "0" => "Hayır",
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => true
                    ],
            
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => self::INPUT_TEXT,
                                self::DEFAULT_VALUE => "div"
                            ],
                            "attributes" => [
                                self::ITEMS => [
                                    "class" => [
                                        "type" => self::INPUT_TEXT,
                                        self::DEFAULT_VALUE => ""
                                    ],
                                    "role" => [
                                        "key" => "role",
                                        "name" => "Alert role",
                                        self::DESCRIPTION => "",
                                        "type" => self::INPUT_TEXT,
                                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                                        self::DEFAULT_VALUE => self::ALERT
                                    ]
                                ]
                            ]
                        ]
            
                    ],
        
        
                ]
            ],
            
        ],
    
    
    ];
    public $options = [
        self::ALERT_TYPE => self::PRIMARY,
        self::HEADING => null,
        self::DISMISSABLE => true,
        "html" => [
            "tag" => "div",
            "void_element" => false,
            "attributes" => [
                "role" => self::ALERT
            ]
        ]
    ];
    
    public $collectable_as = [self::ALERT];
    
    public function getHtmlElement()
    {
        if ($this->ignored_if) {
            //TODO: Code in needed lines: eleminate ignored items , so they won't count in actions like collect, validate etc.
            return (new HtmlElement());
        }
        
        $el = $this::getSelfHtmlElement();
        
        $contents = $this->getContentHtmlElements();
        
        $alert_type = $this->options[self::ALERT_TYPE] ?? self::PRIMARY;
        
        $heading = $this->options[self::HEADING] ?? null;
        $dismissable = $this->options[self::DISMISSABLE] ?? null;
        
        $el->add_class(self::ALERT)->add_class("alert-$alert_type");
        
        if ($heading) {
            $heading = (new HtmlElement('h4'))->add_class("alert-heading")->add_content($heading);
            array_unshift($contents, $heading);
        }
        
        if ($dismissable) {
            $close_button = (new HtmlElement("button"))->add_class("close")->add_attr("data-dismiss", self::ALERT)->add_attr("aria-label", "Kapat");
            $span = (new HtmlElement("span"))->add_attr("aria-hidden", "true")->add_content("&times;");
            $close_button->add_content($span);
            array_push($contents, $close_button);
        }
        
        
        $el->add_content($contents);
        return $el;
        
    }
    
}