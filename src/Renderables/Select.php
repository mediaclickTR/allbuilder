<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\HtmlElement;

class Select extends BuilderRenderable
{
    public const SELECTED = "selected";
    public const OPTION = "option";
    public const VALUE = "value";
    public const SELECT = "select";
    public const DEFAULT_TEXT = "default_text";
    public const MERGE = "merge";
    public const ADDITIONAL_CONTENT = "additional_content";
    public const SHOW_DEFAULT_OPTION = "show_default_option";
    public const VALUES = "values";
    public const RADIO = "radio";
    public const MULTIPLE = "multiple";
    public const DEFAULT_VALUE = "default_value";
    public const CUSTOM_TEMPLATE = "custom_template";
    public const INPUT_TEXT = "input_text";
    public const ITEMS = "items";
    public const DESCRIPTION = "description";
    public $info = [
        "icon_key" => "caret-square-down",
        "object_key" => "Select",
        "object_class" => __CLASS__,
        "object_tags" => [
            "basic_html", "form"
        ],
        "name" => "HTML Select",
        self::DESCRIPTION => "Standart HTML Select Etiketi",
        self::ITEMS => [
            "options" => [
                self::ITEMS => [
                    "title" => [
                        "key" => "title",
                        "name" => "Başlık",
                        self::DESCRIPTION => "Nesnenin başlığı",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ""
                    ],
                    self::MULTIPLE => [
                        "key" => self::MULTIPLE,
                        "name" => "Çoklu Seçim",
                        self::DESCRIPTION => "Birden fazla değer seçmeye izin verilip verilmeyeceğini belirler",
                        "type" => self::RADIO,
                        self::VALUES => [
                            "0" => "Hayır",
                            "1" => "Evet",
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "0"
                    ],
                    self::VALUE => [
                        "key" => self::VALUE,
                        "name" => "Değer",
                        self::DESCRIPTION => "Nesneye atanacak değer",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ""
                    ],
                    self::SHOW_DEFAULT_OPTION => [
                        "key" => self::SHOW_DEFAULT_OPTION,
                        "name" => "Varsayılan Seçimi Göster",
                        self::DESCRIPTION => "Kullanıcı seçim yapmadan önce seçili görünecek bir değer olup olmayacağını belirler",
                        "type" => self::RADIO,
                        self::VALUES => [
                            "1" => "Evet",
                            "0" => "Hayır",
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => null
                    ],
                    self::ADDITIONAL_CONTENT => [
                        "key" => self::ADDITIONAL_CONTENT,
                        "name" => "Data ile üretilen içerik",
                        self::DESCRIPTION => "Data parametrelerinden üretilen içeriği, hali hazırda bulunan içerikle değiştirilmesini ya da ona eklenmesini belirler",
                        "type" => self::RADIO,
                        self::VALUES => [
                            "replace" => "Yer değiştir",
                            self::MERGE => "Birleştir",
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => self::MERGE
                    ],
                    self::DEFAULT_VALUE => [
                        "key" => self::DEFAULT_VALUE,
                        "name" => "Varsayılan eleman değeri",
                        self::DESCRIPTION => "",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => null
                    ],
                    self::DEFAULT_TEXT => [
                        "key" => self::DEFAULT_TEXT,
                        "name" => "Varsayılan eleman metni",
                        self::DESCRIPTION => "",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "---Seçiniz---"
                    ],
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "readonly_text",
                                self::DEFAULT_VALUE => self::SELECT
                            ],
                            "attributes" => [
                                self::ITEMS => [
                                    "class" => [
                                        "type" => self::INPUT_TEXT,
                                        self::DEFAULT_VALUE => ""
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
            "data" => [
                self::ITEMS => [
                    self::VALUES => [
                        "key" => self::VALUES,
                        "name" => "Seçenekler",
                        self::DESCRIPTION => "Seçilebilecek unsurları barındıran anahtar-değer eşleri dizisi.",
                        "type" => "input_array",
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "[]"
                    ]
                ]
            ]

        ],
    ];
    public $options = [
        self::VALUE => null,
        "html" => [
            "tag" => self::SELECT,
            "void_element" => false,
        ],
        self::MULTIPLE => false,
        self::ADDITIONAL_CONTENT => self::MERGE, // replace
        self::SHOW_DEFAULT_OPTION => true,
        self::DEFAULT_TEXT => "---- Seçim Yok ----",
        self::DEFAULT_VALUE => ""
    ];

    public $collectable_as = [self::SELECT, "input", "formfield"];


    public function __construct(array $params = [], array $contents = [], array $options = [], array $data = [])
    {
        parent::__construct($params, $contents, $options, $data);

        $this->_objectSpecificConstructor();

    }

    protected function _objectSpecificConstructor(){

        $values = $this->data[self::VALUES] ?? null;
        if(! is_countable($values)){
            $values = [];
        }

        $additional_content = [];

        $option_cls_path = $this->options["renderable_types"][self::OPTION] ?? null;

        $use_old_input = !(isset($this->options["dont_use_old_input"]) && $this->options["dont_use_old_input"]);
        $name = data_get($this->options,"html.attributes.name", null);
        if($name && array_key_exists($name,old()) && $use_old_input){
            $selected_val = old($name);
        }else{
            $selected_val = $this->options[self::VALUE] ?? null;
        }

        $default_option = null;
        if ($this->options[self::SHOW_DEFAULT_OPTION]) {
            if ($this->options[self::MULTIPLE]) {
                $this->warnings[] = "Select(" . $this->class_id . ") nesne ayarı aynı anda hem multiple=true hem show_default_option=true olamaz. show_default_option=false kabul edildi.";
            } else {
                $default_option = (new HtmlElement(self::OPTION, false))
                    ->add_attr(self::VALUE, $this->options[self::DEFAULT_VALUE])
                    ->data("display", "Seçin")
                    ->add_content($this->options[self::DEFAULT_TEXT]);
                array_unshift($this->contents, $default_option);
            }
        }

        if ($this->options[self::MULTIPLE]) {
            data_set($this->options, "html.attributes.multiple", "multiple ", true);
            $this->addClasses(["selectpicker"]);
        } elseif (isset($this->options["html"][self::ATTRIBUTES][self::MULTIPLE])) {
            unset($this->options["html"][self::ATTRIBUTES][self::MULTIPLE]);
        }


        if ($values) {
            foreach ($values as $k => $v) {
                $selected = is_countable($selected_val) ? in_array(($k . ""), $selected_val) : ($k . "") === ($selected_val . "");
                $selected_str = $selected ? "selected" : "";
                $additional_content[] = '<option value="'.$k.'" '.$selected_str.'>'.$v.'</option>';
            }
        }

        $additional_content_strategy = $this->options[self::ADDITIONAL_CONTENT] ?? "";

        switch ($additional_content_strategy) {
            case "replace":
                $this->contents = $additional_content;
                break;
            case self::MERGE:
            default:
                $this->contents = array_merge($this->contents, $additional_content);
                break;
        }

        /*dump($additional_content);
        dump($this->contents);*/
    }




}
