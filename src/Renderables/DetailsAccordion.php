<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\HtmlElement;

class DetailsAccordion extends BuilderRenderable
{

    public const DETAIL = "detail";
    public const DESCRIPTION = "description";
    public const ITEMS = "items";
    public const OPTIONS = "options";
    public const ATTRIBUTES = "attributes";
    public const DEFAULT_VALUE = "default_value";
    public const INPUT_TEXT = "input_text";
    public const PARAMS = "params";
    public const KEYNAME = "keyname";
    public const CUSTOM_TEMPLATE = "custom_template";
    public const ITEMNAME = "itemname";
    public const ARRAY = "array";
    public $collectable_as = ["accordion", "detailsaccordion"];

    private $rename_search = self::DETAIL;
    private $rename_replace = self::DETAIL;

    public $info = [
        "icon_key" => "bars",
        "object_key" => "DetailsAccordion",
        "object_class" => __CLASS__,
        "object_tags" => [
            "container", "mediapress", "object_specific"
        ],
        "name" => "Çoklu Detail Akordeonu",
        self::DESCRIPTION => "Verilen Detail nesnelerini akordeon grubu olarak sunan nesne",
        self::ITEMS => [
            self::OPTIONS => [
                self::ITEMS => [
                    "html" => [
                        self::ITEMS => [
                            self::ATTRIBUTES => [
                                self::ITEMS => [
                                    "class" => [
                                        "type" => self::INPUT_TEXT,
                                        self::DEFAULT_VALUE => "accordion row"
                                    ],
                                ]
                            ]
                        ]

                    ],


                ]
            ],
            self::PARAMS => [
                self::ITEMS => [
                    self::KEYNAME => [
                        "key" => self::KEYNAME,
                        "name" => "Döngü anahtar değişkeni adı",
                        self::DESCRIPTION => "Koleksiyon için girilen foreach döngüsünde, koleksiyon elemanının anahtar değerini taşıyacak değişkenin adı",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "key"
                    ],
                    self::ITEMNAME => [
                        "key" => self::ITEMNAME,
                        "name" => "Döngü değer değişkeni adı",
                        self::DESCRIPTION => "Koleksiyon için girilen foreach döngüsünde, koleksiyon elemanının değerini taşıyacak değişkenin adı",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => self::DETAIL
                    ],
                    self::ARRAY => [
                        "key" => self::ARRAY,
                        "name" => "Detail Koleksiyonu",
                        self::DESCRIPTION => "Detail koleksiyonu. Ebeveyn nesnelerin yönetimine bırakmak için devre dışı bırakın.",
                        "type" => "input_array",
                        "collection_items" => [],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "[]"
                    ]
                ]
            ]
        ],
    ];

    public $options = [
        "html" => [
            "tag" => "div",
            self::ATTRIBUTES => [
                "class" => "accordion row"
            ]
        ],
        "forced_html_classes"=>[
            "details-wrapper"
        ]
    ];

    public $params = [
        self::KEYNAME => "key",
        self::ITEMNAME => self::DETAIL,
        self::ARRAY => [],
        "details" => []
    ];

    // Bu

    public function __construct(array $params = [], array $contents = [], array $options = [], array $data = [])
    {

        parent::__construct($params, $contents, $options, $data);
        $contents = $this->contents;

        $keyname = $this->params[self::KEYNAME] ?? 'key';
        $itemname = $this->params[self::ITEMNAME] ?? self::DETAIL;
        $this->rename_search = $itemname;
        $array = $this->params[self::ARRAY] ?? $this->params['details'];

        $tabs = [];
        $modelpane = array_shift($this->contents);
        if (isset($this->options["capsulate"]) && $this->options["capsulate"]) {
            $current_class = data_get($modelpane, "options.html.attributes.class", "");
            $renewed_class = implode(" ", array_filter([$current_class, "detail-capsule"]));
            data_set($modelpane, "options.html.attributes.class", $renewed_class);
        }

        foreach ($array as ${$keyname} => ${$itemname}) {
            $tab = $modelpane;
            $variation_title = get_variation_local_title(${$itemname});
            data_set($tab, "title", $variation_title, true);

            $detail_class = get_class(${$itemname});
            $parent_class = preg_replace('/Detail$/', '', $detail_class);
            data_set($tab, "options.html.attributes.data-detail-type", $detail_class);
            data_set($tab, "options.html.attributes.data-detail-id", ${$itemname}->id);
            data_set($tab, "options.html.attributes.data-language-id", ${$itemname}->language_id);
            data_set($tab, "options.html.attributes.data-country-group-id", ${$itemname}->country_group_id);
            data_set($tab, "options.html.attributes.data-parent-type", $parent_class);
            data_set($tab, "options.html.attributes.data-parent-id", ${$itemname}->parent_id);
            $find_alias = array_search($detail_class, $this->options["object_types"]);
            if ($find_alias !== false) {
                $this->rename_replace = $find_alias . "->" . ${$itemname}->id;
            }
            $tab = $this->map_raw_contents([$this, "renameFormField"], $tab);

            if (!isset($tab[self::PARAMS])) {
                $tab[self::PARAMS] = [];
            }

            data_set($tab, 'options.title', $variation_title, false);


            $tab[self::PARAMS][$itemname] = ${$itemname};
            $tabs[] = $tab;
        }

        $this->contents += $this->parseAnnotations($tabs);
    }

    protected function renameFormField($field)
    {
        $name = $field[self::OPTIONS]["html"][self::ATTRIBUTES]["name"] ?? null;
        if (!is_null($name)) {
            if (\Str::startsWith($name, $this->rename_search)) {
                $field[self::OPTIONS]["html"][self::ATTRIBUTES]["name"] = preg_replace('/^' . preg_quote($this->rename_search, '/') . '/', $this->rename_replace, $name);
            }
        }
        return $field;
    }

}
