<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\HtmlElement;

class DetailSlugControl extends BuilderRenderable
{
    public const DESCRIPTION = "description";
    public const ITEMS = "items";
    public const OPTIONS = "options";
    public const NAME = "name";
    public const DEFAULT_VALUE = "default_value";
    public const CUSTOM_TEMPLATE = "custom_template";
    public const ATTRIBUTES = "attributes";
    public const INPUT = "input";
    public const CAPTION = "caption";
    public const CLASS1 = "class";
    public const DETAIL = "detail";
    public const COMPONENTS = "components";
    public const WRAPPER = "wrapper";
    public const LABEL = "label";
    public const PREFIXINPUT = "prefixinput";
    public const CONTROLSDIV = "controlsdiv";
    public $info = [
        "icon_key" => "terminal",
        "object_key" => "DetailSlugControl",
        "object_class" => __CLASS__,
        "object_tags" => [
            "mediapress", "form", "object_specific"
        ],
        self::NAME => "Slug Kontrolü <small class='text-danger'>(deprecated - kullanım dışı)</small>",
        self::DESCRIPTION => "Bir Detail nesnesi için Slug düzenleme aracı",
        self::ITEMS => [
            self::OPTIONS => [
                self::ITEMS => [
                    self::CAPTION => [
                        "key" => self::CAPTION,
                        self::NAME => "Başlık",
                        self::DESCRIPTION => "Kullanıcıya gösterilecek başlık",
                        "type" => "input_text",
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "Slug"
                    ],
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "readonly_text",
                                self::DEFAULT_VALUE => self::INPUT
                            ],
                            self::ATTRIBUTES => [
                                self::ITEMS => [
                                    self::CLASS1 => [
                                        "type" => "input_text",
                                        self::DEFAULT_VALUE => "slug-control focus"
                                    ],
                                    /*"value" => [
                                        "key" => "value",
                                        self::NAME => "Değer",
                                        self::DESCRIPTION => "Değer",
                                        "type" => "input_text",
                                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                                        self::DEFAULT_VALUE => "<print></print>"
                                    ]*/
                                ]
                            ]
                        ]
                    
                    ],
                ]
            ],
            "params" => [
                self::ITEMS => [
                    self::DETAIL => [
                        "key" => self::DETAIL,
                        self::NAME => "Detail Nesnesi",
                        self::DESCRIPTION => "Aracın bağlanacağı *Detail nesnesi",
                        "type" => "object_selector",
                        "object_selector_parameters" => [
                            "selector_type" => "",
                            "selectable_objects" => [
                                "Sayfa Yapısı Detayı" => "Mediapress\\Modules\\Content\\Models\\SitemapDetail",
                                "Sayfa Detayı" => "Mediapress\\Modules\\Content\\Models\\PageDetail",
                                "Kategori Detayı" => "Mediapress\\Modules\\Content\\Models\\CategoryDetail",
                            ]
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "<var>detail</var>"
                    ],
                ]
            ]
        ],
    
    
    ];
    public $options = [
        //"sluggable_selector" => ".detail-name", //javascript selector of  source text tag
        self::CAPTION => "Slug",
        "html" => [
            "tag" => self::INPUT,
            self::ATTRIBUTES => [
                self::CLASS1 => "focus",
                "readonly" => "readonly",
                self::NAME => "detail->slug"
            ]
        ],
        "collectable_as"=>["formfield"],
        "forced_html_classes"=>[
            "slug-control"
        ],
        self::COMPONENTS => [
            self::WRAPPER => [
                "type" => "formgroup",
                self::OPTIONS => [
                    "html" => [
                        self::ATTRIBUTES => [
                            self::CLASS1 => "form-group focus detail-slug-control"
                        ]
                    ]
                ]
            ],
            /*"caption"=>[
                "type"=>"bluetitle",
                "contents"=>[]
            ],*/
            self::LABEL => [
                "type" => self::LABEL,
                "contents" => []
            ],
            self::PREFIXINPUT => [
                "type" => self::INPUT,
                self::OPTIONS => [
                    "html" => [
                        self::ATTRIBUTES => [
                            "type" => "hidden",
                            self::NAME => "detail->slug_prefix"
                        ]
                    ]
                ]
            ],
            self::CONTROLSDIV => [
                "type" => "div",
                self::OPTIONS => [
                    "html" => [
                        self::ATTRIBUTES => [
                            self::CLASS1 => "col-12",
                            "style" => "margin-top:20px;"
                        ]
                    ]
                ]
            ],
            "autoupdatecontrol" => [
                "type" => "icheck",
                self::OPTIONS => [
                    "title" => "Oto güncelle",
                    "checked" => false,
                    "html" => [
                        self::ATTRIBUTES => [
                            self::CLASS1 => "autoupdate"
                        ]
                    ]
                ]
            ],
            "manualeditbtn" => [
                "type" => "button",
                self::OPTIONS => [
                    self::CAPTION => "Elle güncelle",
                    "iconname" => "edit",
                    "html" => [
                        self::ATTRIBUTES => [
                            self::CLASS1 => "btn float-right manual-slug-btn",
                        ]
                    ]
                ]
            ],
            "updatenowbtn" => [
                "type" => "button",
                self::OPTIONS => [
                    self::CAPTION => "Şimdi güncelle",
                    "iconname" => "sync",
                    "html" => [
                        self::ATTRIBUTES => [
                            self::CLASS1 => "btn float-right sync-slug-btn mr-1",
                        ]
                    ]
                ]
            ]
        ],
    ];
    
    public $params = [
        self::DETAIL => null
    ];
    
    
    public function getHtmlElement()
    {
        //dump($this->options["collectable_as"]);
        if ($this->ignored_if) {
            return (new HtmlElement());
        }
        extract($this->params);
        
        
        //return $el;
    
        if ($detail) {
            data_set($this->options, "html.attributes.value", $detail->slug, true);
        }
        
        $el = parent::getHtmlElement();
        
        if (!($this->options[self::COMPONENTS][self::CONTROLSDIV] ?? null)) {
            data_set($this->options, "components.controlsdiv", ["type" => "div", self::OPTIONS => []], true);
        }
        if (!($this->options[self::COMPONENTS][self::WRAPPER] ?? null)) {
            data_set($this->options, "components.wrapper", ["type" => "formgroup"], true);
        }
        if (!($this->options[self::COMPONENTS][self::PREFIXINPUT] ?? null)) {
            data_set(
                $this->options,
                "components.prefixinput", [
                "type" => self::INPUT,
                self::OPTIONS => [
                    "html" => [
                        self::ATTRIBUTES => [
                            "type" => "hidden",
                            self::NAME => "detail->slug_prefix"
                        ]
                    ]
                ]
            ],
                true
            );
        }
        
        
        $wrapper = $this->options[self::COMPONENTS][self::WRAPPER];
        $controlsdiv = $this->options[self::COMPONENTS][self::CONTROLSDIV];
        $prefixinput = $this->options[self::COMPONENTS][self::PREFIXINPUT];
        $label = $this->options[self::COMPONENTS][self::LABEL] ?? null;
        //$caption =  $this->options["components"]["caption"] ?? null;
        $autoupdatecontrol = $this->options[self::COMPONENTS]["autoupdatecontrol"] ?? null;
        $manualeditbtn = $this->options[self::COMPONENTS]["manualeditbtn"] ?? null;
        $updatenowbtn = $this->options[self::COMPONENTS]["updatenowbtn"] ?? null;
        
        $caption_str = $this->options[self::CAPTION] ?? null;
        
        
        data_set($wrapper, "contents.prefixinput", $prefixinput, true);
        
        if ($label) {
            data_set($label, "options.html.attributes.for", $el->get_id());
        }
        
        /*if( $caption && $caption_str){
            data_set($caption,"contents.text",$caption_str,true);
        }

        if($caption){
            data_set($wrapper,"contents.caption",$caption,true);
        }*/
        if ($label && $caption_str) {
            data_set($label, "contents.text", $caption_str, true);
        }
        
        if ($label) {
            data_set($wrapper, "contents.label", $label, true);
        }
        
        data_set($wrapper, "contents.input", $el, true);
        
        
        if ($autoupdatecontrol) {
            data_set($controlsdiv, "contents.autoupdatecontrol", $autoupdatecontrol, true);
        }
        if ($manualeditbtn) {
            data_set($controlsdiv, "contents.manualeditbtn", $manualeditbtn, true);
        }
        if ($updatenowbtn) {
            data_set($controlsdiv, "contents.updatenowbtn", $updatenowbtn, true);
        }
        
        data_set($wrapper, "contents.controlsdiv", $controlsdiv, true);
        
        
        $wrapper = $this->buildRenderableFromArray($wrapper);
        
        return $wrapper->getHtmlElement();
        
    }
    
}
