<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;

class Anchor extends BuilderRenderable
{

    public const ITEMS = "items";
    public const DEFAULT_VALUE = "default_value";
    public $info = [
        "icon_key" => "link",
        "object_key" => "Anchor",
        "object_class" => __CLASS__,
        "object_tags" => [
            "basic_html"
        ],
        "name" => "Bağlantı",
        "description" => "Html Anchor elemanı",
        self::ITEMS => [
            "options" => [
                self::ITEMS => [

                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "readonly_text",
                                self::DEFAULT_VALUE => "a"
                            ],
                            "attributes" => [
                                self::ITEMS => [
                                    "class" => [
                                        "type" => "input_text",
                                        self::DEFAULT_VALUE => ""
                                    ],
                                    "href" => [
                                        "key" => "href",
                                        "name" => "Hedef",
                                        "description" => "",
                                        "type" => "input_text",
                                        "custom_template" => "", // html - used if input_type is custom,
                                        self::DEFAULT_VALUE => "#"
                                    ]
                                ]
                            ]
                        ]

                    ],


                ]
            ],
        ],


    ];
    public $options = [
        "html" => [
            "tag" => "a",
            "void_element" => false,
            "attributes" => [
                "href" => "#"
            ]
        ]
    ];

    public $collectable_as = ["a", "anchor", "link"];

}