<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\HtmlElement;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\PropertyDetail;

class PagePropertiesControl extends BuilderRenderable
{
    public const ATTRIBUTES = "attributes";
    public const DEFAULT_VALUE = "default_value";
    public const OPTIONS = "options";
    public const ITEMS = "items";
    public const DESCRIPTION = "description";
    public $collectable_as = ["tabs", "stacktabs", "pagepropertiescontrol", "pagepropertiestabs"];
    
    public $info = [
        "icon_key" => "tasks",
        "object_key" => "PagePropertiesControl",
        "object_class" => __CLASS__,
        "object_tags" => [
            "mediapress", "form", "object_specific"
        ],
        "name" => "Page Properties",
        self::DESCRIPTION => "Verilen sayfanın özelliklerini listeleyen sekme",
        self::ITEMS => [
            self::OPTIONS => [
                self::ITEMS => [
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "input_text",
                                self::DEFAULT_VALUE => "div"
                            ],
                            self::ATTRIBUTES => [
                                self::ITEMS => [
                                    "id" => [
                                        "key" => "id",
                                        "name" => "HTML ID",
                                        self::DESCRIPTION => "",
                                        "type" => "input_text",
                                        "custom_template" => "", // html - used if input_type is custom,
                                        self::DEFAULT_VALUE => "ozellikler"
                                    ]
                                ]
                            ]
                        ]
                    ],
                ]
            ],
            "params" => [
                self::ITEMS => [
                    "page_model" => [
                        "key" => "page_model",
                        "name" => "Page Nesnesi",
                        self::DESCRIPTION => "Aracın bağlanacağı Page nesnesi",
                        "type" => "object_selector",
                        "object_selector_parameters" => [
                            "selector_type" => "",
                            "selectable_objects" => []
                        ],
                        "custom_template" => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => null
                    ],
                ]
            ]
        ],
    ];
    
    public $options = [
        "html" => [
            "tag" => "div",
            self::ATTRIBUTES => [
                "id" => "ozellikler"
            ]
        ]
    ];
    
    
    public function getHtmlElement()
    {
        
        if ($this->ignored_if) {
            return (new HtmlElement());
        }
        $el = $this->getSelfHtmlElement();
        
        $contents = $this->getContentHtmlElements();
        $nav = (new HTMLElement("div", false))->add_class("tab");
        $counter = 0;
        foreach ($contents as $ckey => $content) {
            if (is_a($content, "Mediapress\\Foundation\\HTMLElement") && $content->has_class("tabcontent") && $content->get_tag()) {
                $counter++;
                if ($counter == 1) {
                    $content->add_class("active");
                }
                $button = (new HtmlElement('a', false))
                    ->add_attr("href", 'javascript:void(0);')
                    ->add_attr("onclick", "switchVerticalTab(event,'" . $content->get_id() . "');")
                    ->add_class('tablinks')
                    ->add_content($content->data('tab-title'))
                    ->data("prop-id", $content->data('prop-id'));
                $nav->add_content($button->add_class($counter == 1 ? "active" : ""));
            }
        }
        $txtdiv = (new HtmlElement('div'))->add_class('txt');
        $tabcontent = (new HTMLElement("div", false))->add_class("tab-content")->add_attr("style", "padding-top:40px;");
        
        $md3 = (new HtmlElement('div'))->add_class("col-3");
        $md9 = (new HtmlElement('div'))->add_class("col-9");
        $kriterbox = (new HtmlElement('div'))->add_class('row');
        
        $kriterbox->add_content($md3->add_content($nav))
            ->add_content(
                $md9->add_content(
                    $txtdiv->add_content($contents)
                //->add_content("<div class='clearfix'></div>")
                )
            );
        return $el->add_content($kriterbox);
    }
    
    
    public function defaultContents()
    {
        extract($this->params);
        
        $contents_generated = [];
        
        /** @var Page $page_model */
        
        if (!isset($page_model) || !$page_model || !is_a($page_model, Page::class)) {
            data_set($this->options, "html.tag", null, true);
            $this->ignored_if = true;
            $this->errors[] = "page_model parametresi bir " . Page::class . " nesnesi olmalıdır.";
            return [];
        }
        /*if (!isset($page_model)) {
            throw new \Exception('PagePropertiesControl için page_model parametresine geçerli bir ' . Page::class . " sınıfı örneği atanmalıdır.");
        }*/
        
        $smprops = $page_model->sitemap->properties()->with(['details', 'details.language', 'details.countryGroup'])->get();
        
        $mainnodes = $smprops->where('depth', 0);
        $counter = 0;
        $category_ids_str = "";
        foreach ($mainnodes as $mn) {
            $counter++;
            $tabmodel = [
                "type" => "tab",
                self::OPTIONS => [
                    "title" => $mn->detail->name,
                    "html" => [
                        self::ATTRIBUTES => [
                            "class" => "tabcontent" . ($counter == 1 ? " active" : ""),
                            "style" => ($counter == 1 ? "display: block;" : "display: none;"),
                            "data-prop-id" => $mn->id,
                            "id" => "tab-page-prop-" . $mn->id
                        ]
                    ]
                ],
                "contents" => [
                ]
            ];
            /*get_variation_local_title($mndt,true)*/
            /** @var PropertyDetail $mndt */
            foreach ($mn->details as $mndt) {
                $tabmodel["contents"][] =
                    /*[
                        "type"=>"inputwithimgbtn",
                    ];*/
                    /*[
                        "type"=>"inputwithimgbtn",
                        "options"=>[
                            "html"=>[
                                "attributes"=>[
                                    "type"=>"text",
                                    "name"=>"page_property:fon(page_id:$page_model->id,property_id:$mn->id,language_id:$mndt->language_id)->value",
                                    "id"=>"inp-prop-".$mn->id."-".$mndt->id,
                                    "placeholder"=>$mndt->name
                                ]
                            ],
                            "components"=>[
                                "caption"=>["contents"=>["deneme"]]
                            ]
                        ]
                    ];*/
                    [
                        "type" => "inputwithlabel",
                        self::OPTIONS => [
                            "title" => ($mndt->countryGroup ? $mndt->countryGroup->title . " - " : "") . $mndt->language->name,
                            "html" => [
                                self::ATTRIBUTES => [
                                    "type" => "text",
                                    "name" => "page_property->fon:prop$mndt->id(page_id:$page_model->id,property_id:$mn->id,language_id:$mndt->language_id)->value",
                                    "id" => "inp-prop-" . $mn->id . "-" . $mndt->id,
                                    //"placeholder"=>$mndt->name,
                                    "class" => "form-control"
                                ]
                            ]
                        ]
                    ];
            }
            
            
            $contents_generated[] = $tabmodel;
        }
        
        return $contents_generated;
        
    }
}
