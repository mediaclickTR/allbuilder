<?php
/**
 * Created by PhpStorm.
 * User: kahraman
 * Date: 16.01.2020
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Facades\DataSourceEngine;
use Mediapress\Foundation\HtmlElement;

class ProvinceSelect extends Select
{
    public const SELECTED = "selected";
    public const OPTION = "option";
    public const VALUE = "value";
    public const SELECT = "select";
    public const DEFAULT_TEXT = "default_text";
    public const MERGE = "merge";
    public const ADDITIONAL_CONTENT = "additional_content";
    public const SHOW_DEFAULT_OPTION = "show_default_option";
    public const VALUES = "values";
    public const RADIO = "radio";
    public const MULTIPLE = "multiple";
    public const DEFAULT_VALUE = "default_value";
    public const CUSTOM_TEMPLATE = "custom_template";
    public const INPUT_TEXT = "input_text";
    public const ITEMS = "items";
    public const DESCRIPTION = "description";
    public const TITLE = "title";
    public const FORMGROUP = "formgroup";
    public const COMPONENTS = "components";
    public const CAPTION = "caption";
    public const CLASS1 = "class";
    public const CONTENTS = "contents";
    public const LABEL = "label";

    public $info = [
        "icon_key" => "caret-square-down",
        "object_key" => "CountryandProvinceSelect",
        "object_class" => __CLASS__,
        "object_tags" => [
            "basic_html", "form"
        ],
        "name" => "Şehir Seçici",
        self::DESCRIPTION => "Şehir seçimi yapabileceğiniz bir HTML Select",
        self::ITEMS => [
            "options" => [
                self::ITEMS => [
                    "title" => [
                        "key" => "title",
                        "name" => "Başlık",
                        self::DESCRIPTION => "Nesnenin başlığı",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "Şehir Seçin"
                    ],
                    self::MULTIPLE => [
                        "key" => self::MULTIPLE,
                        "name" => "Çoklu Seçim",
                        self::DESCRIPTION => "Birden fazla değer seçmeye izin verilip verilmeyeceğini belirler",
                        "type" => self::RADIO,
                        self::VALUES => [
                            "0" => "Hayır",
                            "1" => "Evet",
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "0"
                    ],
                    self::VALUE => [
                        "key" => self::VALUE,
                        "name" => "Değer",
                        self::DESCRIPTION => "Nesneye atanacak değer",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => ""
                    ],
                    self::SHOW_DEFAULT_OPTION => [
                        "key" => self::SHOW_DEFAULT_OPTION,
                        "name" => "Varsayılan Seçimi Göster",
                        self::DESCRIPTION => "Kullanıcı seçim yapmadan önce seçili görünecek bir değer olup olmayacağını belirler",
                        "type" => self::RADIO,
                        self::VALUES => [
                            "1" => "Evet",
                            "0" => "Hayır",
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => null
                    ],
                    self::ADDITIONAL_CONTENT => [
                        "key" => self::ADDITIONAL_CONTENT,
                        "name" => "Data ile üretilen içerik",
                        self::DESCRIPTION => "Data parametrelerinden üretilen içeriği, hali hazırda bulunan içerikle değiştirilmesini ya da ona eklenmesini belirler",
                        "type" => self::RADIO,
                        self::VALUES => [
                            "replace" => "Yer değiştir",
                            self::MERGE => "Birleştir",
                        ],
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => self::MERGE
                    ],
                    self::DEFAULT_VALUE => [
                        "key" => self::DEFAULT_VALUE,
                        "name" => "Varsayılan eleman değeri",
                        self::DESCRIPTION => "",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => null
                    ],
                    self::DEFAULT_TEXT => [
                        "key" => self::DEFAULT_TEXT,
                        "name" => "Varsayılan eleman metni",
                        self::DESCRIPTION => "",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "---Seçiniz---"
                    ],
                    "country_id" => [
                        "key" => "country_id",
                        "name" => "Ülke Seçici id'si",
                        self::DESCRIPTION => "Eğer Ülkeye bağlı  şehir seçilecek ise kullanılmalıdır. Sehir Seçici'yi bağlamak istediğin Ülke Seçici id'si girilmelidir ",
                        "type" => self::INPUT_TEXT,
                        self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "select-country"
                    ],
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => "readonly_text",
                                self::DEFAULT_VALUE => self::SELECT
                            ],
                            "attributes" => [
                                self::ITEMS => [
                                    "class" => [
                                        "type" => self::INPUT_TEXT,
                                        self::DEFAULT_VALUE => ""
                                    ],
                                "data-live-search" => [
                                    "key" => "data-live-search",
                                    "name" => "Seçeneklerde arama Yapılabilsin mi?",
                                    "type" => self::RADIO,
                                    self::VALUES => [
                                        "true" => "Evet",
                                        "false" => "Hayır",
                                    ],
                                    self::DESCRIPTION => "",
                                    self::CUSTOM_TEMPLATE => "", // html - used if input_type is custom,
                                    self::DEFAULT_VALUE => "true"
                                ],
                                ]
                            ]
                        ]
                    ],
                ]
            ]

        ],
    ];

    public $options = [
        self::VALUE => null,
        "html" => [
            "tag" => self::SELECT,
            "void_element" => false,
            "attributes"=>[
                "id"=>"select-province",
                "data-live-search"=>"true",
                "data-width"=>"350",
                "data-size"=>"8",

            ]
        ],
        "primary_key"=>"id",
        self::MULTIPLE => false,
        self::ADDITIONAL_CONTENT => self::MERGE, // replace
        self::SHOW_DEFAULT_OPTION => true,
        self::DEFAULT_TEXT => "---- Seçim Yok ----",
        "country_id"=>"select-country",
        self::DEFAULT_VALUE => "",
        "forced_html_classes"=>["selectpicker"],
        "caption_mode" => "last_two",
        self::COMPONENTS => [
            self::FORMGROUP => [
                "type" => "div",
                self::OPTIONS => [
                    "html" => [
                        self::ATTRIBUTES => [
                            self::CLASS1 => "form-group focus pagecategoriescontrol-form-group"
                        ]
                    ],
                    "collectable_as" => ["form-group"]
                ],
                self::CONTENTS => []

            ],
            self::CAPTION => [
                "type" => "bluetitle",
                self::OPTIONS => [
                    "collectable_as" => [self::CAPTION]
                ]
            ]
        ]
    ];

    public $data=[
        "values"=>[]
    ];


    public function __construct(array $params = [], array $contents = [], array $options = [], array $data = [])
    {

        $this->options = array_replace_recursive(
            $this->baseOptions(),
            array_replace_recursive(
                $this->options,
                array_replace_recursive(
                    $this->defaultOptions(),
                    $options)
            )
        );
        $primary_key = $this->options["primary_key"] ?? "id";
        $country_id = $this->options["country_id"];


        $this->data["values"]=  DataSourceEngine::getDataSource('MPCore', 'ProvincesDS')->setParams(["pluck"=>true, "pluck_key"=>$primary_key, "pluck_column"=>"name"])->getData();
        $this->extendStack("scripts", $this->script($country_id));

        parent::__construct($params, $contents, $options, $data);

    }


    public $collectable_as = [self::SELECT, "input", "formfield", "country_select"];

    private function script($country_id)
    {
        $script = <<<SCRIPT
    <script>
    $(document).ready(function(){
        $("#$country_id").change(function(){
            var country_id = $(this).children("option:selected").val();
            if(typeof pageCountryandProvinceSelectionChanged === "function"){
                pageCountryandProvinceSelectionChanged(country_id);
            }else{
                console.warn("MEDIAPRESS400: pageCategoriesSelectionChanged(page_id,category_ids_arr) fonksiyonu tanımlanmamış. Seçilen kategorilere göre işlem yapabilmek için bu fonksiyonu tanımlayın.")
            }
        }).change();
    });
</script>
SCRIPT;
        return $script;

    }

    public function getHtmlElement()
    {
        if ($this->ignored_if) {
            return (new HtmlElement());
        }
        $el = parent::getHtmlElement();
        /*$contents = $this->getContentHtmlElements();
        $el->add_content($contents);*/

        data_set($this->options, self::TITLE, "Ülke ", false);

        $name = \Str::slug($this->options[self::TITLE]);
        $this->options[self::TITLE] = getPanelLangPartKey($name, $this->options[self::TITLE], true);

        $classes = data_get($this->options, "html.attributes.class", "");
        $new_cls = $classes . " page-categories-control" . (isset($this->options[self::MULTIPLE]) && $this->options[self::MULTIPLE] ? " multiple" : "");
        data_set($this->options, "html.attributes.class", $new_cls, true);

        $formgroup = $this->options[self::COMPONENTS][self::FORMGROUP] ?? null;
        $caption = $this->options[self::COMPONENTS][self::CAPTION] ?? null;

        if ($formgroup && $caption) {
            $this->options[self::COMPONENTS][self::CAPTION][self::CONTENTS][] = $this->options[self::TITLE];
            $caption = $this->buildRenderableFromArray($this->options[self::COMPONENTS][self::CAPTION]);
        }

        if ($formgroup) {
            if ($caption) {
                $this->options[self::COMPONENTS][self::FORMGROUP][self::CONTENTS][self::CAPTION] = $caption;
            }
            $this->options[self::COMPONENTS][self::FORMGROUP][self::CONTENTS]["input"] = (new HtmlElement("div"))->add_class("row")->add_content((new HtmlElement("div"))->add_class("col")->add_content($el));
            $formgroup = $this->buildRenderableFromArray($this->options[self::COMPONENTS][self::FORMGROUP]);
        }
        return $formgroup ? $formgroup->getHtmlElement() : $el;
    }



}
