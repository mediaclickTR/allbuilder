<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 24.09.2018
 * Time: 16:07
 */

namespace Mediapress\AllBuilder\Renderables;

use Mediapress\AllBuilder\Foundation\BuilderRenderable;
use Mediapress\Foundation\HtmlElement;

class AccordionPane extends BuilderRenderable
{

    public const DESCRIPTION = "description";
    public const ITEMS = "items";
    public const OPTIONS = "options";
    public const TITLE = "title";
    public const INPUT_TEXT = "input_text";
    public const DEFAULT_VALUE = "default_value";
    public const INITIAL_STATUS = "initial_status";
    public const CLOSED = "closed";
    public const ATTRIBUTES = "attributes";
    public const CLASS1 = "class";
    public const CARD_ACCORDION_PANE = "card accordion-pane";
    public const COMPONENTS = "components";
    public const CONTENTS = "contents";
    public const BUTTON = "button";
    public const COLLAPSE = "collapse";
    public const COLLAPSED = "collapsed";
    public $info = [
        "icon_key" => "bars",
        "object_key" => "AccordionPane",
        "object_class" => __CLASS__,
        "object_tags" => [
            "container", "mediapress"
        ],
        "name" => "Akordeon Dilimi",
        self::DESCRIPTION => "Akordeon nesnesine eklenebilecek bir dilim",
        self::ITEMS => [
            self::OPTIONS => [
                self::ITEMS => [
                    self::TITLE => [
                        "key" => self::TITLE,
                        "name" => "Başlık",
                        self::DESCRIPTION => "Dilimin kullanıcıya gösterilecek başlığı",
                        "type" => self::INPUT_TEXT,
                        "custom_template" => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => "Akordeon"
                    ],
                    self::INITIAL_STATUS => [
                        "key" => self::INITIAL_STATUS,
                        "name" => "İlk durum",
                        self::DESCRIPTION => "Dilimin başlangıçta nasıl görüntüleneceğini seçin",
                        "type" => "select",
                        "values" => [
                            ":null" => "Seçim Yok",
                            "open" => "Açık",
                            self::CLOSED => "Kapalı"
                        ],
                        "custom_template" => "", // html - used if input_type is custom,
                        self::DEFAULT_VALUE => self::CLOSED
                    ], //open | closed
                    "html" => [
                        self::ITEMS => [
                            "tag" => [
                                "type" => self::INPUT_TEXT,
                                self::DEFAULT_VALUE => "div"
                            ],
                            self::ATTRIBUTES => [
                                self::ITEMS => [
                                    self::CLASS1 => [
                                        "type" => self::INPUT_TEXT,
                                        self::DEFAULT_VALUE => self::CARD_ACCORDION_PANE
                                    ]
                                ]
                            ]
                        ]

                    ],


                ]
            ]
        ],


    ];
    public $options = [
        self::TITLE => "Accordion Pane",
        self::INITIAL_STATUS => self::CLOSED, //open | closed
        "html" => [
            "tag" => "div", // self is card
            self::ATTRIBUTES => [
                self::CLASS1 => self::CARD_ACCORDION_PANE
            ]
        ],
        self::COMPONENTS => [
            "header" => [
                "type" => "div",
                self::OPTIONS => [
                    "html" => [
                        self::ATTRIBUTES => [
                            self::CLASS1 => "card-header"
                        ]
                    ]
                ],
                self::CONTENTS => [
                    "header" => [
                        "type" => "blank",
                        self::OPTIONS => [
                            "html" => [
                                "tag" => "h5",
                                self::ATTRIBUTES => [
                                    self::CLASS1 => "mb-0"
                                ]
                            ]
                        ],
                        self::CONTENTS => [
                            self::BUTTON => [
                                "type" => self::BUTTON,
                                self::OPTIONS => [
                                    "html" => [
                                        self::ATTRIBUTES => [
                                            "type" => self::BUTTON,
                                            self::CLASS1 => "btn btn-link",
                                            "data-toggle" => self::COLLAPSE,
                                            "data-target" => "#", // #collapsibleid
                                            "aria-expanded" => "false", //initial_status==open ? true : false
                                            "aria-controls" => "" //collapsibleid
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ],
            "collapsible" => [
                "type" => "div",
                self::OPTIONS => [
                    "html" => [
                        self::ATTRIBUTES => [
                            self::CLASS1 => self::COLLAPSE, //initial_status==open ? collapse show : collapse
                            "data-parent" => ".accordion"
                        ]
                    ]
                ],
                self::CONTENTS => [
                    "cardbody" => [
                        "type" => "div",
                        self::OPTIONS => [
                            "html" => [
                                self::ATTRIBUTES => [
                                    self::CLASS1 => "accordion-body"
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ];


    public function __construct(array $params = [], array $contents = [], array $options = [], array $data = [])
    {

        $this->contents = array_replace_recursive($this->contents, array_replace_recursive($this->defaultContents(), $contents));
        $this->options = array_replace_recursive(
            $this->baseOptions(),
            array_replace_recursive(
                $this->options,
                array_replace_recursive(
                    $this->defaultOptions(),
                    $options)
            )
        );

        $name = \Str::slug($this->options[self::TITLE]);
        $this->options[self::TITLE] = getPanelLangPartKey($name, $this->options[self::TITLE], true);

        $contents_to_wrap = $this->contents;
        $new_contents = $this->options[self::COMPONENTS];

        $initial_status = data_get($this->options, self::INITIAL_STATUS, self::CLOSED);
        $initial_status = $initial_status == "open";
        $class_id = strtolower("div") . "-" . \Str::random(10);

        data_set($new_contents, "collapsible.options.html.attributes.id", $class_id);

        data_set($new_contents, "header.contents.header.contents.button.options.html.attributes.data-target", "#" . $class_id, true);

        $btnclass = data_get($new_contents, "header.contents.header.contents.button.options.html.attributes.class", "btn btn-link");
        $btnclass = preg_split("/\s/", $btnclass);
        if ($initial_status && in_array(self::COLLAPSED, $btnclass)) {
            unset($btnclass[array_search(self::COLLAPSED, $btnclass)]);
        } else {
            $btnclass[] = self::COLLAPSED;
        }
        $btnclass = implode(" ", array_unique(array_filter($btnclass)));
        data_set($new_contents, "header.contents.header.contents.button.options.html.attributes.class", $btnclass, true);


        $classofthis = data_get($this->options, "html.attributes.class", self::CARD_ACCORDION_PANE);
        $classofthis = preg_split("/\s/", $classofthis);
        $classofthis[] = "card";
        $classofthis[] = "accordion-pane";
        $classofthis = implode(" ", array_unique(array_filter($classofthis)));
        data_set($this->options, "html.attributes.class", $classofthis, true);


        data_set($new_contents, "header.contents.header.contents.button.options.html.attributes.aria-controls", $class_id, true);
        data_set($new_contents, "header.contents.header.contents.button.options.caption", $this->options[self::TITLE], true);

        data_set($new_contents, "collapsible.contents.cardbody.contents", $contents_to_wrap, true);

        data_set($new_contents, "header.contents.header.contents.button.options.html.attributes.aria-expanded", ($initial_status ? "true" : "false"));
        data_set($new_contents, "collapsible.options.html.attributes.class", ($initial_status ? "collapse show" : self::COLLAPSE));
        $this->contents = $new_contents;

        unset($this->options[self::COMPONENTS]);

        parent::__construct($params, $this->contents, $this->options, $data);

    }
}
